<?
$_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__).'/..'); // на один уровень выше, чем /cron/
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS',true);
define('SITE_ID', 's1');
$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

// Проверяем, подключен ли модуль Инфоблоки
if (!\Bitrix\Main\Loader::includeModule('iblock')) return false;

// Записываем в переменные конфигурационные данные API Youtube
$apiKey = "AIzaSyA1ZIBkGFxKbCfNos-r5kjeoELu4pgRmWA";
$chanelID = "UCvy7FIEQYztchmNfCROlgDw";
$maxResult = 10;

// Записываем параметры запроса на Youtube в массив
$params = [
    "order" => "date",
    "part" => "snippet",
    "channelId" => $chanelID,
    "maxResults" => $maxResult,
    "key" => $apiKey
];

// Формируем ссылку запроса
$url = "https://www.googleapis.com/youtube/v3/search?" . http_build_query($params);

// Делаем запрос на Youtube
$c = curl_init($url);
curl_setopt_array($c, [
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_HTTPHEADER => [
        "Content-Type: application/json"
    ]
]);

// Записываем результат выполнения запроса в переменную
$rs = curl_exec($c);
curl_close($c);

// Декодируем полученный ответ в формате json
$rs = json_decode($rs);

// Если в ответе есть ошибки, возвращаем false
if($rs->error) {
    return false;
}

// Параметры для транслита
$params = array(
    "max_len" => 100,
    "change_case" => "L",
    "replace_space" => "_",
    "replace_other" => "_",
    "delete_repeat_replace" => "true",
    "use_google" => "false",
);

// Если есть видео в ответе сервера
if(!empty($rs->items)){
    // Перебираем полученные видео
    foreach($rs->items as $item) {

        var_dump($item->id->videoId);
        // Формируем ссылку на видео Youtube
        $videoUrl = "https://www.youtube.com/watch?v=" . $item->id->videoId;

        // Проверяем, есть ли видео
        $rsVideo = CIBlockElement::GetList([], ["PROPERTY_VIDEO" => $videoUrl], false, [], ["PROPERTY_VIDEO"]);
        // Если видео нет;
        if(!$rsVideo->SelectedRowsCount()) {
            // Добавляем видео
            $elVideo = new CIBlockElement;

            $properties = [];
            $properties["VIDEO"] = $videoUrl;

            $arrElVideoData = [
                "IBLOCK_SECTION_ID" => 3,
                "IBLOCK_ID" => 3,
                "PROPERTY_VALUES"=> $properties,
                "NAME" => $item->snippet->title,
                "CODE" => CUtil::translit($item->snippet->title, "ru", $params),
                "ACTIVE" => "Y"
            ];

            $elVideo->Add($arrElVideoData);
        }
    }
}

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
?>