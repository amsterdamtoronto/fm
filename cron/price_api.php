<?
$_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__).'/..'); // на один уровень выше, чем /cron/
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS',true);
define('SITE_ID', 's1');
$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

if (CModule::IncludeModule("iblock")):
    global $message_log;
    $message_log = "Date ". date('jS \of F Y G:i'). "\n";
    $iblockId = "4";
    $arFilter = Array("IBLOCK_ID" => $iblockId, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, array('ID', 'NAME', 'IBLOCK_ID', 'SORT'));
    while ($arElements = $res->GetNextElement()) // Получаем список элементов
    {
        $element = $arElements->GetFields();
        $elementId = $element["ID"];
        $message_log .= "\n" . $element["NAME"];

        $sort = $element["SORT"];




        $elementsProp = CIBlockElement::GetProperty($iblockId, $elementId, array("sort" => "asc"), array("")); // Получаем список свойств элемента
        while ($elementProp = $elementsProp->GetNext()) {

            if ($elementProp["ID"] == "17") {
                $apiId = $elementProp["VALUE"];
            }



        }



        if ($apiId) :
            $dataApiUrl = "https://script.google.com/macros/s/AKfycbxLkYuOqup8VQnATxwcgUnJar7fGGFDNj9GbwmUV0l0zr6VoG0i/exec?id=" . $apiId;
            $dataApi = file_get_contents($dataApiUrl);
            $dataApi = json_decode($dataApi, true);
            $dataApiPrice = $dataApi['price'];
            //debug($dataApiPrice);
            $message_log .= "\nNew price: " . $dataApiPrice . "\n";
            // записываем цену в свойство
            if (is_numeric($dataApiPrice)) { // отсекаем NaN
                if($dataApiPrice == 0){
                    CIBlockElement::SetPropertyValuesEx($elementId, false, array("PRICE" => $dataApiPrice));
                    CIBlockElement::SetPropertyValuesEx($elementId, false, array("NO_PRICE" => $dataApi['text']));

                    $sort = $sort + 1000;
                    $el = new CIBlockElement();
                    $el->Update($elementId, array('SORT' => $sort));


                }else{
                    CIBlockElement::SetPropertyValuesEx($elementId, false, array("PRICE" => $dataApiPrice));

                    if($sort >= 1001){

                        $sort = $sort + 1000;
                        $el = new CIBlockElement();
                        $el->Update($elementId, array('SORT' => $sort));
                    }

                }
            }
        endif;
    }
    $message_log .= "------------------ \n";
    $filename = $_SERVER["DOCUMENT_ROOT"] . '/upload/tmp/log_api_price.txt';
    file_put_contents($filename, $message_log . PHP_EOL, FILE_APPEND);
    debug($message_log);
endif;
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
?>
