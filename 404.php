<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Страница не найдена");?>
    <style>
        .bWhite{
            background: #fff;
            display: flex;
            align-items: center;
            justify-items: center;
            padding: 55px 15px;
            flex-direction: column;
        }
        .smText{
            margin-bottom: 25px;
            font-size: 26px;
        }
        .warnText{
            font-size: 70px;
            font-weight: 900;
            margin-bottom: 25px;
        }
    </style>
<div class="head-section"></div>
    <div class="advantages gray">
        <div class="container bWhite">
            <h1 class="smText">Страницы не существует. Вернитесь на главную.</h1>
            <a class="btn" href="/" >Перейти на главную</a>
        </div>
    </div>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>