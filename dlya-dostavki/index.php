<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Аренда электросамокатов для бизнеса в Москве");
$APPLICATION->SetPageProperty("description", "Аренда электросамокатов для бизнеса от Московской мастерской FastMotion. Самокаты и велосипеды для доставки, прокат мощного транспорта для работы.");
$APPLICATION->SetTitle("B2B Аренда");
?><section class="head-section headSection-b2b">
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"bread",
	Array(
		"PATH" => "",
		"SITE_ID" => "s1",
		"START_FROM" => "0"
	)
);?>
		</div>
		<div class="col-lg-12">
			<h1 class="head-section__title">
			<? $APPLICATION->ShowTitle(false) ?> </h1>
 <a class="btn" href="#" data-target="#exampleModal2" data-toggle="modal">БЕСПЛАТНАЯ КОНСУЛЬТАЦИЯ</a>
		</div>
	</div>
</div>
 </section> <section class="advantages advantagesB2B">
<div class="container">
	<div class="row d-flex justify-content-between pb-5">
		<div class="col-lg-5">
			<div class="advantages__left">
 <img src="/local/templates/motion_new/images/alogo.svg" class="logo">
				<h2 class="advantages__title">
				Решение по аренде и обсуживанию электротранспорта </h2>
				<div class="advantages__desc">
					 для курьерских служб, даркстров, дарктчинов в <br>
 <span class="advantages__desc-red">Москве, Санкт-Петербурге и Краснодаре.</span>
				</div>
			</div>
		</div>
		<div class="col-lg-7 d-flex flex-wrap justify-content-between">
			<div class="advantages__block d-flex flex-column">
 <img src="/bitrix/templates/motion/images/01.svg">
				<div class="advantages__t">
					 Аренда
				</div>
				<div class="advantages__d">
					 Транспорт подготовлен для работы в условиях доставки всесезонно
				</div>
			</div>
			<div class="advantages__block d-flex flex-column">
 <img src="/bitrix/templates/motion/images/02.svg">
				<div class="advantages__t">
					 Обслуживание
				</div>
				<div class="advantages__d">
					 Регулярное ТО и оперативный ремонт по заявке
				</div>
			</div>
			<div class="advantages__block d-flex flex-column">
 <img src="/bitrix/templates/motion/images/03.svg">
				<div class="advantages__t">
					 Отчет
				</div>
				<div class="advantages__d">
					 Подрабный отчет по всем выполненным работам
				</div>
			</div>
			<div class="advantages__block d-flex flex-column">
 <img src="/bitrix/templates/motion/images/bat55.svg">
				<div class="advantages__t">
					 Дополнительные батареии
				</div>
				<div class="advantages__d">
					 Для быстрой замены
				</div>
			</div>
		</div>
	</div>
	<div class="row mt-5 d-flex align-items-center justify-content-between ">
		<div class="col-lg-9 mb-5">
			<h2 class="title mb-3">
			Мы накопили большой опыт экспуатации электротранспорта в B2B сегменте</h2>
 <span class="subtitle">В условиях повышеной нагрузки (до 3 000 км в мясяц) и в любую погоду и время года. </span>
		</div>
		<div class="col-lg-5 pt-4">
			<h2 class="title-sm mb-5">
			Предлагаем электротранспорт для использования в суровых условиях</h2>
			<div class="advent-list b2bAdvent-list">
				<div class="advent-list__line d-flex align-items-center">
 <img src="/bitrix/templates/motion/images/check.svg">Герметизация и усиление основных узлов (колеса, мотор-колесо, батарейка, дека и руль)
				</div>
				<div class="advent-list__line d-flex align-items-center">
 <img src="/bitrix/templates/motion/images/check.svg">Специальное ПО
				</div>
				<div class="advent-list__line d-flex align-items-center">
 <img src="/bitrix/templates/motion/images/check.svg">Обсуживание парка электротранспорта на специальных условиях при аренды
				</div>
			</div>
		</div>
		<div class="col-lg-7">
			<div class="youtubeButton">
				<div class="youtubeButton__icon">
 <img src="/local/templates/motion_new/images/youtube_icon.png" alt="">
				</div>
				<div class="youtubeButton__text">
					 Подробно посмотреть работы можно на нашем Youtube канале
				</div>
 <a class="btn youtubeButton__link" target="_blank" href="https://www.youtube.com/c/fastmotionelectric">Перейти на канал</a>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<h2 class="col-lg-12 title title-sm mb-1 mt-5">
		Как мы работаем </h2>
	</div>
	<div class="row mt-5">
		<div class="col-lg-9 line">
			<div class="line-red">
			</div>
		</div>
	</div>
	<div class="row work-block__red">
		<div class="col-lg-3 col-md-6">
			<div class="work-block d-flex flex-column">
				 1
				<div class="work-block__title">
					 Согласовываем кол-во самокатов и опции
				</div>
			</div>
		</div>
		<div class="col-lg-2 col-md-6">
			<div class="work-block d-flex flex-column">
				 2
				<div class="work-block__title">
					 Заключаем договор
				</div>
			</div>
		</div>
		<div class="col-lg-2 col-md-6">
			<div class="work-block d-flex flex-column">
				 3
				<div class="work-block__title">
					 Предоставляем доступ к платформе
				</div>
			</div>
		</div>
		<div class="col-lg-2 col-md-6">
			<div class="work-block d-flex flex-column">
				 4
				<div class="work-block__title">
					 Доставляем самокаты
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="work-block d-flex flex-column">
				 5
				<div class="work-block__title">
					 Мы полностью берем обслуживание самокатов на себя
				</div>
			</div>
		</div>
	</div>
</div>
 </section> <section class="gray">
<div class="container">
	<div class="advantagesProduct">
		<div class="row">
			<div class="col-lg-6">
				<div class="row">
					<div class="col-lg-8">
 <img src="/local/templates/motion_new/images/mijia_icon.png" class="advantagesProduct__icon">
						<h2 class="title">Самокат на базе Xiaomi M365 Pro </h2>
						<div class="advent-list b2bAdvent-list">
							<div class="advent-list__line d-flex align-items-center">
 <img src="/bitrix/templates/motion/images/check.svg">
								Самокат полностью подготовлен для всесезонной экспуатации
							</div>
							<div class="advent-list__line d-flex align-items-center">
 <img src="/bitrix/templates/motion/images/check.svg">
								Аккумулятор 12800 mAh (~30км)
							</div>
						</div>
					</div>
					<div class="col-lg-11">
						<div class="advantagesProduct__price-wrap row">
							<div class="col-lg-6 col-sm-6">
								<div class="advantagesProduct__price">
									<div class="advantagesProduct__price-desc">
										 Аренда на месяц
									</div>
									<div class="advantagesProduct__price-price">
										 8 000 ₽
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-sm-6">
								<div class="advantagesProduct__price">
									<div class="advantagesProduct__price-desc">
										 Покупка и подготовка
									</div>
									<div class="advantagesProduct__price-price">
										 40 000 ₽
									</div>
								</div>
							</div>
						</div>
 <a class="btn" href="#" data-toggle="modal" data-target="#exampleModal2">БЕСПЛАТНАЯ КОНСУЛЬТАЦИЯ</a>
					</div>
				</div>
			</div>
			<div class="col-lg-6 actionImg__Wrap">
				<div class="images-about actionImg__m365Pro">
 <img src="/local/templates/motion_new/images/m365_prob2b.png" class="images-about__item">
					<div class="tooltips d-flex align-items-start justify-content-center">
 <span class="circle"></span>
						<div class="tooltips-block">
							<div class="tooltips-block__title">
								 Специализированное ПО
							</div>
						</div>
					</div>
					<div class="tooltips two d-flex align-items-start justify-content-center">
 <span class="circle"></span>
						<div class="tooltips-block">
							<div class="tooltips-block__title">
								 Усиленный узел или муфта
							</div>
						</div>
					</div>
					<div class="tooltips three d-flex align-items-start justify-content-center">
 <span class="circle"></span>
						<div class="tooltips-block">
							<div class="tooltips-block__title">
								 Полная герметизация
							</div>
						</div>
					</div>
					<div class="tooltips four d-flex align-items-start justify-content-center">
 <span class="circle"></span>
						<div class="tooltips-block">
							<div class="tooltips-block__title">
								 Поддержка крыла
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="advantagesProduct">
		<div class="row">
			<div class="col-lg-6">
				<div class="row">
					<div class="col-lg-8">
 <img src="/local/templates/motion_new/images/mijia_icon.png" class="advantagesProduct__icon">
						<h2 class="title">Самокат на базе Xiaomi M365 </h2>
						<div class="advent-list b2bAdvent-list">
							<div class="advent-list__line d-flex align-items-center">
 <img src="/bitrix/templates/motion/images/check.svg">
								Самокат полностью подготовлен для всесезонной экспуатации
							</div>
							<div class="advent-list__line d-flex align-items-center">
 <img src="/bitrix/templates/motion/images/check.svg">
								Аккумулятор 7800 mAh (~20км)
							</div>
						</div>
					</div>
					<div class="col-lg-11">
						<div class="advantagesProduct__price-wrap row">
							<div class="col-lg-6 col-sm-6">
								<div class="advantagesProduct__price">
									<div class="advantagesProduct__price-desc">
										 Аренда на месяц
									</div>
									<div class="advantagesProduct__price-price">
										 6 000 ₽
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-sm-6">
								<div class="advantagesProduct__price">
									<div class="advantagesProduct__price-desc">
										 Покупка и подготовка
									</div>
									<div class="advantagesProduct__price-price">
										 29 000 ₽
									</div>
								</div>
							</div>
						</div>
 <a class="btn" href="#" data-toggle="modal" data-target="#exampleModal2">БЕСПЛАТНАЯ КОНСУЛЬТАЦИЯ</a>
					</div>
				</div>
			</div>
			<div class="col-lg-6 actionImg__Wrap">
				<div class="images-about actionImg__m365">
 <img src="/local/templates/motion_new/images/xiaomi_m365_b2b.png" class="images-about__item">
					<div class="tooltips d-flex align-items-start justify-content-center">
 <span class="circle"></span>
						<div class="tooltips-block">
							<div class="tooltips-block__title">
								 Специализированное ПО
							</div>
						</div>
					</div>
					<div class="tooltips two d-flex align-items-start justify-content-center">
 <span class="circle"></span>
						<div class="tooltips-block">
							<div class="tooltips-block__title">
								 Усиленный узел или муфта
							</div>
						</div>
					</div>
					<div class="tooltips three d-flex align-items-start justify-content-center">
 <span class="circle"></span>
						<div class="tooltips-block">
							<div class="tooltips-block__title">
								 Полная герметизация
							</div>
						</div>
					</div>
					<div class="tooltips four d-flex align-items-start justify-content-center">
 <span class="circle"></span>
						<div class="tooltips-block">
							<div class="tooltips-block__title">
								 Поддержка крыла
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="advantagesProduct">
		<div class="row">
			<div class="col-lg-6 ">
				<div class="row">
					<div class="col-lg-8">
 <img src="/local/templates/motion_new/images/xinze_icon.svg" class="advantagesProduct__icon">
						<h2 class="title">Велосипед на базе Xinze V8</h2>
						<div class="advent-list b2bAdvent-list">
							<div class="advent-list__line d-flex align-items-center">
 <img src="/bitrix/templates/motion/images/check.svg">
								Велосипед полностью подготовлен для всесезонной экспуатации
							</div>
							<div class="advent-list__line d-flex align-items-center">
 <img src="/bitrix/templates/motion/images/check.svg">
								Аккумулятор 12000 mAh (~35км)
							</div>
						</div>
					</div>
					<div class="col-lg-11">
						<div class="advantagesProduct__price-wrap row">
							<div class="col-lg-6 col-sm-6">
								<div class="advantagesProduct__price">
									<div class="advantagesProduct__price-desc">
										 Аренда на месяц
									</div>
									<div class="advantagesProduct__price-price">
										 11 000 ₽
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-sm-6">
								<div class="advantagesProduct__price">
									<div class="advantagesProduct__price-desc">
										 Покупка и подготовка
									</div>
									<div class="advantagesProduct__price-price">
										 46 000 ₽
									</div>
								</div>
							</div>
						</div>
 <a class="btn" href="#" data-toggle="modal" data-target="#exampleModal2">БЕСПЛАТНАЯ КОНСУЛЬТАЦИЯ</a>
					</div>
				</div>
			</div>
			<div class="col-lg-6 actionImg__Wrap">
				<div class="images-about actionImg__xinze_v8">
 <img src="/local/templates/motion_new/images/xinze_v8_b2b.png" class="images-about__item">
					<div class="tooltips d-flex align-items-start justify-content-center">
 <span class="circle"></span>
						<div class="tooltips-block">
							<div class="tooltips-block__title">
								 Настройка и смазка всех узлов
							</div>
						</div>
					</div>
					<div class="tooltips two d-flex align-items-start justify-content-center">
 <span class="circle"></span>
						<div class="tooltips-block">
							<div class="tooltips-block__title">
								 Гидроизоляция
							</div>
						</div>
					</div>
					<div class="tooltips three d-flex align-items-start justify-content-center">
 <span class="circle"></span>
						<div class="tooltips-block">
							<div class="tooltips-block__title">
								 Платформа для сумки
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 </section> <section class="advantages">
<div class="container mt-5 mb-5">
	<div class="row">
		<div class="col-lg-9">
			<h2 class="title mb-3">
			Обсуживание</h2>
		</div>
		<div class="col-lg-9">
			<div class="advantages__desc mb-5">
				 Мы обсуживанием электротранспорт и обычные велописеды в <span class="advantages__desc-red"> Москве, Санкт-Петербурге и Краснодаре. </span> <br>
				 Стоимость при условии аренды 
			</div>
		</div>
		<div class="col-lg-12">
			<div class="priceTable">
				<div class="priceTable__head">
					<div class="priceTable__head-items First">
						 Наименование деталей
					</div>
					<div class="priceTable__head-items Second">
						 Цена детали
					</div>
					<div class="priceTable__head-items Third">
						 Стоимость замены
					</div>
				</div>
				<div class="priceTable__body">
					<div class="priceTable__line">
						<div class="priceTable__line-item First">
 <span class="priceTable__lgText">Камера</span> <span class="priceTable__grayText">Бесплатная диагностика поломок электросамокатов Mijia</span>
						</div>
						<div class="priceTable__line-item Second">
 <span class="priceTable__smText">450₽</span>
						</div>
						<div class="priceTable__line-item Third">
 <span class="priceTable__smText">700₽</span>
						</div>
					</div>
					<div class="priceTable__line">
						<div class="priceTable__line-item First">
 <span class="priceTable__lgText">Покрышка</span> <span class="priceTable__grayText">Аккумуляторная батарея электросамоката</span>
						</div>
						<div class="priceTable__line-item Second">
 <span class="priceTable__smText">700₽</span>
						</div>
						<div class="priceTable__line-item Third">
 <span class="priceTable__smText">700₽</span>
						</div>
					</div>
					<div class="priceTable__line">
						<div class="priceTable__line-item First">
 <span class="priceTable__lgText">Крыло с поддержкой</span> <span class="priceTable__grayText">Электромотор Mijia M365 и PRO</span>
						</div>
						<div class="priceTable__line-item Second">
 <span class="priceTable__smText">500₽</span>
						</div>
						<div class="priceTable__line-item Third">
 <span class="priceTable__smText">300₽</span>
						</div>
					</div>
					<div class="priceTable__line">
						<div class="priceTable__line-item First">
 <span class="priceTable__lgText">Порты зарядки</span> <span class="priceTable__grayText">Зарядка M365</span>
						</div>
						<div class="priceTable__line-item Second">
 <span class="priceTable__smText">500₽</span>
						</div>
						<div class="priceTable__line-item Third">
 <span class="priceTable__smText">400₽</span>
						</div>
					</div>
					<div class="priceTable__line">
						<div class="priceTable__line-item First">
 <span class="priceTable__lgText">Ось узла складывания</span> <span class="priceTable__grayText">Зарядка M365</span>
						</div>
						<div class="priceTable__line-item Second">
 <span class="priceTable__smText">700₽</span>
						</div>
						<div class="priceTable__line-item Third">
 <span class="priceTable__smText">500₽</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12">
			<div class="rent-block rentBlockNew rent-big">
 <img src="/bitrix/templates/motion/images/r3.jpg">
				<div class="rent-block__main rentBlockNew__main">
					<div class="rent-block__name">
						 Выезд мастера по Москве
					</div>
					<div class="rent-block__desc">
						 При аренде транпорта, мы готовы предоставить <br>
						 специалиные условия по обсуживанию, выезд мастер и подменный пранспорт
					</div>
					<div class="rent-block__price">
						 1 000₽ / под ключ<a class="btn" href="#" data-target="#exampleModal5" data-toggle="modal">вызвать мастера</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 </section> <section class="gray">
<div class="container">
	<div class="row d-flex justify-content-between mt-5 pt-5">
		<div class="col-lg-5">
			<div class="advantages__left">
 <img src="/local/templates/motion_new/images/alogo.svg" class="logo">
				<h2 class="title-lg title mb-4">
				Разработка индивидуального решения</h2>
				<div class="advantages__desc">
					 Если вам не подходят наши решения, мы готовы подобрать и подготовить любые ваши решения
				</div>
			</div>
		</div>
		<div class="col-lg-7 d-flex flex-wrap justify-content-between">
			<form class="contacts-form contactsForm">
 <input type="text" name="name" id="name3" placeholder="Ваше имя" required=""> <input class="small-input" type="text" name="phone" id="phone3" placeholder="Номер телефона" required=""> <input class="small-input" type="text" name="mail" id="mail" placeholder="Электронная почта" required=""> <textarea rows="1" placeholder="Сообщение" required=""></textarea> <label class="for-custom d-flex flex-row-reverse align-items-center justify-content-end" for="agree">Я согласен с условиями обработки персональных данных <input class="custom-checkbox" type="checkbox" name="agree" id="agree"> </label>
				<div class="bottom">
				</div>
 <button class="btn" type="submit">Отправить</button>
			</form>
		</div>
	</div>
</div>
 </section> <section class="advantages">
<div class="container">
	<div class="row">
		<div class="col-lg-6">
			<h1 class="title title-lg">
			С нами сотрудничают </h1>
		</div>
		<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-6 col-sm-6">
					<div class="trustIn">
						<div class="trustIn__left">
							<div class="trustIn__brandName">
								 ВкусноHouse
							</div>
							<div class="trustIn__brandDesc">
								 Доставка пиццы, роллов и другой вкусной еды
							</div>
						</div>
						<div class="trustIn__right">
 <img src="/local/templates/motion_new/images/vkus_home.png" alt="">
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-sm-6">
					<div class="trustIn">
						<div class="trustIn__left">
							<div class="trustIn__brandName">
								 «Лента»
							</div>
							<div class="trustIn__brandDesc">
								 Российская сеть гипермаркетов
							</div>
						</div>
						<div class="trustIn__right">
 <img src="/local/templates/motion_new/images/lenta.png" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 </section><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>