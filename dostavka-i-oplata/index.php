<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Условия оплаты и доставки электросамокатов FastMotion");
$APPLICATION->SetPageProperty("description", "Условия и полная информация о доставке и оплате после ремонта или покупки электросамоката в мастерской FastMotion. Хотите узнать как купить электросамокат с доставкой? Заходите сюда.");
$APPLICATION->SetTitle("Оплата и доставка");
?><section class="head-section">
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"bread",
	Array(
		"PATH" => "",
		"SITE_ID" => "s1",
		"START_FROM" => "0"
	)
);?>
		</div>
		<div class="col-lg-12">
			<h1 class="head-section__title">
			<?$APPLICATION->ShowTitle(false)?> </h1>
		</div>
	</div>
</div>
 </section> <section class="white-section">
<div class="container">
	<div class="row d-flex align-items-center justify-content-between">
		<div class="col-md-12">
			<h2 class="advantages__title">Доставка по Москве и Московской области</h2>
			<div class="about-text pt-3">
				<ul>
					<li><strong>Самокаты</strong><br>
					<p class="pt-2">
						 600 руб в пределах МКАД, 1000 руб Московская область (до 15 км за МКАД). Доставка осуществляется ежедневно. <br>
						 При оформлении самоката до 15.00 доставка осуществляется на следующий день.
					</p>
					<p>
						 Новый самокат с комплексными работами может быть доставлен через 2-3 дня после заказа.
					</p>
 </li>
					<li> <strong>Самовывоз</strong>
					<p class="pt-2">
						 Вы всегда можете забрать самокат после подтверждения вашего заказа по адресу улица Строителей 4c2 .
					</p>
 </li>
					<li><strong>Доставка самоката в мастерсую</strong>
					<div class="pt-2">
						 1200 (600) руб в пределах МКАД .Мы можем забрать и/или доставить ваш самокат в пределах МКАД для диагностики и согласованного ремонта.&nbsp;
					</div>
 </li>
					<li><strong>Выезд мастера</strong>
					<div class="pt-2">
						 1500 руб в пределах МКАД + Стоимость ремонта. На выезде мы можем сделать только мелкий ремонт (шиномонтаж, регулировку и т.д)
					</div>
 </li>
				</ul>
			</div>
<div>
	<br/>
	<div class="row d-flex align-items-center justify-content-between">
		<div class="col-md-12">
			<h2 class="advantages__title">Способы оплаты</h2>
			<div class="about-text pt-3">
				<ul>


					<li> <strong>Наличный расчет</strong>
					<p class="pt-2">
						Если товар доставляется курьером, то оплата осуществляется наличными курьеру в руки. При
получении товара обязательно проверьте комплектацию товара, наличие гарантийного талона и
чека
						</p></li>

<li> <strong>Банковской картой</strong>
					<p class="pt-2">
						Для выбора оплаты товара с помощью банковской карты на соответствующей странице
необходимо нажать кнопку Оплата заказа банковской картой. Оплата происходит через ПАО
СБЕРБАНК с использованием банковских карт следующих платёжных систем: 
					</p>

<p class="bank-logo">
	МИР <img src="/images/world.png" alt="" />
</p>

<p class="bank-logo">
VISA International <img src="/images/visa.png" alt="" />
</p>
<p class="bank-logo">
Mastercard Worldwide <img src="/images/mastercard.png" alt="" />
</p>
<p class="bank-logo">
JCB <img src="/images/jsb.png" alt="" />
</p>

					</li>
				</ul>
			</div>

<div><br/>
	<div class="row d-flex align-items-center justify-content-between">
		<div class="col-md-12">
			<h2 class="advantages__title">Возврат товара</h2>
			<div class="about-text pt-3">
				<ul>

<p class="pt-2">
				Срок возврата товара надлежащего качества составляет 30 дней с момента получения товара.
Возврат переведённых средств, производится на ваш банковский счёт в течение 5-30 рабочих дней
(срок зависит от банка, который выдал вашу банковскую карту).
			</p>

</ul>
			</div>

<div><br/>
	<div class="row d-flex align-items-center justify-content-between">
		<div class="col-md-12">
			<h2 class="advantages__title">Регионы России</h2>
			<div class="about-text pt-3">
				<ul>

<p class="pt-2">
				Доставка курьерскими службами СДЭК, DPD, Boxberry, Pickpoint, Почта России. Стоимость рассчитывается после подтверждения заказа. Отправка осуществляется только по 100% предоплате.
			</p>

</ul>
			</div>



<br/>
			<h2 class="advantages__title">Описание процесса передачи данных</h2>
			<p class="pt-3">
				Для оплаты (ввода реквизитов Вашей карты) Вы будете перенаправлены на платёжный шлюз
ПАО СБЕРБАНК. Соединение с платёжным шлюзом и передача информации осуществляется в
защищённом режиме с использованием протокола шифрования SSL. В случае если Ваш банк
поддерживает технологию безопасного проведения интернет-платежей Verified By Visa,
MasterCard SecureCode, MIR Accept, J-Secure для проведения платежа также может
потребоваться ввод специального пароля.
Настоящий сайт поддерживает 256-битное шифрование. Конфиденциальность сообщаемой
персональной информации обеспечивается ПАО СБЕРБАНК. Введённая информация не будет
предоставлена третьим лицам за исключением случаев, предусмотренных законодательством
РФ. Проведение платежей по банковским картам осуществляется в строгом соответствии с
требованиями платёжных систем МИР, Visa Int., MasterCard Europe Sprl, JCB
			</p>
			
		</div>
	</div>
</div>
 </section>
    <style>
        .about-text p {
            margin-bottom: 10px !important;
        }
    </style><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>