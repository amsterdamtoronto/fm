<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Продажа новых и бу электросамокатов от ведущей мастерской Москвы FastMotion в интернет-магазине: Xiaomi Mijia M365, PRO, Kugoo S3, S4, Ninebot ES2, MAX и другие! Хотите купить взрослый электросамокат с гарантией недорого? Обращайтесь! Высокий уровень сервиса и большой выбор электросамокатов на любой вкус специально для вас.");
$APPLICATION->SetPageProperty("title", "Купить взрослый электросамокат в Москве недорого новые и бу");
$APPLICATION->SetTitle("Электросамокаты в Москве");
?>
<section class="head-section main-page-youtube">
    <div class="container">
	    <div class="section-head d-flex align-items-center justify-content-between mt-2 main-page-youtube__all">
		    <h2 class="title mb-0" style="color: #fff;">Видеоблог</h2>
            <a class="btn" href="/blog/" style="display: block;">все публикации</a>
	    </div>
	 <?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "video_main_page",
    Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array("",""),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "3",
		"IBLOCK_TYPE" => "content",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "3",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => $_REQUEST["SECTION_CODE"],
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array("VIDEO",""),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ID",
		"SORT_BY2" => "ID",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "DESC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>
    </div>
</section>
<section class="gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 section-head d-flex align-items-center justify-content-between mb-5">
                <h2 class="title mb-0">
                Магазин </h2>
                <a class="btn" href="/shop/">все товары</a>
            </div>
            <div class="col-lg-12">
                 <?$APPLICATION->IncludeComponent(
        "bitrix:news.list",
        "good_slider",
        Array(
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "ADD_SECTIONS_CHAIN" => "N",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_ADDITIONAL" => "",
            "AJAX_OPTION_HISTORY" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "Y",
            "CACHE_TIME" => "86400",
            "CACHE_TYPE" => "A",
            "CHECK_DATES" => "Y",
            "COMPONENT_TEMPLATE" => "good_slider",
            "DETAIL_URL" => "",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "DISPLAY_TOP_PAGER" => "N",
            "FIELD_CODE" => array(0=>"",1=>"",),
            "FILTER_NAME" => "",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "IBLOCK_ID" => "4",
            "IBLOCK_TYPE" => "content",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "INCLUDE_SUBSECTIONS" => "Y",
            "MESSAGE_404" => "",
            "NEWS_COUNT" => "20",
            "PAGER_BASE_LINK_ENABLE" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => ".default",
            "PAGER_TITLE" => "Новости",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "PREVIEW_TRUNCATE_LEN" => "",
            "PROPERTY_CODE" => array(0=>"PRICE",1=>"",),
            "SET_BROWSER_TITLE" => "N",
            "SET_LAST_MODIFIED" => "N",
            "SET_META_DESCRIPTION" => "N",
            "SET_META_KEYWORDS" => "N",
            "SET_STATUS_404" => "N",
            "SET_TITLE" => "N",
            "SHOW_404" => "N",
            "SORT_BY1" => "ACTIVE_FROM",
            "SORT_BY2" => "SORT",
            "SORT_ORDER1" => "DESC",
            "SORT_ORDER2" => "ASC",
            "STRICT_SECTION_CHECK" => "N"
        )
    );?>
            </div>
        </div>
    </div>
</section>
    <div class="section_slider-new">
        <?$APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "main_slider",
            Array(
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "ADD_SECTIONS_CHAIN" => "N",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "Y",
                "COMPONENT_TEMPLATE" => "main_slider",
                "DETAIL_URL" => "",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "DISPLAY_TOP_PAGER" => "N",
                "FIELD_CODE" => array(0=>"",1=>"DETAIL_PICTURE",2=>"",),
                "FILTER_NAME" => "",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => "5",
                "IBLOCK_TYPE" => "content",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "INCLUDE_SUBSECTIONS" => "Y",
                "MESSAGE_404" => "",
                "NEWS_COUNT" => "20",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "PAGER_TITLE" => "Новости",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "PROPERTY_CODE" => array(0=>"",1=>"HREF",2=>"",),
                "SET_BROWSER_TITLE" => "N",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "N",
                "SHOW_404" => "N",
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_BY2" => "SORT",
                "SORT_ORDER1" => "DESC",
                "SORT_ORDER2" => "ASC",
                "STRICT_SECTION_CHECK" => "N"
            )
        );?>
    </div>
<section class="advantages">
    <div class="container">
        <div class="row d-flex justify-content-between">
            <div class="col-lg-5">
                <div class="advantages__left">
                    <img src="null" class="lazy" data-original="/bitrix/templates/motion/images/alogo.svg">
                    <h1 class="advantages__title">Электросамокаты в Москве</h1>
                    <div class="advantages__desc">
                         Xiaomi, Kugoo, Ninebot Модернизация, тюнинг,&nbsp;запчасти в Москве и Краснодаре.
                    </div>
                    <a class="btn" href="#" data-toggle="modal" data-target="#exampleModal2">КОНСУЛЬТАЦИЯ</a>
                </div>
            </div>
            <div class="col-lg-7 d-flex flex-wrap justify-content-between">
                <div class="advantages__block d-flex flex-column">
                    <img src="null" class="lazy" data-original="/bitrix/templates/motion/images/01.svg">
                    <div class="advantages__t">
                         Cервисные центры
                    </div>
                    <div class="advantages__d">
                         метро Университет <br>
                         в Москве
                    </div>
                </div>
                <div class="advantages__block d-flex flex-column">
                    <img src="null" class="lazy" data-original="/bitrix/templates/motion/images/02.svg">
                    <div class="advantages__t">Запчасти</div>
                    <div class="advantages__d">Свой склад запчстей, гарантирует оперативный ремонт</div>
                </div>
                <div class="advantages__block d-flex flex-column">
                    <img src="null" class="lazy" data-original="/bitrix/templates/motion/images/03.svg">
                    <div class="advantages__t">Гарантия 1 год</div>
                    <div class="advantages__d">На новые самокаты с нашей подготовкой и ремонт</div>
                </div>
                <div class="advantages__block d-flex flex-column">
                    <img src="null" class="lazy" data-original="/bitrix/templates/motion/images/04.svg">
                    <div class="advantages__t">Выездной ремонт</div>
                    <div class="advantages__d">Мастера cделают все работу у вас дома или в офисе</div>
                </div>
            </div>
        </div>
    </div>
</section>
 <section class="gray">
<div class="container">
	 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/includes/service.php"
	)
);?>
</div>
 </section> <section class="white-section">
<div class="container">
	<div class="mt-5">
		 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/includes/tarifs.php"
	)
);?>
	</div>
</div>
 </section> <section class="gray">
<div class="container">
	 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/includes/arenda.php"
	)
);?>
</div>
 </section> <section class="black-section">
<div class="container">
	<div class="row d-flex justify-content-between mb-5">
		<div class="col-lg-8">
			<div class="trade-pre">
				 Trade-In электросамокатов в Mоскве
			</div>
			<h2 class="trade-name">
			Можем взять Ваш самокат в Trade-In </h2>
		</div>
		<div class="col-lg-3">
 <img src="null" class="lazy" data-original="/bitrix/templates/motion/images/Logo.svg">
		</div>
	</div>
	<div class="row d-flex">
		<div class="col-lg-4 mb-4">
			<div class="trade-block d-flex flex-column justify-content-between">
				<div class="trade-block__main">
					<div class="trade-block__title">
						 Я хочу продать вам электросамокат
					</div>
					<div class="trade-block__desc">
						 Мы проведем диагностику и купим у Вас электросамокат по выгодной рыночной стоимости и вы получите сразу деньги.
					</div>
				</div>
				<div class="trade-block__footer d-flex justify-content-between align-items-center">
					<div class="trade-block__name">
						 Оставить заявку
					</div>
 <a class="elipse-btn" href="#" data-toggle="modal" data-target="#exampleModal4"><img src="null" class="lazy" data-original="/bitrix/templates/motion/images/r1.svg"></a>
				</div>
			</div>
		</div>
		<div class="col-lg-4 mb-4">
			<div class="trade-block d-flex flex-column justify-content-between">
				<div class="trade-block__main">
					<div class="trade-block__title">
						 Я хочу купить у вас электросамокат из trade-in&nbsp;
					</div>
					<div class="trade-block__desc">
						 Все самокаты, которые продаются в нашем сервисном центре, проходят обязательную предпродажную подготовку по одному из трех комплексов модернизации.
					</div>
				</div>
				<div class="trade-block__footer d-flex justify-content-between align-items-center">
					<div class="trade-block__name">
						 Типы комплексного обслуживания&nbsp;
					</div>
 <a class="elipse-btn" href="/modernizatsiya/"><img src="null" class="lazy" data-original="/bitrix/templates/motion/images/r1.svg"></a>
				</div>
			</div>
		</div>
		<div class="col-lg-4 mb-4">
			<div class="trade-block d-flex flex-column justify-content-between">
				<div class="trade-block__main">
					<div class="trade-block__title">
						 Я хочу обменять свой электросамокат&nbsp;
					</div>
					<div class="trade-block__desc">
						 Вам необходимо привезти электросамокат к нам в сервисный центр, выбрать новый самокат или из trade-in, доплатить разницу в стоимости.
					</div>
				</div>
				<div class="trade-block__footer d-flex justify-content-between align-items-center">
					<div class="trade-block__name">
						 Адрес сервис центра
					</div>
 <a class="elipse-btn" href="/kontakty/"><img src="null" class="lazy" data-original="/bitrix/templates/motion/images/r1.svg"></a>
				</div>
			</div>
		</div>
	</div>
</div>
 </section> <section class="white-section">
<div class="container">
	 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"masters",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "masters",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "1",
		"IBLOCK_TYPE" => "content",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "2",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"AUTHOR",1=>"DATE",2=>"",),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>
</div>
 </section>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/includes/testi.php"
	)
);?> <section class="white-section">
<div class="container">
	<div class="row about-text pt-5">
		<div class="col-lg-6 pr-5">
			<h2 class="title mb-3">
			Купить электросамокат в Москве </h2>
			<p>
				 В нашем <a href="/shop/">интернет-магазине</a> вы найдете самый широкий ассортимент электросамокатов. Мы напрямую сотрудничаем с мировыми брендами, предлагая своим клиентам лучшие цены и качественное сервисное обслуживание. На сайте представлены новые и поддержанные устройства в отличном состоянии. Также вы можете взять электросамокат напрокат, чтобы оценить все преимущества экологически чистого транспорта нового поколения и определиться с выбором модели. Не обязательно сразу покупать scooter и вносить 100% стоимости – вы можете взять его в рассрочку или в кредит.
			</p>
			<p>
				 В мастерской Fast-motion осуществляется ремонт, обслуживание и модернизация. Тут профессионально занимаются тюнингом новых и бу электросамокатов, применяя оригинальные детали и комплектующие. В результате клиенты получают безопасные и надежные транспортные средства, соответствующие современным требованиям и стандартам.
			</p>
			<p>
				 Если не знаете, где купить электросамокаты по выгодной цене, тогда обратите внимание на предложение интернет-магазина Fast-motion. Тут представлены современные модели на любой вкус и предпочтение. Выбирайте транспортное средство для себя по определенным критериям, а мы обеспечим высокий уровень сервиса и максимально быструю доставку.
			</p>
			<h2 class="title mb-3">
			Цены на модели для взрослых </h2>
			<p>
				 Если не знаете, сколько стоит электросамокат с необходимыми вам функциями, тогда ознакомьтесь с каталогом нашего магазина. Базовая модель обойдется в 25 000 рублей (Ninebot KickScooter E22). Данный вариант предполагает стандартный набор характеристик:
			</p>
			<ul>
				<li>ручной тормоз;</li>
				<li>цвет – черный;</li>
				<li>вес- 13 кг;</li>
				<li>предельная нагрузка – 100 кг;</li>
				<li> пробег на одном заряде – 22 км;</li>
				<li> мощность – 300 Вт;</li>
				<li>складная рама из алюминиевого сплава.</li>
			</ul>
			<p>
				 Это стоящий недорогой вариант для города. А вот для продолжительных путешествий и активного отдыха стоит присмотреть другую модель. Ninebot Max стоит 55 000 рублей, но его пробег на одном заряде уже составляет 55 км – этого уже вполне достаточно для довольно длительных поездок. Максимальная нагрузка увеличена до 160 кг. Аккумулятор заряжается всего 7 часов. Это электросамокат с большими колесами (диаметр составляет 10 дюймов).
			</p>
			<p>
				 Отличной альтернативой этой модели станет Xiaomi M365 PRO. Пробег на одном заряде немного меньше предыдущего вариант (45 км), но цена значительно ниже – 37 000 рублей. M365 PRO оснащена подножкой, дисковыми тормозами и ручным тормозом. В рейтинге электросамокатов 2020 года эта модель занимает почетное место. Xiaomi M365 PRO станет отличным выбором для тех, кто хочет недорого купить в Москве экологически чистое транспортное средство с внушительным набором функций.
			</p>
			<p>
				 В магазине представлены и другие, не менее стоящие электросамокаты нового поколения. На все модели предоставляется гарантия сроком на 1 год. Систематически проводятся распродажи, чтобы вы могли максимально сэкономить при покупке транспортного средства своей мечты. Вся продукция интернет-магазина на 100% оригинальна. При проведении тюнинга, обслуживания и ремонта применяются фирменные комплектующие, имеющие сертификаты качества и соответствия.
			</p>
			<p>
				 Заказывайте электросамокаты в интернет-магазине или обращайтесь в мастерскую Fast-motion, чтобы модернизировать свое транспортное средство. Наши специалисты выполнят все необходимые процедуры для эффективной и длительной работы устройства.
			</p>
			<h2 class="title mb-3">
			Новый и б/у </h2>
			<p>
				 В нашем каталоге представлены новые и бу электросамокаты, которые находятся в прекрасном техническом состоянии. Все модели обслуживаются и ремонтируются в нашей мастерской, поэтому мы на 100% уверенны в их исправности и безопасности. С транспортными средствами работают лучшие мастера, которые прошли особую подготовку и специализируются на конкретных брендах. При ремонте, обслуживании и тюнинге профессионалы четко следуют инструкциям и рекомендациям от производителей электросамокатов. Это позволяет достигнуть предельной точности и избежать ошибок.
			</p>
			<p>
				 Стоит учитывать, что даже новое транспортное средство нуждается во внимании мастера. Любой электросамокат перед эксплуатацией необходимо герметизировать. Также важной процедурой является настройка параметров в соответствии с потребностями и запросами пользователя.
			</p>
			<p>
				 Не сомневайтесь – покупайте электросамокаты в интернет-магазине Fast-motion, чтобы достигнуть свободы и независимости. С таким транспортным средством вы сможете перемещаться по городу без вреда для окружающей среды.
			</p>
			<h2 class="title mb-3">
			Выбираем электросамокат для города в интернет-магазине </h2>
			<p>
				 Не знаете, как выбрать электросамокат своей мечты? Знакомьтесь с ассортиментом интернет-магазина Fast-motion, в котором представлены модели с различными характеристиками и ценами. Разнообразие вариантов позволит подобрать транспортное средство с учетом потребностей и финансовых возможностей.
			</p>
			<p>
				 Воспользовавшись консультацией специалиста, вы сможете выбрать электросамокат по таким параметрам:
			</p>
			<ul>
				<li>запас хода;</li>
				<li>конструктивные особенности (складные, с сидением, с подножкой);</li>
				<li>тип камеры колес;</li>
				<li>предназначение (для взрослых, для детей);</li>
				<li>мощность;</li>
				<li>максимальная скорость;</li>
				<li>вес;</li>
				<li>тип тормоза;</li>
				<li>максимальная нагрузка;</li>
				<li>цвет;</li>
				<li>рама;</li>
				<li>бренд;</li>
				<li>год выпуска.</li>
			</ul>
			<p>
				 Если вы ищите самый легкий, быстрый и мощный электросамокат, тогда ознакомьтесь с каталогом интернет-магазина Fast-motion. Тут представлены краткие обзоры для каждой модели, которые позволят быстро изучить и сравнить характеристики. Выбираем по скорости, мощности и нагрузке – обращайтесь к нашим специалистам, если возникли дополнительные вопросы или трудности.
			</p>
		</div>
		<div class="col-lg-6">
			<h2 class="title mb-3">
			Ассортимент интернет-магазина Fast-motion </h2>
			<p>
				 В каталоге интернет-магазина Fast-motion представлены такие модели электросамокатов:
			</p>
			<ul>
				<li>Xiaomi Mijia M365;</li>
				<li> Xiaomi Mijia PRO;</li>
				<li>Kugoo S3;</li>
				<li>Kugoo S3 PRO;</li>
				<li>Kugoo M4;</li>
				<li>Kugoo M4 PRO;</li>
				<li>Ninebot ES2;</li>
				<li>Ninebot ES1;</li>
				<li>Ninebot ES4;</li>
				<li>Ninebot MAX.</li>
			</ul>
			<p>
				 Вышеперечисленные электросамокаты входят в топ лучших моделей на современном рынке. Мы подобрали для вас транспортные средства от лучших брендов, поэтому не сомневайтесь и смело выбирайте вариант на сайте.
			</p>
			<p>
				 Среди электросамокатов этих производителей можно выбрать взрослые и детские модели. Все они соответствуют строгим требованиям безопасности и надежности. Некоторыми из них можно управлять через мобильное приложение, что существенно упрощает задачу пользователю. Модели про и макс рассчитаны на более продолжительные путешествия. Они имеют увеличенный запас хода и выгодно выделяться среди других видов мощностью.
			</p>
			<p>
				 Выбирайте лучший kickscooter для себя – обретайте свободу перемещения по городу и путешествуйте с комфортом. С таким спутником вы сможете быстро и легко передвигаться по московским дорогам, забыв про пробки и угнетающий общественный транспорт.
			</p>
			<h2 class="title mb-3">
			Мастерская Fast-motion </h2>
			<p>
				 В мастерской Fast-motion работают лучшие специалисты по ремонту и тюнингу электросамокатов. Они используют современное оборудование и оригинальные комплектующие известных брендов (ксиоми, нанабот, куга). Профессионалы своего дела занимаются не только обслуживанием экологически безопасных транспортных средств. Они обучают начинающих мастеров и проводят мастер-классы по повышению квалификации. Это говорит только об одном - в Fast-motion работают лучшие из лучших.
			</p>
			<p>
				 Таким специалистам вы можете без всяческих сомнений доверить свое транспортное средство.
			</p>
			<p>
				 Мастерская Fast-motion предоставляет такие виды услуг:
			</p>
			<ul>
				<li>тюнинг нового и бу электросамоката;</li>
				<li>обслуживание;</li>
				<li>настройка нового электросамоката в соответствии с потребностями пользователя;</li>
				<li>герметизация;</li>
				<li>тюнинг.</li>
			</ul>
			<p>
				 При модернизации транспортных средств используются последние технологии и инновационные идеи. Это позволяет клиентам получать максимум пользы от тюнинга, чтобы использовать возможности электросамоката на все 100%.
			</p>
			<p>
				 Также в мастерской вы сможете приобрести зарядное для электросамоката или другие необходимые комплектующие. Заказывайте детали только в проверенных местах, чтобы ошибиться с выбором и получить консультацию профессионала.
			</p>
			<p>
				 Воспользуйтесь услугами мастерской Fast-motion, чтобы довести приобретенный электросамокат до совершенства или обновить свое транспортное средство бу. На все виды работ предоставляется гарантия, поэтому вы ничем не рискуете при сотрудничестве с Fast-motion.
			</p>
			<h2 class="title mb-3">
			Аренда электросамокатов </h2>
			<p>
				 Fast-motion предоставляет еще одну полезную услугу – аренду электросамокатов. Данное предложение актуально для тех, кто хочет взять понравившуюся модель на тест-драйв или провести тематическое мероприятие.
			</p>
			<p>
				 Прокат электросамоката – это отличная возможность ближе познакомиться с транспортом нового поколения. За этими технологиями будущее, поэтому не отказывайте себе в удовольствии первым оценить все их преимущества.
			</p>
			<h2 class="title mb-3">
			Почему стоит купить электросамокат в Fast-Motion? </h2>
			<p>
				 В Fast-Motion мы максимально стараемся информировать вас по вопросам новинок на рынке электросамокатов. На сайте вы можете ознакомиться с фото и видео, чтобы визуально оценить нашу продукцию.
			</p>
			<p>
				 Мы развиваем свой блог на ютуб, чтобы вы всегда могли первыми узнавать про новые модели и инновационные идеи. С помощью него вы сможете подробнее изучить особенности каждой модели и даже посмотреть на понравившийся электросамокат в действии.
			</p>
			<p>
				 Клиенты Fast-Motion могут оценить такие преимущества сотрудничества:
			</p>
			<ul>
				<li>широкий ассортимент электросамокатов;</li>
				<li>собственная профессиональная мастерская;</li>
				<li>лучшие цены на рынке;</li>
				<li>предоставление консультаций по выбору модели;</li>
				<li>возможность аренды;</li>
				<li>доступность рассрочки;</li>
				<li>работа с оригинальными комплектующими;</li>
				<li>предоставление гарантии;</li>
				<li>высокие стандарты сервисного обслуживания.</li>
			</ul>
			<p>
				 Fast-Motion – это лидер на рынке электросамокатов. Знакомьтесь с ассортиментом продукции на сайте и заказывайте транспортное средство у лучшего поставщика, чтобы получить максимум выгод.
			</p>
		</div>
	</div>
</div>
 </section><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>