<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Сотрудничество с Fast-Motion");
$APPLICATION->SetPageProperty("description", "Условия сотрудничества с FastMotion.");
$APPLICATION->SetTitle("Сотрудничество с компанией FastMotion");
?>


    <section class="head-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "bread", Array(
                        "PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
                        "SITE_ID" => "s1",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
                        "START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
                    ),
                        false
                    );?>
                </div>
                <div class="col-lg-12">
                    <h1 class="head-section__title"><?$APPLICATION->ShowTitle(false)?></h1><a class="btn" href="#">БЕСПЛАТНАЯ КОНСУЛЬТАЦИЯ</a>
                </div>
            </div>
        </div>
    </section>
    <section class="white-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p>Мы ориентированы на современного потребителя. Мы предлагаем качественную сертифицированную технику.</p>
                    <p>В своей деятельности мы придерживаемся позиции честности, открытости, порядочности. Мы стремимся не просто заинтересовать партнеров, а построить стабильные партнерские отношения.</p>
                </div>
            </div>
            <div class="row about-text mt-5 mb-5">
                <div class="col-lg-12 title">Мы гарантируем</div>
                <div class="col-lg-4 mb-4">
                    <div class="conditions-block">
                        <div class="conditions-block__top d-flex align-items-center"><span>1</span>
                            <div class="conditions-block__name">Гарантия</div>
                        </div>
                        <div class="conditions-block__text">Осуществлять гарантийное и постгарантийное обслуживание;</div>
                    </div>
                </div>
                <div class="col-lg-4 mb-4">
                    <div class="conditions-block">
                        <div class="conditions-block__top d-flex align-items-center"><span>2</span>
                            <div class="conditions-block__name">Документация</div>
                        </div>
                        <div class="conditions-block__text">Предоставлять техническую документацию</div>
                    </div>
                </div>
                <div class="col-lg-4 mb-4">
                    <div class="conditions-block">
                        <div class="conditions-block__top d-flex align-items-center"><span>3</span>
                            <div class="conditions-block__name">Продукция</div>
                        </div>
                        <div class="conditions-block__text">Предоставлять полноценную информацию о продукции</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <p>С нами выгодно сотрудничать, поскольку мы имеем свой собственный склад, на котором находится до 90% продукции. Это позволяет вести безостановочный торговый процесс, удовлетворяя без промедления запросы потребителей.</p>
                    <p>Наши постоянно повышают свою квалификацию, совершенствуют профессиональные навыки. Если вы стремитесь стать успешными, обращайтесь в наш интернет-магазин, мы предложим выгодные условия сотрудничества, которые позволят реализовать ваши даже самые амбициозные планы.</p>
                </div>
            </div>
        </div>
    </section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>