<?php

use Bitrix\Main\Page\Asset;

$url = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];


$i = Asset::getInstance();
global $APPLICATION;
?>
<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1 user-scalable=no">
    <title><?= $APPLICATION->ShowTitle() ?></title>
    <meta name="yandex-verification" content="990ba5d82695e283"/>
    <link rel="icon" type="image/png" href="/favicon.png"/>
    <meta name="google-site-verification" content="HfpKeeh4x_xgOJslMvjwo6BvZJbT722RMuHFO-QRL8o"/>

    <?php
    $i->addCss(SITE_TEMPLATE_PATH . '/bower_components/swiper/dist/css/swiper.css');
    $i->addCss(SITE_TEMPLATE_PATH . '/bower_components/bootstrap/dist/css/bootstrap.css');
    $i->addCss(SITE_TEMPLATE_PATH . '/bower_components/Magnific-Popup/dist/magnific-popup.css');
    //  $i->addCss(SITE_TEMPLATE_PATH . '/bower_components/jquery/jquery-ui.css');
    $i->addCss(SITE_TEMPLATE_PATH . '/bower_components/mmenu/dist/mmenu-light.css');
    //$i->addCss('https://vjs.zencdn.net/7.6.6/video-js.css');
    $i->addCss(SITE_TEMPLATE_PATH . '/bower_components/styler/dist/jquery.formstyler.css');
    $i->addCss(SITE_TEMPLATE_PATH . '/bower_components/styler/dist/jquery.formstyler.theme.css');
    $i->addCss('https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800&amp;display=swap&amp;subset=cyrillic');
    $i->addCss(SITE_TEMPLATE_PATH . '/css/jquery.mCustomScrollbar.min.css');
    $i->addCss(SITE_TEMPLATE_PATH . '/css/main.css');
    $i->addCss(SITE_TEMPLATE_PATH . '/css/custom.css');

    $i->addJs(SITE_TEMPLATE_PATH . '/js/jquery-1.12.4.min.js');

    $i->addJs(SITE_TEMPLATE_PATH . '/js/jquery.maskedinput.min.js');
    //	$i->addJs('https://code.jquery.com/ui/1.12.1/jquery-ui.js');
    $i->addJs(SITE_TEMPLATE_PATH . '/bower_components/jquery.lazyload.min.js');

    $i->addJs(SITE_TEMPLATE_PATH . '/bower_components/mmenu/dist/mmenu-light.js');
    $i->addJs(SITE_TEMPLATE_PATH . '/bower_components/bootstrap/dist/js/bootstrap.js');
    $i->addJs(SITE_TEMPLATE_PATH . '/bower_components/Magnific-Popup/dist/jquery.magnific-popup.js');
    $i->addJs(SITE_TEMPLATE_PATH . '/bower_components/swiper/dist/js/swiper.js');
    $i->addJs(SITE_TEMPLATE_PATH . '/bower_components/styler/dist/jquery.formstyler.min.js');
    $i->addJs(SITE_TEMPLATE_PATH . '/js/jquery.mCustomScrollbar.concat.min.js');
    $i->addJs('https://kit.fontawesome.com/3d56b3c686.js');
    //  $i->addJs('https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js');
    //  $i->addJs('https://vjs.zencdn.net/7.6.6/video.js');
    // $i->addJs(SITE_TEMPLATE_PATH . '/js/yt.js');
    $i->addJs(SITE_TEMPLATE_PATH . '/js/main.js');
    $i->addJs(SITE_TEMPLATE_PATH . '/js/custom.js');
    ?>
    <?
    $APPLICATION->ShowHead();
    ?>

    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-KKGFJNR');</script>
    <!-- End Google Tag Manager -->

    <script data-ad-client="ca-pub-2781247375543461" async
            src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KKGFJNR"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="panel">
    <?= $APPLICATION->ShowPanel() ?>
</div>
<header>
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 d-flex justify-content-between align-items-center">
                    <? $APPLICATION->IncludeComponent("bitrix:menu", "li", Array(
                        "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
                        "CHILD_MENU_TYPE" => "top",    // Тип меню для остальных уровней
                        "DELAY" => "N",    // Откладывать выполнение шаблона меню
                        "MAX_LEVEL" => "1",    // Уровень вложенности меню
                        "MENU_CACHE_GET_VARS" => array(    // Значимые переменные запроса
                            0 => "",
                        ),
                        "MENU_CLASS" => 'header-top__menu d-flex',
                        "MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
                        "MENU_CACHE_TYPE" => "N",    // Тип кеширования
                        "MENU_CACHE_USE_GROUPS" => "Y",    // Учитывать права доступа
                        "ROOT_MENU_TYPE" => "top",    // Тип меню для первого уровня
                        "USE_EXT" => "N",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
                    ),
                        false
                    ); ?>

                    <div class="header-yandex">
                        <iframe src="https://yandex.ru/sprav/widget/rating-badge/172166944624?type=rating&theme=dark" width="150" height="50" frameborder="0" style="border-radius: 6px;"></iframe>
                    </div>

                    <ul class="header-top__social d-flex align-items-center">
                      <!--  <li>
                            <a href="https://www.instagram.com/fastmotionelectric/">
                                <img class="lazy" data-original="<?/*= SITE_TEMPLATE_PATH */?>/images/in.svg">
                            </a>
                        </li>-->
                        <li>
                            <a href="https://www.tiktok.com/@fastmotionelectric/">
                                <img class="lazy" data-original="<?= SITE_TEMPLATE_PATH ?>/images/tiktok.svg">
                            </a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/c/fastmotionelectric">
                                <img class="lazy" data-original="<?= SITE_TEMPLATE_PATH ?>/images/yt.svg">
                            </a>
                        </li>
                        <li>
                            <a href="https://t.me/FastMotionChannel">
                                <img class="lazy" data-original="/local/templates/motion_new/images/tg.svg"
                                     src="/local/templates/motion_new/images/tg.svg" style="display: inline;">
                                <span>Канал</span>
                            </a>
                        </li>
                        <li>
                            <a href="https://t.me/FastMotion">
                                <img class="lazy" data-original="/local/templates/motion_new/images/tg.svg"
                                     src="/local/templates/motion_new/images/tg.svg" style="display: inline;">
                                <span>Чат по Xiaomi</span>
                            </a>
                        </li>
                        <li>
                            <a href="https://t.me/NINEBOT_ES">
                                <img class="lazy" data-original="/local/templates/motion_new/images/tg.svg"
                                     src="/local/templates/motion_new/images/tg.svg" style="display: inline;">
                                <span>Чат по Nineboot_ES/E</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="main-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 d-flex align-items-center justify-content-between">
                    <div class="header-logo main-header__left d-flex align-items-center">

                        <? if ($APPLICATION->GetCurPage(false) !== '/'){ ?>
                        <a class="logo" href="/">
                            <? } ?>

                            <img src="<?= SITE_TEMPLATE_PATH ?>/images/Logo.svg">

                            <? if ($APPLICATION->GetCurPage(false) !== '/'){ ?>
                        </a>
                    <? } ?>

                        <span class="logo-desc">Ремонт и продажа <br>электросамокатов</span></div>
                    <div class="main-header__right d-flex align-items-center">
                        <div class="graffic d-flex flex-column">
                            <span>
                                <div class="adres-hover">
                                    <div class="adres-hover__action">
                                        Адреса мастерских в москве
                                          <i class="fas fa-caret-down ml-3"></i>
                                    </div>

                                    <div class="adres-hover-block">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:main.include",
                                                "",
                                                Array(
                                                    "AREA_FILE_SHOW" => "file",
                                                    "AREA_FILE_SUFFIX" => "inc",
                                                    "EDIT_TEMPLATE" => "",
                                                    "PATH" => "/includes/adres.php"
                                                )
                                            ); ?>
                                    </div>
                                </div>
                            </span>
                            <span class="small">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "AREA_FILE_SHOW" => "file",
                                        "AREA_FILE_SUFFIX" => "inc",
                                        "EDIT_TEMPLATE" => "",
                                        "PATH" => "/includes/worktime.php"
                                    )
                                ); ?>
                            </span>
                        </div>
                        <div class="phone d-flex flex-column"><? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "AREA_FILE_SHOW" => "file",
                                    "AREA_FILE_SUFFIX" => "inc",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/includes/phone.php"
                                )
                            ); ?><? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "AREA_FILE_SHOW" => "file",
                                    "AREA_FILE_SUFFIX" => "inc",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/includes/email.php"
                                )
                            ); ?></div>
                        <a class="header-mobile__phone" href="tel:+7 495 133 59 40">
                            <svg width="23" height="23" viewBox="0 0 23 23" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path d="M16.5301 23.0001C15.7876 23.0001 15.0515 22.8688 14.3316 22.6073C11.1603 21.4553 8.21673 19.5789 5.81889 17.1812C3.42105 14.7834 1.54474 11.8397 0.392762 8.66842C0.0225752 7.64927 -0.0866706 6.59724 0.0681692 5.54144C0.213216 4.55248 0.596295 3.5924 1.17604 2.76497C1.75834 1.9339 2.53465 1.24267 3.42101 0.766023C4.36622 0.257752 5.3979 0 6.48748 0C6.82636 0 7.11924 0.236774 7.19026 0.568105L8.31839 5.8327C8.36946 6.07109 8.29619 6.31914 8.12384 6.49155L6.19618 8.41916C8.01463 12.0346 10.9654 14.9853 14.5808 16.8038L16.5084 14.8761C16.6808 14.7038 16.9289 14.6306 17.1673 14.6816L22.4319 15.8097C22.7632 15.8807 23 16.1736 23 16.5125C23 17.6021 22.7422 18.6338 22.2339 19.579C21.7572 20.4654 21.066 21.2417 20.2349 21.824C19.4076 22.4037 18.4475 22.7868 17.4585 22.9318C17.1486 22.9773 16.8387 23 16.5301 23.0001V23.0001ZM5.91323 1.46889C4.476 1.62755 3.20036 2.38086 2.35326 3.58988C1.40068 4.94939 1.17855 6.62155 1.74383 8.17767C3.95857 14.2745 8.72554 19.0415 14.8224 21.2562C16.3785 21.8215 18.0507 21.5994 19.4102 20.6468C20.6192 19.7997 21.3725 18.524 21.5312 17.0868L17.2484 16.1691L15.2353 18.1822C15.0211 18.3965 14.6956 18.4534 14.4215 18.3246C10.1455 16.3157 6.68432 12.8545 4.6755 8.57858C4.54671 8.30444 4.60358 7.97894 4.8178 7.76476L6.83094 5.75162L5.91323 1.46889Z"
                                      fill="white"/>
                                <path d="M22.2813 12.2183C21.8844 12.2183 21.5626 11.8965 21.5626 11.4996C21.5626 5.95129 17.0487 1.43745 11.5005 1.43745C11.1035 1.43745 10.7817 1.11564 10.7817 0.718723C10.7817 0.321808 11.1035 0 11.5005 0C14.5721 0 17.4599 1.19613 19.6319 3.36811C21.8039 5.54009 23 8.42788 23 11.4996C23 11.8965 22.6783 12.2183 22.2813 12.2183Z"
                                      fill="#FF0D35"/>
                                <path d="M19.4064 12.2181C19.0095 12.2181 18.6877 11.8963 18.6877 11.4994C18.6877 7.53639 15.4635 4.3122 11.5005 4.3122C11.1035 4.3122 10.7817 3.99039 10.7817 3.59348C10.7817 3.19656 11.1035 2.87476 11.5005 2.87476C16.2561 2.87476 20.1251 6.74377 20.1251 11.4994C20.1251 11.8963 19.8034 12.2181 19.4064 12.2181Z"
                                      fill="#FF0D35"/>
                                <path d="M16.5315 12.2183C16.1346 12.2183 15.8128 11.8965 15.8128 11.4995C15.8128 9.12169 13.8783 7.1872 11.5005 7.1872C11.1035 7.1872 10.7817 6.86539 10.7817 6.46848C10.7817 6.07156 11.1035 5.74976 11.5005 5.74976C14.6709 5.74976 17.2502 8.32907 17.2502 11.4995C17.2502 11.8965 16.9285 12.2183 16.5315 12.2183Z"
                                      fill="#FF485A"/>
                            </svg>
                        </a>
                        <a class="btn" href="#" data-toggle="modal" data-target="#exampleModal">запись на ремонт</a><a
                                class="burger" href="#menu"><img class="lazy"
                                                                 data-original="<?= SITE_TEMPLATE_PATH ?>/images/menu.svg"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-menu">
        <div class="container">
            <div class="row">
                <? $APPLICATION->IncludeComponent("bitrix:menu", "li", Array(
                    "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
                    "CHILD_MENU_TYPE" => "top",    // Тип меню для остальных уровней
                    "DELAY" => "N",    // Откладывать выполнение шаблона меню
                    "MAX_LEVEL" => "1",    // Уровень вложенности меню
                    "MENU_CACHE_GET_VARS" => array(    // Значимые переменные запроса
                        0 => "",
                    ),
                    "MENU_CLASS" => 'col-lg-12 main-menu__item d-flex align-items-center justify-content-between',
                    "MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
                    "MENU_CACHE_TYPE" => "N",    // Тип кеширования
                    "MENU_CACHE_USE_GROUPS" => "Y",    // Учитывать права доступа
                    "ROOT_MENU_TYPE" => "main",    // Тип меню для первого уровня
                    "USE_EXT" => "N",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
                ),
                    false
                ); ?>
            </div>
        </div>
    </div>
    <div style="display: none">
        <nav id="menu" style="display: none;">
            <ul class="first">
                <? $APPLICATION->IncludeComponent("bitrix:menu", "li", Array(
                    "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
                    "CHILD_MENU_TYPE" => "top",    // Тип меню для остальных уровней
                    "DELAY" => "N",    // Откладывать выполнение шаблона меню
                    "MAX_LEVEL" => "1",    // Уровень вложенности меню
                    "MENU_CACHE_GET_VARS" => array(    // Значимые переменные запроса
                        0 => "",
                    ),
                    "WITHOUT_UL" => true,
                    "MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
                    "MENU_CACHE_TYPE" => "N",    // Тип кеширования
                    "MENU_CACHE_USE_GROUPS" => "Y",    // Учитывать права доступа
                    "ROOT_MENU_TYPE" => "top",    // Тип меню для первого уровня
                    "USE_EXT" => "N",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
                ),
                    false
                ); ?>
                <? $APPLICATION->IncludeComponent("bitrix:menu", "li", Array(
                    "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
                    "CHILD_MENU_TYPE" => "top",    // Тип меню для остальных уровней
                    "DELAY" => "N",    // Откладывать выполнение шаблона меню
                    "MAX_LEVEL" => "1",    // Уровень вложенности меню
                    "MENU_CACHE_GET_VARS" => array(    // Значимые переменные запроса
                        0 => "",
                    ),
                    "WITHOUT_UL" => true,
                    "MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
                    "MENU_CACHE_TYPE" => "N",    // Тип кеширования
                    "MENU_CACHE_USE_GROUPS" => "Y",    // Учитывать права доступа
                    "ROOT_MENU_TYPE" => "main",    // Тип меню для первого уровня
                    "USE_EXT" => "N",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
                ),
                    false
                ); ?>

                <div class="phone d-flex flex-column">
                    <a href="tel:+7 495 133 59 40">+7 495 133 59 40</a>
                </div>
                <div class="adress d-flex flex-column">
                    <div class="adres-hover-block" style="display: block">
                        ул. Строителей 4с2
                    </div>
                    <a class="mail" href="mailto:office@fast-motion.ru">office@fast-motion.ru</a>
                </div>
                <div class="mobile-massanger d-flex align-items-center flex-wrap">
                    <span>Подписывайтесь</span>
                    <div class="icons d-flex flex-wrap">
                      <!--  <a href="https://www.instagram.com/fastmotionelectric/">
                            <img src="<?/*= SITE_TEMPLATE_PATH */?>/images/inb.svg">
                        </a>-->
                        <a href="https://www.tiktok.com/@fastmotionelectric/">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/images/ttblack.svg">
                        </a>
                        <a href="https://www.youtube.com/c/fastmotionelectric">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/images/ytb.svg">
                        </a>
                    </div>
                    <div class="d-flex m-link__telegram">
                        <a href="https://t.me/FastMotionChannel">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/images/tgb.svg">
                            <span>Канал</span>
                        </a>

                        <a href="https://t.me/FastMotion">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/images/tgb.svg">
                            <span>Чат по Xiaomi</span>
                        </a>
                        <a href="https://t.me/NINEBOT_ES">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/images/tgb.svg">
                            <span>Чат по Nineboot_ES/E</span>
                        </a>
                    </div>
                </div>
            </ul>
        </nav>
    </div>

</header>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title" id="exampleModalLabel">Запись на ремонт</div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="ajax-form">
                    <input type="hidden" name="subject" value="Запись на ремонт">
                    <input type="hidden" name="url" value="<?= $url ?>">
                    <input type="hidden" name="goal" value="remont">

                    <div class="form-group">
                        <label for="exampleInputPhone1">Ваше имя</label>
                        <input type="name" name="name" class="form-control" id="exampleInputName1"
                               aria-describedby="nameHelp">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPhone1">Ваш телефон</label>
                        <input type="phone" required name="phone" class="form-control phone_mask"
                               id="exampleInputPhone1">
                    </div>


                    <div class="form-group">
                        <label>Предпочтительный способ связи</label>
                        <select class="form-control" name="sv">
                            <option>Телефон</option>
                            <option>WhatsUp</option>
                            <option>Viber</option>
                            <option>Telegram</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <small>Оставьте свои данные и мы вам перезвоним</small>
                    </div>
                    <button type="submit" class="btn btn-primary">Отправить</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title" id="exampleModalLabel">Консультация</div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="ajax-form">
                    <input type="hidden" name="subject" value="Консультация">

                    <input type="hidden" name="goal" value="consult">
                    <input type="hidden" name="url" value="<?= $url ?>">
                    <div class="form-group">
                        <label for="exampleInputPhone2">Ваше имя</label>
                        <input type="name" name="name" class="form-control" id="exampleInputName2"
                               aria-describedby="nameHelp">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPhone2">Ваш телефон</label>
                        <input type="phone" required name="phone" class="form-control phone_mask"
                               id="exampleInputPhone2">
                    </div>

                    <div class="form-group">
                        <label>Предпочтительный способ связи</label>
                        <select class="form-control" name="sv">
                            <option>Телефон</option>
                            <option>WhatsUp</option>
                            <option>Viber</option>
                            <option>Telegram</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <small>Оставьте свои данные и мы вам перезвоним</small>
                    </div>
                    <button type="submit" class="btn btn-primary">Отправить</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title" id="exampleModalLabel">Тариф</div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="ajax-form">
                    <input type="hidden" name="subject" value="Заявка на тариф">
                    <input type="hidden" name="url" value="<?= $url ?>">

                    <input type="hidden" name="goal" value="order-tarif">
                    <div class="form-group">
                        <label for="exampleInputPhone3">Ваше имя</label>
                        <input type="name" name="name" class="form-control" id="exampleInputName3"
                               aria-describedby="nameHelp">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPhone3">Ваш телефон</label>
                        <input type="phone" name="phone" class="form-control phone_mask" id="exampleInputPhone3">
                    </div>

                    <div class="form-group">
                        <label>Предпочтительный способ связи</label>
                        <select class="form-control" name="sv">
                            <option>Телефон</option>
                            <option>WhatsUp</option>
                            <option>Viber</option>
                            <option>Telegram</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <small>Оставьте свои данные и мы вам перезвоним</small>
                    </div>
                    <button type="submit" class="btn btn-primary">Отправить</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title" id="exampleModalLabel">Оставить заявку</div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="ajax-form">
                    <input type="hidden" name="subject" value="Оставить заявку">
                    <input type="hidden" name="url" value="<?= $url ?>">

                    <input type="hidden" name="goal" value="order">
                    <div class="form-group">
                        <label for="exampleInputPhone4">Ваше имя</label>
                        <input type="name" name="name" class="form-control" id="exampleInputName4"
                               aria-describedby="nameHelp">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPhone4">Ваш телефон</label>
                        <input type="phone" required name="phone" class="form-control phone_mask"
                               id="exampleInputPhone4">
                    </div>
                    <!--                    <div class="form-group">-->
                    <!--                        <label for="exampleFormControlTextarea1">Сообщение</label>-->
                    <!--                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>-->
                    <!--                    </div>-->
                    <div class="form-group">
                        <label>Предпочтительный способ связи</label>
                        <select class="form-control" name="sv">
                            <option>Телефон</option>
                            <option>WhatsUp</option>
                            <option>Viber</option>
                            <option>Telegram</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <small>Оставьте свои данные и мы вам перезвоним</small>
                    </div>
                    <button type="submit" class="btn btn-primary">Отправить</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title" id="exampleModalLabel">Заказать самокат</div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="ajax-form">
                    <input type="hidden" name="subject" value="Заказать самокат">
                    <input id="nameOfProduct" type="hidden" name="product" value="">
                    <input type="hidden" name="url" value="<?= $url ?>">

                    <input type="hidden" name="goal" value="order-item">
                    <div class="form-group">
                        <label for="exampleInputPhone5">Ваше имя</label>
                        <input type="name" name="name" class="form-control" id="exampleInputName5"
                               aria-describedby="nameHelp">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPhone5">Ваш телефон</label>
                        <input type="phone" name="phone" class="form-control phone_mask" id="exampleInputPhone5">
                    </div>
                    <div class="form-group">
                        <label>Предпочтительный способ связи</label>
                        <select class="form-control" name="sv">
                            <option>Телефон</option>
                            <option>WhatsUp</option>
                            <option>Viber</option>
                            <option>Telegram</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <small>Оставьте свои данные и мы вам перезвоним</small>
                    </div>
                    <button type="submit" class="btn btn-primary">Заказать</button>
                </form>
            </div>
        </div>
    </div>
</div>