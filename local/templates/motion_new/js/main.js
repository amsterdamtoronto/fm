"use strict";

function genSvg() {
    jQuery("img.svg").each(function () {
        var e = jQuery(this), i = e.attr("id"), t = e.attr("class"), a = e.attr("src");
        jQuery.get(a, function (a) {
            var r = jQuery(a).find("svg");
            "undefined" != typeof i && (r = r.attr("id", i)), "undefined" != typeof t && (r = r.attr("class", t + " replaced-svg")), r = r.removeAttr("xmlns:a"), e.replaceWith(r)
        }, "xml")
    })
}

$(function () {
    genSvg();
    var e = (
        new Swiper("#main-slider", {
            slidesPerView: 1,
            spaceBetween: 0,
            watchSlidesProgress: true,
            mousewheelControl: true,
            keyboardControl: true,
            effect: 'fade',
            observer: !0,
            speed: 1e3,
            observeParents: !0,
            pagination: {el: ".swiper-pagination", clickable: !0},
            autoplay: {delay: 5e3, disableOnInteraction: !0}
        }),
            new Swiper("#item-slider", {
            slidesPerView: 3,
            spaceBetween: 30,
            observer: !0,
            observeParents: !0,
            pagination: {el: ".swiper-pagination", clickable: !0},
            autoplay: {delay: 5e3, disableOnInteraction: !1},
            breakpoints: {900: {slidesPerView: 2}, 650: {slidesPerView: 1}}
        }), new Swiper("#docs-slider", {
            slidesPerView: 5,
            spaceBetween: 30,
            observer: !0,
            observeParents: !0,
            autoHeight: !0,
            navigation: {nextEl: ".docs .swiper-button-next", prevEl: ".docs .swiper-button-prev"},
            autoplay: {delay: 5e3, disableOnInteraction: !1},
            breakpoints: {975: {slidesPerView: 4}, 860: {slidesPerView: 3}, 620: {slidesPerView: 2}}
        }), new Swiper(".gallery-thumbs", {
            spaceBetween: 20,
            slidesPerView: 3,
            freeMode: !0,
            watchSlidesVisibility: !0,
            watchSlidesProgress: !0,
            direction: "vertical",
            autoplay: {delay: 3e3}
        })), i = (new Swiper(".gallery-top", {
            spaceBetween: 10,
            thumbs: {swiper: e}
        }), new MmenuLight(document.querySelector("#menu"), "all")),
        t = (i.navigation({selectedClass: "Selected", slidingSubmenus: !0, title: ""}), i.offcanvas({}));
    document.querySelector('a[href="#menu"]').addEventListener("click", function (e) {
        e.preventDefault(), t.open()
    }), $(document).ready(function () {
        $(".popup-gallery").magnificPopup({
            delegate: "a",
            type: "image",
            tLoading: "Loading image #%curr%...",
            mainClass: "mfp-img-mobile",
            gallery: {enabled: !0, navigateByImgClick: !0, preload: [0, 1]},
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.', titleSrc: function (e) {
                    return e.el.attr("title")
                }
            }
        })
    })
}), function (e) {
    e(function () {
        e("select").styler()
    })
}(jQuery);

$(function () {
    $('.ajax-form').submit(function (event) {

        event.preventDefault();
        var g = $(this).find('input[name=goal]').val();
        var d = $(this).serialize(),
            f = $(this);

        var dataValid = {};
        $(this).find('input').each(function () {
            dataValid[this.name] = $(this).val();
        });
        //Обязятельное поле phone
        if (dataValid['phone'].length !== 16) {
            $(this).find('input[name="phone"]').addClass('is-invalid')
        } else if (dataValid['phone'].length === 16){
            $(this).find('input[name="phone"]').removeClass('is-invalid')

            if (g) {
                ym(62180977, 'reachGoal', g)
            }
            $.ajax({
                type: 'post',
                url: '/mailer.php',
                data: d,
                success: function (res) {
                    f.html('<p>Ваша заявка успешно отправлена</p>');
                }
            });
        }

        return false;
    });
    $(".phone_mask").mask("+7(999) 999-9999");
});

$(document).ready(function () {
    $('.popup-gallery2').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
            titleSrc: function (item) {
                return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
            }
        }
    });

    $(document).scroll(function () {
        var s_top = $("html, body").scrollTop();
        var blocks = $(".ajax-load-block");
        blocks.each(function (index, el) {
            if (!$(el).hasClass('loaded') && s_top + 1000 > $(el).offset().top) {

                $(el).addClass('loaded');
                $.ajax({
                    type: 'get',
                    url: $(el).data('lhref'),
                    success: function (res) {
                        $(el).html(res);
                    }
                });
            }

        });
    });

    $("img.lazy").lazyload({
        effect: "fadeIn"
    });
});


'use strict';

function r(f) {
    /in/.test(document.readyState) ? setTimeout('r(' + f + ')', 9) : f()
}

r(function () {
    if (!document.getElementsByClassName) {
        // Поддержка IE8
        var getElementsByClassName = function (node, classname) {
            var a = [];
            var re = new RegExp('(^| )' + classname + '( |$)');
            var els = node.getElementsByTagName("*");
            for (var i = 0, j = els.length; i < j; i++)
                if (re.test(els[i].className)) a.push(els[i]);
            return a;
        }
        var videos = getElementsByClassName(document.body, "youtube");
    } else {
        var videos = document.getElementsByClassName("youtube");
    }
    var nb_videos = videos.length;
    for (var i = 0; i < nb_videos; i++) {
        // Находим постер для видео, зная ID нашего видео
        videos[i].style.backgroundImage = 'url(https://i.ytimg.com/vi/' + videos[i].id + '/sddefault.jpg)';
        // Размещаем над постером кнопку Play, чтобы создать эффект плеера
        var play = document.createElement("div");
        play.setAttribute("class", "play");
        videos[i].appendChild(play);
        videos[i].onclick = function () {

            /**
             *
             * <iframe width="560" height="315"
             * src="https://www.youtube.com/embed/R4FQHgmLFSI"
             * frameborder="0"
             * allow="accelerometer;
             * autoplay;
             * clipboard-write;
             * encrypted-media;
             * gyroscope;
             * picture-in-picture"
             * allowfullscreen></iframe>
             *
             * allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
             * */
                // Создаем iFrame и сразу начинаем проигрывать видео, т.е. атрибут autoplay у видео в значении 1
            var iframe = document.createElement("iframe");
            var iframe_url = "https://www.youtube.com/embed/" + this.id + "?autoplay=1&autohide=1";
            if (this.getAttribute("data-params")) iframe_url += '&' + this.getAttribute("data-params");
            iframe.setAttribute("src", iframe_url);
            iframe.setAttribute("frameborder", '0');
            iframe.setAttribute("allow", 'accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope;');
            iframe.setAttribute("allowfullscreen", '');

            // Высота и ширина iFrame будет как у элемента-родителя
            iframe.style.width = this.style.width;
            iframe.style.height = this.style.height;
            // Заменяем начальное изображение (постер) на iFrame
            this.parentNode.replaceChild(iframe, this);
        }
    }
});
