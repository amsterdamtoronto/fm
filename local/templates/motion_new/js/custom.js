function initCustomScroll() {
    $(".js-custom-scroll").mCustomScrollbar();
}

var swipers;
function initTuningSlider() {
    swipers = new Swiper('.js-tuning-slider', {
        speed: 400,
        spaceBetween: 60,
        slidesPerView: 1,
        navigation: {
            nextEl: '.js-tuning-slider-button-next',
            prevEl: '.js-tuning-slider-button-prev',
        }
    });

    for (let i = 0; i < swipers.length; i++) {
        const swiper = swipers[i];

        swiper.on('slideChange', function () {
            setTuningFixedTopVariant($(this.slides[this.activeIndex]).find('.js-tuning-slider-header').html());
        });
    }

    $('.js-tuning-slider-button-prev-double').on('click', function(e) {
        e.preventDefault();
        $('.js-tuning-slider.is-active .js-tuning-slider-button-prev').click();
    });

    $('.js-tuning-slider-button-next-double').on('click', function(e) {
        e.preventDefault();
        $('.js-tuning-slider.is-active .js-tuning-slider-button-next').click();
    });
}

function setTuningFixedTopVariant(html) {
    var headerFixed = $('.js-tuning-slider-header-fixed');
    if (headerFixed.length) {
        headerFixed.html(html);
    }
}

function getTuningVariant() {
    return $('.js-tuning-slider.is-active .swiper-slide-active .js-tuning-slider-header').html();
}

function updateTuningSlider() {
    for (let i = 0; i < swipers.length; i++) {
        const swiper = swipers[i];

        swiper.update();
    }
}

function initTuningSelector() {
    $('.js-tuning-selector-btn').on('click', function(e) {
        e.preventDefault();
        //$(this).closest('.js-tuning-selector').addClass('is-active');
        $('.js-tuning-selector').addClass('is-active');
    });

    $('.js-tuning-selector-close').on('click', function(e) {
        e.preventDefault();
        //$(this).closest('.js-tuning-selector').removeClass('is-active');
        $('.js-tuning-selector').removeClass('is-active');
    });

    $('.js-tuning-select-model').on('click', function(e) {
        e.preventDefault();
        var id = $(this).data('tuning');
        var tuning = $(this).closest('.js-tuning');
        var containers = tuning.find('.js-tuning-container');
        var selectModelTexts = tuning.find('.js-tuning-select-model-selected');
        containers.removeClass('is-active');
        containers.filter('[data-tuning='+id+']').addClass('is-active');
        selectModelTexts.html('');
        selectModelTexts.filter('[data-tuning='+id+']').html('Выбрано');
        updateTuningSlider();
        setTuningFixedTopVariant(getTuningVariant());

        $(this).closest('.js-tuning-selector').removeClass('is-active');
    });
}

function initTuningFixedTop() {
    var fixedtop = $('.js-tuning-fixed-top');
    var container = $('.js-tuning-fixed-top-container');
    var offset = 170;

    $(window).on('scroll', function(){
        if (!fixedtop.length || !container.length) return;
        ;
        var topContainer = container.offset().top;
        var bottomContainer = topContainer + container.height();
        var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        var scrollBottom = scrollTop + $(window).height();
    
        if (scrollTop > topContainer + offset && scrollBottom < bottomContainer) {
            fixedtop.addClass('is-active');
        } else {
            fixedtop.removeClass('is-active');
        }
    });
}


$(document).ready(function(){
    initTuningSlider();
    initCustomScroll();
    initTuningSelector();
    initTuningFixedTop();


    $('#showGoTo').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Кнопка, запускающая модальное окно
        var youtube = button.data('youtube') // Извлечь информацию из атрибутов data- *
        // При необходимости Вы можете инициировать здесь запрос AJAX (а затем выполнить обновление в обратном вызове).
        // Обновите содержимое модального окна. Здесь мы будем использовать jQuery, но вместо этого Вы можете использовать библиотеку привязки данных или другие методы.
        if(youtube){
            var createWrap = document.createElement("div");
            createWrap.setAttribute("class", "wrapYoutube");
            var modal = $(this);
            var body =  modal.find('.modal-body');



            var createYoutube = document.createElement("div");

            createYoutube.setAttribute("class", "youtube youtubePopUp");
            createYoutube.setAttribute("id", youtube);

            createWrap.appendChild(createYoutube);

            $(body).append(createWrap);
            r(function () {
                if (!document.getElementsByClassName) {
                    // Поддержка IE8
                    var getElementsByClassName = function (node, classname) {
                        var a = [];
                        var re = new RegExp('(^| )' + classname + '( |$)');
                        var els = node.getElementsByTagName("*");
                        for (var i = 0, j = els.length; i < j; i++)
                            if (re.test(els[i].className)) a.push(els[i]);
                        return a;
                    }
                    var videos = getElementsByClassName(document.body, "youtube");
                } else {
                    var videos = document.getElementsByClassName("youtube");
                }
                var nb_videos = videos.length;
                for (var i = 0; i < nb_videos; i++) {
                    // Находим постер для видео, зная ID нашего видео
                    videos[i].style.backgroundImage = 'url(https://i.ytimg.com/vi/' + videos[i].id + '/sddefault.jpg)';
                    // Размещаем над постером кнопку Play, чтобы создать эффект плеера
                    var play = document.createElement("div");
                    play.setAttribute("class", "play");
                    videos[i].appendChild(play);
                    videos[i].onclick = function () {

                        /**
                         *
                         * <iframe width="560" height="315"
                         * src="https://www.youtube.com/embed/R4FQHgmLFSI"
                         * frameborder="0"
                         * allow="accelerometer;
                         * autoplay;
                         * clipboard-write;
                         * encrypted-media;
                         * gyroscope;
                         * picture-in-picture"
                         * allowfullscreen></iframe>
                         *
                         * allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                         * */
                            // Создаем iFrame и сразу начинаем проигрывать видео, т.е. атрибут autoplay у видео в значении 1
                        var iframe = document.createElement("iframe");
                        var iframe_url = "https://www.youtube.com/embed/" + this.id + "?autoplay=1&autohide=1";
                        if (this.getAttribute("data-params")) iframe_url += '&' + this.getAttribute("data-params");
                        iframe.setAttribute("src", iframe_url);
                        iframe.setAttribute("frameborder", '0');
                        iframe.setAttribute("allow", 'accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope;');
                        iframe.setAttribute("allowfullscreen", '');

                        // Высота и ширина iFrame будет как у элемента-родителя
                        iframe.style.width = this.style.width;
                        iframe.style.height = this.style.height;
                        // Заменяем начальное изображение (постер) на iFrame
                        this.parentNode.replaceChild(iframe, this);
                    }
                }
            });
        }else {
           var clone = $(button).find('.data .swiper-slide').clone(); // создаем копию первого элемента формы
                     // вставляем клонированный
            var modal = $(this);
            var body =  modal.find('.modal-body .swiper-wrapper');
            clone.appendTo(body);

            new Swiper("#popUpSlider", {
                slidesPerView: 1,
                spaceBetween: 0,
                watchSlidesProgress: true,
                mousewheelControl: true,
                keyboardControl: true,
                navigation: {nextEl: ".swiper-button-next", prevEl: ".swiper-button-prev"},

                observer: !0,
                speed: 1e3,
                observeParents: !0,
                pagination: {el: ".swiper-pagination", clickable: !0},
                autoplay: {delay: 5e3, disableOnInteraction: !0}
            })
        }

    });


    $('#showGoTo').on('hide.bs.modal', function (event) {
            var modal = $(this);
             modal.find('.modal-body .wrapYoutube').remove();
        modal.find('.modal-body .swiper-slide').remove();

    });





});