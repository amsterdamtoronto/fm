<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 d-flex justify-content-between main-footer">
                <div class="footer-block">

<? if ($APPLICATION->GetCurPage(false) !== '/'){ ?>
<a class="logo" href="#">
	<?} ?>
<img class="lazy" data-original="<?=SITE_TEMPLATE_PATH?>/images/Logo.svg">

<? if ($APPLICATION->GetCurPage(false) !== '/'){ ?>
</a>
					<? }?>
                    <div class="footer-block__desc">Ремонт и продажа электросамокатов</div><a class="btn" href="#"  data-toggle="modal" data-target="#exampleModal">запись на ремонт</a>
                    <ul class="social">
                       <!-- <li><a href="https://www.instagram.com/fastmotionelectric/"><img class="lazy" data-original="<?/*=SITE_TEMPLATE_PATH*/?>/images/in.svg"></a></li>-->
                        <li><a href="https://t.me/FastMotion"><img class="lazy" data-original="<?=SITE_TEMPLATE_PATH?>/images/tg.svg"></a></li>
                        <li><a href="https://www.youtube.com/c/fastmotionelectric"><img class="lazy" data-original="<?=SITE_TEMPLATE_PATH?>/images/yt.svg"></a></li>
                    </ul>

					<ul class="polity-list">
						<li><a href="/privacy-policy/">Политика конфиденциальности</a></li>
						<li><a href="/personal-data/">Обработка персональных данных</a></li>
					</ul>
                </div>
                <div class="footer-block">
                    <?$APPLICATION->IncludeComponent("bitrix:menu", "li", Array(
                        "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                        "CHILD_MENU_TYPE" => "left_bottom",	// Тип меню для остальных уровней
                        "DELAY" => "N",	// Откладывать выполнение шаблона меню
                        "MAX_LEVEL" => "1",	// Уровень вложенности меню
                        "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
                            0 => "",
                        ),
                        "MENU_CLASS" => 'footer-block__menu',
                        "MENU_CACHE_TIME" => "3600",	// Время кеширован ия (сек.)
                        "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                        "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                        "ROOT_MENU_TYPE" => "left_bottom",	// Тип меню для первого уровня
                        "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                    ),
                        false
                    );?>
                </div>
                <div class="footer-block">

                    <?$APPLICATION->IncludeComponent("bitrix:menu", "li", Array(
                        "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                        "CHILD_MENU_TYPE" => "bottom",	// Тип меню для остальных уровней
                        "DELAY" => "N",	// Откладывать выполнение шаблона меню
                        "MAX_LEVEL" => "1",	// Уровень вложенности меню
                        "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
                            0 => "",
                        ),
                        "MENU_CLASS" => 'footer-block__menu',
                        "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                        "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                        "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                        "ROOT_MENU_TYPE" => "bottom",	// Тип меню для первого уровня
                        "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                    ),
                        false
                    );?>
                </div>

                <div class="footer-block">
                    <div class="footer-block__line">
                        <div class="footer-block__mini">Пн-Сб 11:00-19:00 (Вс выходной)</div>
                        <div class="footer-block__lg">Москва, Университет, ул. Строителей 4с2</div>
                    </div>
                    <div class="footer-block__line">
						<div class="footer-block__mini">Сервисный центр</div><a class="footer-block__lg" href="tel:+7 495 133-59-40">+7 495 133-59-40</a>
                    </div>
                    <div class="footer-block__line">
						<div class="footer-block__mini">По всем вопросам</div><a class="footer-block__lg" href="mailto:office@fast-motion.ru">office@fast-motion.ru</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row d-flex justify-content-between align-items-center mt-5 footer-bottom">
            <div class="col-lg-4">
                <div class="copyright">FastMotion © 2022 Все права защищены.</div>
            </div>
            <div class="col-lg-4 d-flex align-items-center dev">
                <div class="developer">Разработка и продвижение <a href="https://dublingroup.ru/">DublinGroup.ru</a></div><img src="<?=SITE_TEMPLATE_PATH?>/images/dg.svg">
            </div>
        </div>
    </div>
</footer>
<script src="//code-ya.jivosite.com/widget/0vXH57CEJD" async></script>