<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="black-slider">
    <div class="swiper-container" id="main-slider">
        <div class="swiper-wrapper">

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

    <div class="swiper-slide" style="background-image: url(<?=$arItem["DETAIL_PICTURE"]["SRC"]?>)" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-8 col-12 slider-content">
                    <div class="slider-content__logo"><img class="lazy" data-original="<?=CFile::GetPath($arItem["PROPERTIES"]["BRAND"]["VALUE"])?>?>"></div>
                    <div class="slider-content__title"><?=$arItem["DETAIL_TEXT"]?></div><a class="elipse-btn" href="<?=$arItem["PROPERTIES"]["HREF"]["VALUE"]?>"><img class="lazy" data-original="<?=SITE_TEMPLATE_PATH?>/images/r1.svg"></a>
                </div>
            </div>
        </div>
    </div>
<?endforeach;?>

        </div>
        <div class="swiper-pagination"></div>
    </div>
</section>