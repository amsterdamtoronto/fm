<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? if(!isset($arParams['HIDE_TITLE'])) { ?>
<div class="row">
    <div class="col-lg-12 section-head d-flex align-items-center justify-content-between mb-5">
        <h2 class="title mb-0">Обучение</h2><a class="btn" href="/obuchenie/">все курсы</a>
    </div>
</div>
<? } ?>
<div class="row">

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

    <div class="col-lg-6 col-md-6">
        <div class="master-block d-flex"  id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <div class="master-block__left">
                <div class="master-block__date"><?=$arItem["PROPERTIES"]["DATE"]["VALUE"]?></div>
                <div class="master-block__name"><?=$arItem["NAME"]?></div>
                <div class="master-block__desc"><?=$arItem["PROPERTIES"]["AUTHOR"]["VALUE"]?></div><a class="btn" href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>">Подробнее</a>
            </div>
            <div class="master-block__right d-flex align-items-end">

            </div>
        </div>
    </div>
<?endforeach;?>
</div>
