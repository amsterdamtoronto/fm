<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

$currentModel = current(current($arResult['CATEGORIES'])['ITEMS']);
?>

<?if($arResult['CATEGORIES']):?>
	<div class="tuning js-tuning js-tuning-fixed-top-container">
		<div class="tuning-top">
			<div class="row">
				<div class="col-12 col-lg-4">
					<div class="tuning-header">
						<div class="row">
							<div class="col-xxs-12 col-6 col-md-5 col-lg-12">
								<?foreach($arResult['CATEGORIES'] as $arCategory):?>
									<?foreach($arCategory['ITEMS'] as $arModel):?>
										<div class="tuning-header__model js-tuning-container<?=($arModel['ID'] == $currentModel['ID']) ? ' is-active' : ''?>" data-tuning="<?=$arModel['ID']?>">
											<div class="row">
												<div class="d-none d-sm-block col-4">
													<div class="tuning-header__img">
														<?if($arModel['IMAGE']['NORMAL']['src']):?>
															<img src="<?=$arModel['IMAGE']['NORMAL']['src']?>" alt="">
														<?else:?>
															<img src="<?=$this->GetFolder().'/images/none.png'?>" alt="">
														<?endif?>
													</div>
												</div>
												<div class="col-12 col-sm-8">
													<div class="tuning-header__body-container">
														<div class="tuning-header__body">
															<div class="tuning-header__subtitle">Выбранная модель</div>
															<div class="tuning-header__title"><?=$arModel['NAME']?></div>
														</div>
													</div>
												</div>
											</div>
										</div>
									<?endforeach?>
								<?endforeach?>
							</div>
							<div class="col-xxs-12 col-6 col-md-7 col-lg-12">
								<div class="tuning-header__selector-container">
									<div class="tuning-header__selector tuning-selector js-tuning-selector">
										<a class="btn-selector js-tuning-selector-btn" href="#">
											<span class="d-none d-md-inline">Выбрать другую модель</span>
											<span class="d-md-none">Изменить</span>
											<img src="<?=$this->GetFolder().'/images/chevron-down.svg'?>" alt="">
										</a>
										<div class="tuning-header-selector__overlay tuning-selector-overlay js-tuning-selector-close"></div>
										<div class="tuning-header-selector__popup js-custom-scroll tuning-selector-popup">
											<div class="tuning-header-selector__models">
												<div class="tuning-header-selector__models-title">Выберите модель</div>
												<?foreach($arResult['CATEGORIES'] as $arCategory):?>
													<div class="tuning-header-selector__model">
														<div class="tuning-header-selector__model-name">Модели <?=$arCategory['NAME']?></div>
														<div class="tuning-header-selector__items">
															<?foreach($arCategory['ITEMS'] as $arModel):?>
																<a href="#" class="tuning-header-selector__item js-tuning-select-model" data-tuning="<?=$arModel['ID']?>">
																	<span class="tuning-header-selector__item-img">
																		<?if($$arModel['IMAGE']['NORMAL']):?>
																			<img src="<?=$$arModel['IMAGE']['SMALL']['src']?>" alt="">
																		<?else:?>
																			<img src="<?=$this->GetFolder().'/images/none.png'?>" alt="">
																		<?endif?>
																	</span>
																	<span class="tuning-header-selector__item-name"><?=$arModel['NAME']?></span>
																	<span class="tuning-header-selector__item-selected js-tuning-select-model-selected" data-tuning="<?=$arModel['ID']?>"><?=($arModel['ID'] == $currentModel['ID']) ? 'Выбрано' : ''?></span>
																</a>
															<?endforeach?>
														</div>
													</div>
												<?endforeach?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="d-none d-lg-block col-8">
					<?foreach($arResult['CATEGORIES'] as $arCategory):?>
						<?foreach($arCategory['ITEMS'] as $arModel):?>
							<div class="tuning-variants-headers js-tuning-container<?=($arModel['ID'] == $currentModel['ID']) ? ' is-active' : ''?>" data-tuning="<?=$arModel['ID']?>">
								<div class="row">
									<?foreach($arModel['VARIANTS'] as $arVariant):?>
										<div class="col-4">
											<div class="tuning-variants-header">
												<div class="tuning-variants-header__info-container">
													<div class="tuning-variants-header__info">
														<div class="tuning-variants-header__name"><?=$arVariant['NAME']?></div>
														<div class="tuning-variants-header__price"><?=number_format(intval($arVariant['PROPERTIES']['PRICE']['VALUE']), 0, '.', ' ')?>₽</div>
													</div>
												</div>
												<div class="tuning-variants-header__buy">
													<a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
												</div>
											</div>
										</div>
									<?endforeach?>
								</div>
							</div>
						<?endforeach?>
					<?endforeach?>
				</div>
			</div>
		</div>
		<div class="tuning-bottom d-none d-lg-block">
			<?foreach($arResult['CATEGORIES'] as $arCategory):?>
				<?foreach($arCategory['ITEMS'] as $arModel):?>
					<div class="tuning-bottom__properties js-tuning-container<?=($arModel['ID'] == $currentModel['ID']) ? ' is-active' : ''?>" data-tuning="<?=$arModel['ID']?>">
						<?foreach($arModel['PROPERTIES'] as $arProperty):?>
							<div class="tuning-bottom__property">
								<div class="row">
									<div class="col-4">
										<div class="tuning-bottom__property-title"><?=$arProperty['NAME']?></div>
									</div>
									<div class="col-8">
										<div class="tuning-bottom__property-variants">
											<div class="row">
												<?foreach($arModel['VARIANTS'] as $arVariant):?>
													<div class="col-4">
														<div class="tuning-bottom__property-value">
															<?if($arVariant['DISPLAY_PROPERTIES'][$arProperty['CODE']]['VALUE_XML_ID'] == 'Y'):?>
																<img src="<?=$this->GetFolder().'/images/check.svg'?>" alt="">
															<?elseif($arVariant['DISPLAY_PROPERTIES'][$arProperty['CODE']]['VALUE_XML_ID'] == 'N'):?>
																<img src="<?=$this->GetFolder().'/images/close.svg'?>" alt="">
															<?else:?>
																<?=$arVariant['DISPLAY_PROPERTIES'][$arProperty['CODE']]['VALUE']?>
															<?endif?>
														</div>
													</div>
												<?endforeach?>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?endforeach?>
					</div>
				<?endforeach?>
			<?endforeach?>
		</div>
		<div class="tuning-bottom tuning-bottom-mobile d-lg-none">
			<?foreach($arResult['CATEGORIES'] as $arCategory):?>
				<?foreach($arCategory['ITEMS'] as $arModel):?>
					<div class="tuning-bottom-mobile__variants js-tuning-slider js-tuning-container<?=($arModel['ID'] == $currentModel['ID']) ? ' is-active' : ''?>" data-tuning="<?=$arModel['ID']?>">
						<div class="swiper-wrapper">
							<?foreach($arModel['VARIANTS'] as $arVariant):?>
								<div class="tuning-bottom-mobile__variant swiper-slide">
									<div class="tuning-bottom-mobile__variant-header js-tuning-slider-header">
										<div class="tuning-variants-header__name tuning-bottom-mobile__variant-name"><?=$arVariant['NAME']?></div>
										<div class="tuning-variants-header__price tuning-bottom-mobile__variant-price"><?=number_format(intval($arVariant['PROPERTIES']['PRICE']['VALUE']), 0, '.', ' ')?>₽</div>
									</div>
									<div class="tuning-bottom-mobile__properties">
										<?foreach($arModel['PROPERTIES'] as $arProperty):?>
											<div class="tuning-bottom__property tuning-bottom-mobile__property">
												<div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
													<?if($arVariant['DISPLAY_PROPERTIES'][$arProperty['CODE']]['VALUE_XML_ID'] == 'Y'):?>
														<img src="<?=$this->GetFolder().'/images/check.svg'?>" alt="">
													<?elseif($arVariant['DISPLAY_PROPERTIES'][$arProperty['CODE']]['VALUE_XML_ID'] == 'N'):?>
														<img src="<?=$this->GetFolder().'/images/close.svg'?>" alt="">
													<?else:?>
														<?=$arVariant['DISPLAY_PROPERTIES'][$arProperty['CODE']]['VALUE']?>
													<?endif?>
												</div>
												<div class="tuning-bottom__property-title"><?=$arProperty['NAME']?></div>
											</div>
										<?endforeach?>
									</div>
									<div class="tuning-variants-header__buy">
										<a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
									</div>
								</div>
							<?endforeach?>
						</div>
						<div class="tuning-slider-button-prev swiper-button-prev js-tuning-slider-button-prev">
							<img src="<?=$this->GetFolder().'/images/chevron-left.svg'?>" alt="">
						</div>
						<div class="tuning-slider-button-next swiper-button-next js-tuning-slider-button-next">
							<img src="<?=$this->GetFolder().'/images/chevron-right.svg'?>" alt="">
						</div>
					</div>
				<?endforeach?>
			<?endforeach?>
		</div>
		<div class="tuning-top js-tuning-fixed-top tuning-fixed-top d-md-none">
			<div class="tuning-bottom-mobile__variant-header js-tuning-slider-header-fixed">
				<div class="tuning-variants-header__name tuning-bottom-mobile__variant-name"><?=current($currentModel['VARIANTS'])['NAME']?></div>
				<div class="tuning-variants-header__price tuning-bottom-mobile__variant-price"><?=number_format(intval(current($currentModel['VARIANTS'])['PROPERTIES']['PRICE']['VALUE']), 0, '.', ' ')?>₽</div>
			</div>
			<div class="tuning-slider-button-prev swiper-button-prev js-tuning-slider-button-prev-double">
				<img src="<?=$this->GetFolder().'/images/chevron-left.svg'?>" alt="">
			</div>
			<div class="tuning-slider-button-next swiper-button-next js-tuning-slider-button-next-double">
				<img src="<?=$this->GetFolder().'/images/chevron-right.svg'?>" alt="">
			</div>
			<div class="tuning-header">
				<div class="row">
					<div class="col-xxs-12 col-6 col-md-5 col-lg-12">
						<?foreach($arResult['CATEGORIES'] as $arCategory):?>
							<?foreach($arCategory['ITEMS'] as $arModel):?>
								<div class="tuning-header__model js-tuning-container<?=($arModel['ID'] == $currentModel['ID']) ? ' is-active' : ''?>" data-tuning="<?=$arModel['ID']?>">
									<div class="row">
										<div class="d-none d-sm-block col-4">
											<div class="tuning-header__img">
												<?if($arModel['IMAGE']['NORMAL']['src']):?>
													<img src="<?=$arModel['IMAGE']['NORMAL']['src']?>" alt="">
												<?else:?>
													<img src="<?=$this->GetFolder().'/images/none.png'?>" alt="">
												<?endif?>
											</div>
										</div>
										<div class="col-12 col-sm-8">
											<div class="tuning-header__body-container">
												<div class="tuning-header__body">
													<div class="tuning-header__subtitle">Выбранная модель</div>
													<div class="tuning-header__title"><?=$arModel['NAME']?></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							<?endforeach?>
						<?endforeach?>
					</div>
					<div class="col-xxs-12 col-6 col-md-7 col-lg-12">
						<div class="tuning-header__selector-container">
							<div class="tuning-header__selector tuning-selector js-tuning-selector">
								<a class="btn-selector js-tuning-selector-btn" href="#">
									<span class="d-none d-md-inline">Выбрать другую модель</span>
									<span class="d-md-none">Изменить</span>
									<img src="<?=$this->GetFolder().'/images/chevron-down.svg'?>" alt="">
								</a>
								<?/*<div class="tuning-header-selector__overlay tuning-selector-overlay js-tuning-selector-close"></div>
								<div class="tuning-header-selector__popup js-custom-scroll tuning-selector-popup">
									<div class="tuning-header-selector__models">
										<div class="tuning-header-selector__models-title d-sm-none">Выберите модель</div>
										<?foreach($arResult['CATEGORIES'] as $arCategory):?>
											<div class="tuning-header-selector__model">
												<div class="tuning-header-selector__model-name">Модели <?=$arCategory['NAME']?></div>
												<div class="tuning-header-selector__items">
													<?foreach($arCategory['ITEMS'] as $arModel):?>
														<a href="#" class="tuning-header-selector__item js-tuning-select-model" data-tuning="<?=$arModel['ID']?>">
															<span class="tuning-header-selector__item-img">
																<?if($$arModel['IMAGE']['NORMAL']):?>
																	<img src="<?=$$arModel['IMAGE']['SMALL']['src']?>" alt="">
																<?else:?>
																	<img src="<?=$this->GetFolder().'/images/none.png'?>" alt="">
																<?endif?>
															</span>
															<span class="tuning-header-selector__item-name"><?=$arModel['NAME']?></span>
															<span class="tuning-header-selector__item-selected js-tuning-select-model-selected" data-tuning="<?=$arModel['ID']?>"><?=($arModel['ID'] == $currentModel['ID']) ? 'Выбрано' : ''?></span>
														</a>
													<?endforeach?>
												</div>
											</div>
										<?endforeach?>
									</div>
								</div>*/?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?endif?>