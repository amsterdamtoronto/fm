<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach ($arResult['ITEMS'] as $key => $arItem) {
    if ($arItem['IBLOCK_SECTION_ID']) {
        $arSectionsItems[$arItem['IBLOCK_SECTION_ID']]['VARIANTS'][$arItem['ID']] = $arItem;
        if (!$arSectionsItems[$arItem['IBLOCK_SECTION_ID']]['PROPERTIES']) {
            $arSectionsItems[$arItem['IBLOCK_SECTION_ID']]['PROPERTIES'] = array();
        }

        foreach ($arItem['DISPLAY_PROPERTIES'] as $arProperty) {
            if (!$arSectionsItems[$arItem['IBLOCK_SECTION_ID']]['PROPERTIES'][$arProperty['CODE']]) {
                $arSectionsItems[$arItem['IBLOCK_SECTION_ID']]['PROPERTIES'][$arProperty['CODE']] = array(
                    'NAME' => $arProperty['NAME'],
                    'CODE' => $arProperty['CODE'],
                );
            }
        }
    }
}

if (!$arSectionsItems)
    return;

$rsSectionLvl2 = CIBlockSection::GetList(array("SORT" => "ASC"), array('ID' => array_keys($arSectionsItems), 'ACTIVE' => 'Y'), false, array('ID', 'IBLOCK_SECTION_ID', 'NAME', 'PICTURE'));
while ($arSectionLvl2 = $rsSectionLvl2->Fetch()) {
    if ($arSectionLvl2['IBLOCK_SECTION_ID']) {
        $arSectionLvl2['IMAGE']['NORMAL'] = CFile::ResizeImageGet($arSectionLvl2['PICTURE'], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
        $arSectionLvl2['IMAGE']['SMALL'] = CFile::ResizeImageGet($arSectionLvl2['PICTURE'], array('width' => 30, 'height' => 30), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);

        $arSectionsSections[$arSectionLvl2['IBLOCK_SECTION_ID']]['ITEMS'][$arSectionLvl2['ID']] = $arSectionLvl2;
        $arSectionsSections[$arSectionLvl2['IBLOCK_SECTION_ID']]['ITEMS'][$arSectionLvl2['ID']]['VARIANTS'] = $arSectionsItems[$arSectionLvl2['ID']]['VARIANTS'];
        $arSectionsSections[$arSectionLvl2['IBLOCK_SECTION_ID']]['ITEMS'][$arSectionLvl2['ID']]['PROPERTIES'] = $arSectionsItems[$arSectionLvl2['ID']]['PROPERTIES'];
    }
}

$rsSectionLvl1 = CIBlockSection::GetList(array("SORT" => "ASC"), array('ID' => array_keys($arSectionsSections), 'ACTIVE' => 'Y'), false, array('ID', 'IBLOCK_SECTION_ID', 'NAME'));
while ($arSectionLvl1 = $rsSectionLvl1->Fetch()) {
    if (!$arSectionLvl1['IBLOCK_SECTION_ID']) {
        $arResult['CATEGORIES'][$arSectionLvl1['ID']] = $arSectionLvl1;
        $arResult['CATEGORIES'][$arSectionLvl1['ID']]['ITEMS'] = $arSectionsSections[$arSectionLvl1['ID']]['ITEMS'];
    }
}