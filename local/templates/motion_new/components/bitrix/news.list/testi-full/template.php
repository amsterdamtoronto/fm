<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

    <div class="row">

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

    <div class="col-lg-4 mb-4">
        <div class="rewievs-block d-flex flex-column justify-content-between"  id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <div class="swiper-head">
                <div class="swiper-head__top d-flex"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>">
                    <div class="swiper-head__right">
                        <div class="swiper-head__name"><?=$arItem["NAME"]?></div>
                        <a href="#" class="swiper-head__proff"><?=$arItem["PROPERTIES"]["WORK"]["VALUE"]?></a>
                    </div>
                </div>
                <div class="swiper-head__text"><?=$arItem["PREVIEW_TEXT"]?></div>
                <? if ( $arItem["PROPERTIES"]["VIDEO"]["VALUE"] ) : ?>
                <div id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <?php
                    $url = $arItem["PROPERTIES"]["VIDEO"]["VALUE"];
                    $url_parts = parse_url($url);
                    parse_str($url_parts['query'], $query_parts);
                    if ($query_parts['v'] ===  NULL){
                        $url = $arItem["PROPERTIES"]["VIDEO"]["VALUE"];
                        $url_parts = parse_url($url);
                        ?>
                        <div class="youtube" id="<?=str_replace('/', '',  $url_parts['path'])?>" style="width: 370px; height: 245px;"></div>
                        <?php
                    }else{
                        ?>
                        <div class="youtube" id="<?=$query_parts['v']?>" style="width: 370px; height: 245px;"></div>
                        <?php
                    }
                    ?>

                </div>
                <? endif ?>
            </div>
            <div class="swiper-date"><?=$arItem["PROPERTIES"]["DATE"]["VALUE"]?></div>
        </div>
    </div>
<?endforeach;?>

    </div>
