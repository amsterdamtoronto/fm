<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="row">
    <div class="col-lg-12 section-head d-flex align-items-center justify-content-between mb-5">
        <h2 class="title mb-0">Отзывы клиентов</h2><a class="btn" href="/otzyvy/">все отзывы</a>
    </div>
</div>
<div class="row lazy-load-box">
    <div class="col-lg-12">
        <div class="swiper-container" id="rewievs-slider">
            <div class="swiper-wrapper">

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

    <div class="swiper-slide d-flex flex-column justify-content-between"  id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div class="swiper-head">
            <div class="swiper-head__top d-flex"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>">
                <div class="swiper-head__right">
                    <div class="swiper-head__name"><?=$arItem["NAME"]?></div>
                    <a href="#" class="swiper-head__proff"><?=$arItem["PROPERTIES"]["WORK"]["VALUE"]?></a>
                </div>
            </div>
            <div class="swiper-head__text"><?=$arItem["PREVIEW_TEXT"]?></div>
            <? if ( $arItem["PROPERTIES"]["VIDEO"]["VALUE"] ) : ?>
                <div id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <?php
                    $url = $arItem["PROPERTIES"]["VIDEO"]["VALUE"];
                    $url_parts = parse_url($url);


                    ?>

                    <div class="youtube" id="<?=str_replace('/', '',  $url_parts['path'])?>" style="width: 370px; height: 245px;"></div>
                </div>
            <? endif ?>
        </div>
        <div class="swiper-date"><?=$arItem["PROPERTIES"]["DATE"]["VALUE"]?></div>
    </div>
<?endforeach;?>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
</div>
