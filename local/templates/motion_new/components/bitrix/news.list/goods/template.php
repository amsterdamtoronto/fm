<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="row">

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	if($arItem["PROPERTIES"]["BRAND"]["VALUE"]){
			$brand = CIBlockElement::GetById($arItem["PROPERTIES"]["BRAND"]["VALUE"])->GetNext();
	} else continue;
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
        <div class="col-lg-4 col-md-6" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <div class="shop-item mt-4 mb-4">
				<div class="swiper-image"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"></a></div>
                <div class="swiper-block">
                    <div class="swiper-block__top">
                        <div class="swiper-block__pre">Электросамокат</div><a class="swiper-block__name" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
                    </div>
                    <div class="swiper-block__bottom d-flex align-items-center justify-content-between">
						<? if($arItem["PROPERTIES"]["PRICE"]["VALUE"] == 0 ) { ?>
                            <div class="swiper-block__price smtext">
                                <?= $arItem["PROPERTIES"]["NO_PRICE"]["VALUE"] ?>
                            </div>
                        <? }else{ ?>
                            <div class="swiper-block__price">
                                <?=number_format($arItem["PROPERTIES"]["PRICE"]["VALUE"], 0, '', ' ' )?> ₽
                            </div>
                       <? } ?>
                        <a class="btn" href="#" onclick="$('#nameOfProduct').val('<?=$arItem["NAME"]?>')" data-toggle="modal" data-target="#exampleModal5">заказать</a>
                    </div>
                </div>

				<img class="swiper-logo" src="<?=CFile::GetPath($brand["PREVIEW_PICTURE"])?>">

            </div>
        </div>
<?endforeach;?>

</div>
