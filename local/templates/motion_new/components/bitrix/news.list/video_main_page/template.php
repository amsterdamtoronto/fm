<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="row">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    <div class="col-lg-4 col-md-4 mb-5 video-blog-links" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <?php
        $url = $arItem["PROPERTIES"]["VIDEO"]["VALUE"];
        $url_parts = parse_url($url);
        parse_str($url_parts['query'], $query_parts);
        if ($query_parts['v'] ===  NULL){
            $url = $arItem["PROPERTIES"]["VIDEO"]["VALUE"];
            $url_parts = parse_url($url);
            ?>
            <div class="youtube" id="<?=str_replace('/', '',  $url_parts['path'])?>" style="width: 370px; height: 245px;"></div>
            <?php
        }else{
            ?>
            <div class="youtube" id="<?=$query_parts['v']?>" style="width: 370px; height: 245px;"></div>
            <?php
        }
        ?>


        <a class="video-name" href="<?=$arItem["PROPERTIES"]["VIDEO"]["VALUE"]?>" target="_blank"><?=$arItem['NAME']?></a>
    </div>
<?endforeach;?>


</div>
