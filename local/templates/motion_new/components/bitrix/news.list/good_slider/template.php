<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<div class="swiper-container" id="item-slider">
    <div class="swiper-wrapper">

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
    if($arItem["PROPERTIES"]["BRAND"]["VALUE"]){
        $brand = CIBlockElement::GetById($arItem["PROPERTIES"]["BRAND"]["VALUE"])->GetNext();
    } else continue;
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    <div class="swiper-slide" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="swiper-image">
            <?
            $renderImage = CFile::ResizeImageGet(
                $arItem["PREVIEW_PICTURE"],
                Array("width" => 250, "height" => 250),
                BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
                false,
                false,
                false,
                false
                );

            echo '<img src="'.$renderImage["src"].'" />';
            ?>
        </a>
        <div class="swiper-block">
            <div class="swiper-block__top">
                <div class="swiper-block__pre">Электросамокат</div>
                <a class="swiper-block__name" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
            </div>
            <div class="swiper-block__bottom d-flex align-items-center justify-content-between">
                <div class="swiper-block__price">
                    <? if($arItem["PROPERTIES"]["PRICE"]["VALUE"] == 0 ) { ?>
                    <p class="news-item">
                        Нет в наличии, можем на заказ.

                    </p>
                    <? }else{ ?>
                        <p class="news-item slider-no-price-text">
                            <?=number_format($arItem["PROPERTIES"]["PRICE"]["VALUE"], 0, '', ' ')?> ₽
                        </p>
                    <? } ?>
                </div>
                <a class="btn" href="#" onclick="$('#nameOfProduct').val('<?=$arItem["NAME"]?>')" data-toggle="modal" data-target="#exampleModal5">заказать</a>
            </div>
        </div>
        <img class="swiper-logo lazy" src="<?=CFile::GetPath($brand["PREVIEW_PICTURE"])?>">
    </div>
<?endforeach;?>

    </div>
    <div class="swiper-pagination"></div>
</div>



