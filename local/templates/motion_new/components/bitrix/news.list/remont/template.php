<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="row">
    <div class="col-lg-12 d-flex flex-column mb-5">
        <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <div class="spec-line d-flex justify-content-between align-items-center">
            <div class="spec-left">
				<?if($arItem["DETAIL_PICTURE"]) { ?><img src="<?= CFile::GetPath($arItem["DETAIL_PICTURE"]["ID"]) ?>"
				alt="Ремонт самокатов <?= $arItem["NAME"] ?>"><? } ?>
                <div class="spec-block d-flex flex-column">
                    <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="spec-name"><?= $arItem["NAME"] ?></a>
                    <span class="spec-desc"><?= $arItem["PROPERTIES"]["SUBTITLE"]["~VALUE"] ?></span>
                </div>
            </div>
            <a href="<?=$arItem["DETAIL_PAGE_URL"] ?>" class="btn">посмотреть</a>
        </div>
    <? endforeach; ?>
</div>
</div>
