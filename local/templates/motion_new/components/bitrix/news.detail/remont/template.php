<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true); ?>
<section class="head-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "bread", Array(
                    "PATH" => "",    // Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
                    "SITE_ID" => "s1",    // Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
                    "START_FROM" => "0",    // Номер пункта, начиная с которого будет построена навигационная цепочка
                ),
                    false
                ); ?>
            </div>
            <div class="col-lg-12">
                <h1 class="head-section__title">
                    Ремонт
                    <? $APPLICATION->ShowTitle(false) ?> </h1>
            </div>
        </div>
    </div>
</section>
<section class="gray">
    <div class="container">
        <div class="row">
            <h2 class="col-lg-10 title">
                Цены на запчасти и ремонт электросамокатов <?= $arResult["NAME"] ?></h2>
        </div>
        <?
        //debug($arResult["CODE"] );
        $path = "/includes/remont/" . $arResult["CODE"] . ".php";
        $APPLICATION->IncludeFile($path, Array(), Array(
            "MODE" => "html",                                           // будет редактировать в веб-редакторе
            "NAME" => "Редактирование включаемой области раздела"     // текст всплывающей подсказки на иконке
        ));
        ?>
    </div>
</section>
<section class="advantages">
    <div class="container">
        <div class="row d-flex align-items-center mt-5">
            <div class="col-lg-6">
                <div class="youtube" id="6hPuT1e9vbM" style="width: 100%; height: 245px;"></div>
            </div>
            <div class="col-lg-6">
                <div class="view">
                    <div class="view-pre">
                        Комплексные работы
                    </div>
                    <div class="title">
                        Смотрите видео о комплексных работах
                    </div>
                    <a href="/blog/" class="btn">Больше видео</a>
                </div>
            </div>
        </div>
        <div class="row d-flex justify-content-between mt-5 pt-5">
            <div class="col-lg-5">
                <div class="advantages__left">
                    <img src="/bitrix/templates/motion/images/alogo.svg" class="logo">
                    <h2 class="advantages__title">
                        Ремонт и сервисное обслуживание <?= $arResult["NAME"] ?></h2>
                    <div class="advantages__desc">
                        Специализированный сервис Xiaomi,&nbsp;Kugoo: модернизация, тюнинг, ремонт, запчасти,
                        покупка и продажа по низким ценам в Москве
                    </div>
                </div>
            </div>
            <div class="col-lg-7 d-flex flex-wrap justify-content-between">
                <div class="advantages__block d-flex flex-column">
                    <img src="/bitrix/templates/motion/images/01.svg">
                    <div class="advantages__t">
                        Cервисные центры
                    </div>
                    <div class="advantages__d">
                        Калужская и Путилково в Москве
                    </div>
                </div>
                <div class="advantages__block d-flex flex-column">
                    <img src="/bitrix/templates/motion/images/02.svg">
                    <div class="advantages__t">
                        Запчасти
                    </div>
                    <div class="advantages__d">
                        Свой склад запчастей
                    </div>
                </div>
                <div class="advantages__block d-flex flex-column">
                    <img src="/bitrix/templates/motion/images/03.svg">
                    <div class="advantages__t">
                        Гарантия 1 год
                    </div>
                    <div class="advantages__d">
                        На новые самокаты с нашей подготовкой и ремонт
                    </div>
                </div>
                <div class="advantages__block d-flex flex-column">
                    <img src="/bitrix/templates/motion/images/04.svg">
                    <div class="advantages__t">
                        Выездной ремонт
                    </div>
                    <div class="advantages__d">
                        Мастера cделают все работу у вас дома или в офисе
                    </div>
                </div>
            </div>
        </div>
        <div class="row d-flex align-items-center ">
            <div class="row mt-5">
                <div class="col-lg-3 mb-4">
                    <div class="problem-block d-flex flex-column justify-content-between">
                        <div class="problem-block__item">
                            <div class="problem-block__pre">
                                Диагностика
                            </div>
                            <div class="problem-block__name">
                                Электросамокат не заряжается
                            </div>
                        </div>
                        <a class="elipse-btn" href="#" data-toggle="modal" data-target="#exampleModal"><img
                                    src="/bitrix/templates/motion/images/reda.svg"></a>
                    </div>
                </div>
                <div class="col-lg-3 mb-4">
                    <div class="problem-block d-flex flex-column justify-content-between">
                        <div class="problem-block__item">
                            <div class="problem-block__pre">
                                Диагностика
                            </div>
                            <div class="problem-block__name">
                                Сломался механизм складывания
                            </div>
                        </div>
                        <a class="elipse-btn" href="#" data-toggle="modal" data-target="#exampleModal"><img
                                    src="/bitrix/templates/motion/images/reda.svg"></a>
                    </div>
                </div>
                <div class="col-lg-3 mb-4">
                    <div class="problem-block d-flex flex-column justify-content-between">
                        <div class="problem-block__item">
                            <div class="problem-block__pre">
                                Диагностика
                            </div>
                            <div class="problem-block__name">
                                Спустило колесо
                            </div>
                        </div>
                        <a class="elipse-btn" href="#" data-toggle="modal" data-target="#exampleModal"><img
                                    src="/bitrix/templates/motion/images/reda.svg"></a>
                    </div>
                </div>
                <div class="col-lg-3 mb-4">
                    <div class="problem-block d-flex flex-column justify-content-between">
                        <div class="problem-block__item">
                            <div class="problem-block__pre">
                                Диагностика
                            </div>
                            <div class="problem-block__name">
                                Самокат залит
                            </div>
                        </div>
                        <a class="elipse-btn" href="#" data-toggle="modal" data-target="#exampleModal"><img
                                    src="/bitrix/templates/motion/images/reda.svg"></a>
                    </div>
                </div>
            </div>
        </div>
</section>
<section class="gray">
    <div class="container">
        <div class="row">
            <h2 class="col-lg-12 title">
                Наш ремонт электросамокатов</h2>
        </div>
        <div class="row about-images">
            <div class="col-lg-3 mb-4">
                <img src="/bitrix/templates/motion/images/09.jpg">
            </div>
            <div class="col-lg-3 mb-4">
                <img src="/bitrix/templates/motion/images/10.jpg">
            </div>
            <div class="col-lg-3 mb-4">
                <img src="/bitrix/templates/motion/images/11.jpg">
            </div>
            <div class="col-lg-3 mb-4">
                <img src="/bitrix/templates/motion/images/12.jpg">
            </div>
        </div>
        <? $APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
                "AREA_FILE_SHOW" => "file",
                "AREA_FILE_SUFFIX" => "inc",
                "EDIT_TEMPLATE" => "",
                "PATH" => "/includes/how-we-work.php"
            )
        ); ?>
    </div>
</section>
<? $APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    Array(
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => "",
        "PATH" => "/includes/testi.php"
    )
); ?>
<section class="white-section">
    <div class="container">
        <? $APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
                "AREA_FILE_SHOW" => "file",
                "AREA_FILE_SUFFIX" => "inc",
                "EDIT_TEMPLATE" => "",
                "PATH" => "/includes/blog.php"
            )
        ); ?>
    </div>
    <div class="container pt-5">
        <div class="about-text">
            <?=$arResult["DETAIL_TEXT"]?>
        </div>
    </div>
</section> <br>