<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true); ?><section class="head-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "bread", Array(
                    "PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
                    "SITE_ID" => "s1",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
                    "START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
                ),
                    false
                );?>
            </div>
            <div class="col-lg-12">
                <h1 class="head-section__title"><?$APPLICATION->ShowTitle(false)?></h1><a class="btn" href="#">заказать презентацию</a>
            </div>
        </div>
    </div>
</section>
<section class="gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="shop-main d-flex justify-content-between align-items-center">
                    <div class="shop-main__left">
                        <div class="shop-main__type">Электросамокат</div>
                        <div class="shop-main__name"><?=$arResult["NAME"]?></div>
                        <div class="shop-main__desc"><?php /*$arResult["DETAIL_TEXT"]*/ ?></div>
                        <div class="shop-main__line d-flex align-items-center">
                            <?if($arResult["PROPERTIES"]["PRICE"]["VALUE"] == 0){ ?>
                            <div class="shop-main__price">
                                <?=$arResult["PROPERTIES"]["NO_PRICE"]["VALUE"]?>
                            </div>
                            <?}else{?>
                                <div class="shop-main__price">
                                    <?=number_format($arResult["PROPERTIES"]["PRICE"]["VALUE"], 0, '', ' ')?>₽
                                </div>
                            <?}?>
                            <a class="btn" href="#" onclick="$('#nameOfProduct').val('<?=$arResult["NAME"]?>')" data-toggle="modal" data-target="#exampleModal5">заказать</a>
                        </div>
                        <? if ($arResult["PROPERTIES"]["PRICE_COMPLEX"]["VALUE"]):?>
                            <div class="shop-main__dop"> + Можно сделать <a href="//fast-motion.ru/modernizatsiya/" class="red"><strong> комплексную подготовку </a><span><?= $arResult["PROPERTIES"]["PRICE_COMPLEX"]["VALUE"] ?></span> ₽</strong></div>
                        <? endif; ?>
                        <div class="shop-main__line d-flex mt-3">
                            <div class="shop-main__block d-flex align-items-center"><img src="<?=SITE_TEMPLATE_PATH?>/images/check.svg"><span>Гарантия от<br> fast-motion 1 год</span></div>
                            <div class="shop-main__block d-flex align-items-center"><img src="<?=SITE_TEMPLATE_PATH?>/images/check.svg"><span>100% оригинальная<br> продукция</span></div>
                        </div>
                    </div>
                    <div class="shop-main__right">
                        <div class="d-flex flex-row-reverse" id="slide-gallery">
                            <div class="swiper-container gallery-thumbs">
                                <div class="swiper-wrapper">
<?
	foreach($arResult["PROPERTIES"]["GALLERY"]["VALUE"] as $photoID) {
?>
									<div class="swiper-slide"><img src="<?=CFile::GetPath($photoID)?>"></div>
									<? } ?>
                                </div>
                            </div>
                            <div class="swiper-container gallery-top">
                                <div class="swiper-wrapper">
                                   <?
	foreach($arResult["PROPERTIES"]["GALLERY"]["VALUE"] as $photoID) {
?>
									<div class="swiper-slide"><img src="<?=CFile::GetPath($photoID)?>"></div>
									<? } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="white-section">
    <div class="container">
        <div class="row pb-5">
            <div class="col-lg-4">
                <div class="advent-shop"><img src="<?=SITE_TEMPLATE_PATH?>/images/01.svg">
                    <div class="advent-shop__name">Доставка</div>
                    <div class="advent-shop__desc">Оперативная доставка в день заказа или на следующий день. Отправка в регионы  </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="advent-shop"><img src="<?=SITE_TEMPLATE_PATH?>/images/02.svg">
                    <div class="advent-shop__name">Подготовка</div>
                    <div class="advent-shop__desc">Дополнительная комплексная подготовка, защищаем от воды, увеличиваем ресурс и надежность.</div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="advent-shop"><img src="<?=SITE_TEMPLATE_PATH?>/images/04.svg">
                    <div class="advent-shop__name">Экспертиза</div>
                    <div class="advent-shop__desc">Мы более 2 лет на рынке электротранспорта. Мы дорожим репутацией перед партнерами и клиентами.</div>
                </div>
            </div>
        </div>
        <div class="row mt-5 pb-5 d-flex align-items-center justify-content-between">
            <div class="col-lg-5">
                <h2 class="title">Характеристики</h2>
                <div class="advent-list">
<?
	foreach($arResult["PROPERTIES"]["PREM"]["~VALUE"] as $str) {
?>
                    <div class="advent-list__line d-flex align-items-center"><img src="<?=SITE_TEMPLATE_PATH?>/images/check.svg"><span><?=$str?></span></div>
					<? } ?>
                </div><a class="btn" href="#" data-toggle="modal" data-target="#exampleModal2">БЕСПЛАТНАЯ КОНСУЛЬТАЦИЯ</a>
            </div>
            <div class="col-lg-6">
                <div class="images-about black-sam">
                    <img class="images-about__item" src="<?=CFile::GetPath($arResult["PROPERTIES"]["PREM_PHOTO"]["VALUE"])?>">
<!--                    <div class="tooltips d-flex align-items-start justify-content-center"><span class="circle"></span>-->
<!--                        <div class="tooltips-block">-->
<!--                            <div class="tooltips-block__title">Зарядка всего 5 часов</div>-->
<!--                            <div class="tooltips-block__desc">Заряжайте его так же как свой мобильный телефон</div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="tooltips two d-flex align-items-start justify-content-center"><span class="circle"></span>-->
<!--                        <div class="tooltips-block">-->
<!--                            <div class="tooltips-block__title">Зарядка всего 5 часов</div>-->
<!--                            <div class="tooltips-block__desc">Заряжайте его так же как свой мобильный телефон</div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="tooltips three d-flex align-items-start justify-content-center"><span class="circle"></span>-->
<!--                        <div class="tooltips-block">-->
<!--                            <div class="tooltips-block__title">Зарядка всего 5 часов</div>-->
<!--                            <div class="tooltips-block__desc">Заряжайте его так же как свой мобильный телефон</div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="tooltips four d-flex align-items-start justify-content-center"><span class="circle"></span>-->
<!--                        <div class="tooltips-block">-->
<!--                            <div class="tooltips-block__title">Зарядка всего 5 часов</div>-->
<!--                            <div class="tooltips-block__desc">Заряжайте его так же как свой мобильный телефон</div>-->
<!--                        </div>-->
<!--                    </div>-->
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <h2 class="col-lg-12 title mb-3 mt-5">Как мы работаем</h2>
            <div class="col-lg-6">
                <p>Мы ценим простоту и открытость в людях, всегда стараемся решить проблему или помочь реализовать пожелания клиента</p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-9 line">
                <div class="line-red"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="work-block d-flex flex-column"><span>1</span>
                    <div class="work-block__title">Заказ<br></div>
                    <div class="work-block__desc">Оставьте заявку через сайт или позвоните нам по телефону</div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="work-block d-flex flex-column"><span>2</span>
                    <div class="work-block__title">Связываемся</div>
                    <div class="work-block__desc">Согласуем все детали заказа </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="work-block d-flex flex-column"><span>3</span>
                    <div class="work-block__title">Подготавливаем</div>
                    <div class="work-block__desc">Дополнительные комплексные работы и акссесуары </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="work-block d-flex flex-column"><span>4</span>
                    <div class="work-block__title">Доставка и оплата <br> </div>
                    <div class="work-block__desc">Статус о выполнении заказа можно посмотреть на нашем сайте </div>
                </div>
            </div>
        </div>
        <?
        /**
         * временно отключил
         * var isShowClientsWithTheElement
        */

        $isShowClientsWithTheElement = false; ?>
        <?if($isShowClientsWithTheElement):?>
        <div class="row mt-5 pt-5">
            <h2 class="col-lg-12 title">Клиенты с <?=$arResult["NAME"]?></h2>
        </div>
        <?endif;?>
        <div class="row about-images">
<?
	foreach($arResult["PROPERTIES"]["CLIENTS"]["VALUE"] as $str) {
?>
			<div class="col-lg-3 mb-4"><img src="<?=CFile::GetPath($str)?>"></div>
			<? } ?>
        </div>
    </div>
</section>
<?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    Array(
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => "",
        "PATH" => "/includes/testi.php"
    )
);?>
<section class="white-section">
    <div class="container">
        <?$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
                "AREA_FILE_SHOW" => "file",
                "AREA_FILE_SUFFIX" => "inc",
                "EDIT_TEMPLATE" => "",
                "PATH" => "/includes/blog.php"
            )
        );?>
    </div>
<div class="container">
	<div class="row about-text pt-5">
		<div class="col-lg-12 pr-5">
				<?=$arResult["PROPERTIES"]["SEO"]['~VALUE']['TEXT']?>
		</div>
	</div>
	</div>
</section>
