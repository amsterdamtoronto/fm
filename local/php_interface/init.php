<?


function  getPriceApi(){

    if (CModule::IncludeModule("iblock")):
        global $message_log;
        $message_log = "Date ". date('jS \of F Y G:i'). "\n";
        $iblockId = "4";
        $arFilter = Array("IBLOCK_ID" => $iblockId, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, array('ID', 'NAME', 'IBLOCK_ID'));
        while ($arElements = $res->GetNextElement()) // Получаем список элементов
        {
            $element = $arElements->GetFields();
            $elementId = $element["ID"];
            $message_log .= "\n" . $element["NAME"];

            $elementsProp = CIBlockElement::GetProperty($iblockId, $elementId, array("sort" => "asc"), array("")); // Получаем список свойств элемента
            while ($elementProp = $elementsProp->GetNext()) {

                if ($elementProp["ID"] == "17") {
                    $apiId = $elementProp["VALUE"];

                }
            }

            if ($apiId) :
                $dataApiUrl = "https://script.google.com/macros/s/AKfycbxLkYuOqup8VQnATxwcgUnJar7fGGFDNj9GbwmUV0l0zr6VoG0i/exec?id=" . $apiId;
                $dataApi = file_get_contents($dataApiUrl);
                $dataApi = json_decode($dataApi, true);
                $dataApiPrice = $dataApi['price'];
                //debug($dataApiPrice);
                $message_log .= "\nNew price: " . $dataApiPrice . "\n";
                // записываем цену в свойство
                if (is_numeric($dataApiPrice)) { // отсекаем NaN
                    CIBlockElement::SetPropertyValuesEx($elementId, false, array("PRICE" => $dataApiPrice));
                }
            endif;
        }
        $message_log .= "------------------ \n";
        $filename = $_SERVER["DOCUMENT_ROOT"] . '/upload/tmp/log_api_price.txt';
        file_put_contents($filename, $message_log . PHP_EOL, FILE_APPEND);
        debug($message_log);
    endif;
    return "getPriceApi();";

}




function getVideoFromYoutube()
{
    // Проверяем, подключен ли модуль Инфоблоки
    if (!\Bitrix\Main\Loader::includeModule('iblock')) return false;

    // Записываем в переменные конфигурационные данные API Youtube
    $apiKey = "AIzaSyA1ZIBkGFxKbCfNos-r5kjeoELu4pgRmWA";
    $chanelID = "UCvy7FIEQYztchmNfCROlgDw";
    $maxResult = 10;

    // Записываем параметры запроса на Youtube в массив
    $params = [
        "order" => "date",
        "part" => "snippet",
        "channelId" => $chanelID,
        "maxResults" => $maxResult,
        "key" => $apiKey
    ];

    // Формируем ссылку запроса
    $url = "https://www.googleapis.com/youtube/v3/search?" . http_build_query($params);

    // Делаем запрос на Youtube
    $c = curl_init($url);
    curl_setopt_array($c, [
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTPHEADER => [
            "Content-Type: application/json"
        ]
    ]);

    // Записываем результат выполнения запроса в переменную
    $rs = curl_exec($c);
    curl_close($c);

    // Декодируем полученный ответ в формате json
    $rs = json_decode($rs);

    // Если в ответе есть ошибки, возвращаем false
    if($rs->error) {
        return false;
    }

    // Параметры для транслита
    $params = array(
        "max_len" => 100,
        "change_case" => "L",
        "replace_space" => "_",
        "replace_other" => "_",
        "delete_repeat_replace" => "true",
        "use_google" => "false",
    );

    // Если есть видео в ответе сервера
    if(!empty($rs->items)){
        // Перебираем полученные видео
        foreach($rs->items as $item) {
            // Формируем ссылку на видео Youtube
            $videoUrl = "https://www.youtube.com/watch?v=" . $item->id->videoId;

            // Проверяем, есть ли видео
            $rsVideo = CIBlockElement::GetList([], ["PROPERTY_VIDEO" => $videoUrl], false, [], ["PROPERTY_VIDEO"]);
            // Если видео нет;
            if(!$rsVideo->SelectedRowsCount()) {
                // Добавляем видео
                $elVideo = new CIBlockElement;

                $properties = [];
                $properties["VIDEO"] = $videoUrl;

                $arrElVideoData = [
                    "IBLOCK_SECTION_ID" => 3,
                    "IBLOCK_ID" => 3,
                    "PROPERTY_VALUES"=> $properties,
                    "NAME" => $item->snippet->title,
                    "CODE" => CUtil::translit($item->snippet->title, "ru", $params),
                    "ACTIVE" => "Y"
                ];

                $elVideo->Add($arrElVideoData);
            }
        }
    }
     return "getVideoFromYoutube();";
}


function debug($var){
    print '<pre style="
padding: 20px;
margin:20px;
border: 1px solid #eee;
background-color: #fafafa;
color: #999;
line-height: 2.2;
font-size: 11px;
font-family: Arial;
">'
        . preg_replace('/\[([\w\d_~!*-]+)\] => /', '<div style="display:inline-block;color:#fff; margin:5px 10px 0 0; padding: 0 10px;background-color:#aaa;" title="\\1">\\1</div>', print_r($var, true))
        . '</pre>';
}

include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/wsrubi.smtp/classes/general/wsrubismtp.php");





?>