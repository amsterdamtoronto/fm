<div class="row">
    <div class="col-lg-12">
        <div class="accordion" id="accordionExample">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h3 class="mb-0 d-flex align-items-center"><img src="/bitrix/templates/motion/images/tire.svg"> <button class="btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Колеса и покрышки </button> </h3>
                </div>
                <div class="collapse show" id="collapseOne" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="card-body__header d-flex">
                            <div class="card-body__left">
                                Услуга
                            </div>
                            <div class="card-body__right">
                                Стоимость
                            </div>
                        </div>
                        <div class="card-body__line d-flex align-items-center">
                            <div class="card-body__left">
                                <div class="card-body__name">
                                    Замена покрышки мотор-колеса
                                </div>
                                <div class="card-body__desc">
                                    Покрышка включена в стоимость
                                </div>
                            </div>
                            <div class="card-body__price">
                                2 500₽
                            </div>
                        </div>
                        <div class="card-body__line d-flex align-items-center">
                            <div class="card-body__left">
                                <div class="card-body__name">
                                    Замена подшипников мотор-колеса (6002)
                                </div>
                                <div class="card-body__desc">
                                    Подшипники включены в стоимость (5 500₽ с заменой покрышки)
                                </div>
                            </div>
                            <div class="card-body__price">
                                3 500₽
                            </div>
                        </div>
                        <div class="card-body__line d-flex align-items-center">
                            <div class="card-body__left">
                                <div class="card-body__name">
                                    Замена подшипников заднего колеса (6200)
                                </div>
                                <div class="card-body__desc">
                                    Подшипники включены в стоимость
                                </div>
                            </div>
                            <div class="card-body__price">
                                1 000₽
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingTwo">
                    <h3 class="mb-0 d-flex align-items-center"><img src="/bitrix/templates/motion/images/a3.svg"> <button class="btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Общие работы </button> </h3>
                </div>
                <div class="collapse" id="collapseTwo" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="card-body__header d-flex">
                            <div class="card-body__left">
                                Услуга
                            </div>
                            <div class="card-body__right">
                                Стоимость
                            </div>
                        </div>
                        <div class="card-body__line d-flex align-items-center">
                            <div class="card-body__left">
                                <div class="card-body__name">
                                    Диагностика
                                </div>
                                <div class="card-body__desc">
                                    При условии работ в СЦ, в другом случае 500₽
                                </div>
                            </div>
                            <div class="card-body__price">
                                Бесплатно
                            </div>
                        </div>
                        <div class="card-body__line d-flex align-items-center">
                            <div class="card-body__left">
                                <div class="card-body__name">
                                    Замена ручки газа/тормоза
                                </div>
                                <div class="card-body__desc">
                                    Ручка включена в стоимость
                                </div>
                            </div>
                            <div class="card-body__price">
                                1 800₽
                            </div>
                        </div>
                        <div class="card-body__line d-flex align-items-center">
                            <div class="card-body__left">
                                <div class="card-body__name">
                                    Замена дисплейного модуля
                                </div>
                                <div class="card-body__desc">
                                    Дисплей включен в стоимость
                                </div>
                            </div>
                            <div class="card-body__price">
                                2 100₽
                            </div>
                        </div>
                        <div class="card-body__line d-flex align-items-center">
                            <div class="card-body__left">
                                <div class="card-body__name">
                                    Проклейка магнитов курка
                                </div>
                            </div>
                            <div class="card-body__price">
                                600₽
                            </div>
                        </div>
                        <div class="card-body__line d-flex align-items-center">
                            <div class="card-body__left">
                                <div class="card-body__name">
                                    Замена светодиодных лент
                                </div>
                                <div class="card-body__desc">
                                    Лента включена в стоимость
                                </div>
                            </div>
                            <div class="card-body__price">
                                1 500₽
                            </div>
                        </div>
                        <div class="card-body__line d-flex align-items-center">
                            <div class="card-body__left">
                                <div class="card-body__name">
                                    Замена стоп-сигналов
                                </div>
                                <div class="card-body__desc">
                                    Стоп-сигнал включен в стоимость
                                </div>
                            </div>
                            <div class="card-body__price">
                                1 500₽
                            </div>
                        </div>
                        <div class="card-body__line d-flex align-items-center">
                            <div class="card-body__left">
                                <div class="card-body__name">
                                    Разборка-сборка рулевой трубы
                                </div>
                                <div class="card-body__desc">
                                    Только при диагностике или замене АКБ или контроллера, без ремонта
                                </div>
                            </div>
                            <div class="card-body__price">
                                1 200₽
                            </div>
                        </div>
                        <div class="card-body__line d-flex align-items-center">
                            <div class="card-body__left">
                                <div class="card-body__name">
                                    Уплотнение ступицы заднего колеса
                                </div>
                            </div>
                            <div class="card-body__price">
                                500₽
                            </div>
                        </div>
                        <div class="card-body__line d-flex align-items-center">
                            <div class="card-body__left">
                                <div class="card-body__name">
                                    Установка 8-ми гранной рулевой втулки
                                </div>
                            </div>
                            <div class="card-body__price">
                                2 000₽
                            </div>
                        </div>
                        <div class="card-body__line d-flex align-items-center">
                            <div class="card-body__left">
                                <div class="card-body__name">
                                    Доработка узла сложения
                                </div>
                            </div>
                            <div class="card-body__price">
                                2 300₽
                            </div>
                        </div>
                        <div class="card-body__line d-flex align-items-center">
                            <div class="card-body__left">
                                <div class="card-body__name">
                                    Доработка заднего амортизатора
                                </div>
                            </div>
                            <div class="card-body__price">
                                2 300₽
                            </div>
                        </div>
                        <div class="card-body__line d-flex align-items-center">
                            <div class="card-body__left">
                                <div class="card-body__name">
                                    Установка втулки заднего колеса
                                </div>
                            </div>
                            <div class="card-body__price">
                                1 700₽
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingThree">
                    <h3 class="mb-0 d-flex align-items-center"><img src="/bitrix/templates/motion/images/a2.svg"> <button class="btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Электрика </button> </h3>
                </div>
                <div class="collapse" id="collapseThree" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="card-body__header d-flex">
                            <div class="card-body__left">
                                Услуга
                            </div>
                            <div class="card-body__right">
                                Стоимость
                            </div>
                        </div>
                        <div class="card-body__line d-flex align-items-center">
                            <div class="card-body__left">
                                <div class="card-body__name">
                                    Замена датчика холла в мотор-колесе
                                </div>
                            </div>
                            <div class="card-body__price">
                                2 000₽
                            </div>
                        </div>
                        <div class="card-body__line d-flex align-items-center">
                            <div class="card-body__left">
                                <div class="card-body__name">
                                    Замена силовых разъемов мотор-колеса (МТ60, MR60)
                                </div>
                            </div>
                            <div class="card-body__price">
                                1 000₽
                            </div>
                        </div>
                        <div class="card-body__line d-flex align-items-center">
                            <div class="card-body__left">
                                <div class="card-body__name">
                                    Пайка резистора питания головы
                                </div>
                            </div>
                            <div class="card-body__price">
                                1 500₽
                            </div>
                        </div>
                        <div class="card-body__line d-flex align-items-center">
                            <div class="card-body__left">
                                <div class="card-body__name">
                                    Восстановление цепи питания контроллера (DC)
                                </div>
                            </div>
                            <div class="card-body__price">
                                2 500₽
                            </div>
                        </div>
                        <div class="card-body__line d-flex align-items-center">
                            <div class="card-body__left">
                                <div class="card-body__name">
                                    Восстановление силовой цепи контроллера (mosfet)
                                </div>
                            </div>
                            <div class="card-body__price">
                                2 000₽
                            </div>
                        </div>
                        <div class="card-body__line d-flex align-items-center">
                            <div class="card-body__left">
                                <div class="card-body__name">
                                    Замена косы питания подсветки и стопов (отрезок МК)
                                </div>
                            </div>
                            <div class="card-body__price">
                                1 200₽
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingFour">
                    <h3 class="mb-0 d-flex align-items-center"><img src="/bitrix/templates/motion/images/a6.svg"> <button class="btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">Комплексная подготовка </button> </h3>
                </div>
                <div class="collapse" id="collapseFour" aria-labelledby="headingFour" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="card-body__header d-flex">
                            <div class="card-body__left">
                                Услуга
                            </div>
                            <div class="card-body__right">
                                Стоимость
                            </div>
                        </div>
                        <div class="card-body__line d-flex align-items-center">
                            <div class="card-body__left">
                                <div class="card-body__name">
                                    ТО с заменой всех расходников
                                </div>
                                <div class="card-body__desc">
                                    Расходники включены
                                </div>
                            </div>
                            <div class="card-body__price">
                                4 500 ₽
                            </div>
                        </div>
                        <div class="card-body__line d-flex align-items-center">
                            <div class="card-body__left">
                                <div class="card-body__name">
                                    Комплекс «Механика ES»
                                </div>
                                <div class="card-body__desc">
                                    Подробно на странице комплексных работ
                                </div>
                            </div>
                            <div class="card-body__price">
                                8 500₽
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>