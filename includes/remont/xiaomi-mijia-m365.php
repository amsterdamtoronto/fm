<div class="row">
	<div class="col-lg-12">
		<div class="accordion" id="accordionExample">
			<div class="card">
				<div class="card-header" id="headingOne">
					<h3 class="mb-0 d-flex align-items-center"><img src="/bitrix/templates/motion/images/tire.svg"> <button class="btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
					Колеса и покрышки </button> </h3>
				</div>
				<div class="collapse show" id="collapseOne" aria-labelledby="headingOne" data-parent="#accordionExample">
					<div class="card-body">
						<div class="card-body__header d-flex">
							<div class="card-body__left">
								 Услуга
							</div>
							<div class="card-body__right">
								 Стоимость
							</div>
						</div>
						<div class="card-body__line d-flex align-items-center">
							<div class="card-body__left">
								<div class="card-body__name">
									 Шиномонтаж
								</div>
								<div class="card-body__desc">
									 1 колесо
								</div>
							</div>
							<div class="card-body__price">
								 700₽
							</div>
						</div>
						<div class="card-body__line d-flex align-items-center">
							<div class="card-body__left">
								<div class="card-body__name">
									 Покрышка 8.5 дюймов
								</div>
								<div class="card-body__desc">
									 Усиленная покрышка 1 500₽
								</div>
							</div>
							<div class="card-body__price">
								 1 000₽
							</div>
						</div>
						<div class="card-body__line d-flex align-items-center">
							<div class="card-body__left">
								<div class="card-body__name">
									 Покрышка 10 дюймов
								</div>
							</div>
							<div class="card-body__price">
								 800₽
							</div>
						</div>
						<div class="card-body__line d-flex align-items-center">
							<div class="card-body__left">
								<div class="card-body__name">
									 Камера
								</div>
							</div>
							<div class="card-body__price">
								 700₽
							</div>
						</div>
						<div class="card-body__line d-flex align-items-center">
							<div class="card-body__left">
								<div class="card-body__name">
									 Переход на 10 дюймовые покрышки
								</div>
								<div class="card-body__desc">
									 Покрышки и проставки включены, при комплексных работах - 3 800₽
								</div>
							</div>
							<div class="card-body__price">
								 5 000₽
							</div>
						</div>
						<div class="card-body__line d-flex align-items-center">
							<div class="card-body__left">
								<div class="card-body__name">
									 Замена подшипников мотора + гидроизоляция
								</div>
								<div class="card-body__desc">
									 Подшипники включены, будет дешевле при комплексном работах
								</div>
							</div>
							<div class="card-body__price">
								 3 000 ₽
							</div>
						</div>
						<div class="card-body__line d-flex align-items-center">
							<div class="card-body__left">
								<div class="card-body__name">
									 Замена подшипников заднего колеса
								</div>
								<div class="card-body__desc">
									 Подшипники включены, при комплексных работах - 1000₽
								</div>
							</div>
							<div class="card-body__price">
								 2 000₽
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header" id="headingTwo">
					<h3 class="mb-0 d-flex align-items-center"><img src="/bitrix/templates/motion/images/a3.svg"> <button class="btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Общие работы </button> </h3>
				</div>
				<div class="collapse" id="collapseTwo" aria-labelledby="headingTwo" data-parent="#accordionExample">
					<div class="card-body">
						<div class="card-body__header d-flex">
							<div class="card-body__left">
								 Услуга
							</div>
							<div class="card-body__right">
								 Стоимость
							</div>
						</div>
						<div class="card-body__line d-flex align-items-center">
							<div class="card-body__left">
								<div class="card-body__name">
									 Диагностика
								</div>
								<div class="card-body__desc">
									 При условии работ в СЦ, в другом случае 500₽
								</div>
							</div>
							<div class="card-body__price">
								 Бесплатно
							</div>
						</div>
						<div class="card-body__line d-flex align-items-center">
							<div class="card-body__left">
								<div class="card-body__name">
									 Регулировка тормоза
								</div>
							</div>
							<div class="card-body__price">
								 500₽
							</div>
						</div>
						<div class="card-body__line d-flex align-items-center">
							<div class="card-body__left">
								<div class="card-body__name">
									 Регулировка узла складывания
								</div>
							</div>
							<div class="card-body__price">
								 500₽
							</div>
						</div>
						<div class="card-body__line d-flex align-items-center">
							<div class="card-body__left">
								<div class="card-body__name">
									 Замена винтов крышки дэки
								</div>
							</div>
							<div class="card-body__price">
								 700₽
							</div>
						</div>
						<div class="card-body__line d-flex align-items-center">
							<div class="card-body__left">
								<div class="card-body__name">
									 Замена замка узла складывания руля
								</div>
								<div class="card-body__desc">
									 Стоимость замка 800 - 1 000₽
								</div>
							</div>
							<div class="card-body__price">
								 500₽
							</div>
						</div>
						<div class="card-body__line d-flex align-items-center">
							<div class="card-body__left">
								<div class="card-body__name">
									 Замена оси узла складывания руля
								</div>
								<div class="card-body__desc">
									 Стоимость оси 800 - 1 500₽
								</div>
							</div>
							<div class="card-body__price">
								 300₽
							</div>
						</div>
						<div class="card-body__line d-flex align-items-center">
							<div class="card-body__left">
								<div class="card-body__name">
									 Установка поддержки заднего крыла
								</div>
								<div class="card-body__desc">
									 Поддержка включена, будет дешевле при комплексном работах
								</div>
							</div>
							<div class="card-body__price">
								 1 000₽
							</div>
						</div>
						<div class="card-body__line d-flex align-items-center">
							<div class="card-body__left">
								<div class="card-body__name">
									 Установка комплектующих (звонок, крылья, дисплей и т.д)
								</div>
							</div>
							<div class="card-body__price">
								 500₽
							</div>
						</div>
						<div class="card-body__line d-flex align-items-center">
							<div class="card-body__left">
								<div class="card-body__name">
									 Гидроизоляция дэки
								</div>
								<div class="card-body__desc">
									 Будет дешевле при комплексном работах
								</div>
							</div>
							<div class="card-body__price">
								 2 000₽
							</div>
						</div>
						<div class="card-body__line d-flex align-items-center">
							<div class="card-body__left">
								<div class="card-body__name">
									 Гидроизоляция кнопки включения
								</div>
							</div>
							<div class="card-body__price">
								 250₽
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header" id="headingThree">
					<h3 class="mb-0 d-flex align-items-center"><img src="/bitrix/templates/motion/images/a2.svg"> <button class="btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Электрика </button> </h3>
				</div>
				<div class="collapse" id="collapseThree" aria-labelledby="headingThree" data-parent="#accordionExample">
					<div class="card-body">
						<div class="card-body__header d-flex">
							<div class="card-body__left">
								 Услуга
							</div>
							<div class="card-body__right">
								 Стоимость
							</div>
						</div>
						<div class="card-body__line d-flex align-items-center">
							<div class="card-body__left">
								<div class="card-body__name">
									 Ремонт батареи
								</div>
								<div class="card-body__desc">
									 Точная стоимость после диагностики
								</div>
							</div>
							<div class="card-body__price">
								 1 000 - 3 000₽
							</div>
						</div>
						<div class="card-body__line d-flex align-items-center">
							<div class="card-body__left">
								<div class="card-body__name">
									 Ремонт контроллера
								</div>
								<div class="card-body__desc">
									 Точная стоимость после диагностики
								</div>
							</div>
							<div class="card-body__price">
								 1 000 - 2 500₽
							</div>
						</div>
						<div class="card-body__line d-flex align-items-center">
							<div class="card-body__left">
								<div class="card-body__name">
									 Обновление ПО
								</div>
								<div class="card-body__desc">
									 Если самокат заблокирован - 1 000₽, при комплексный работах бесплатно
								</div>
							</div>
							<div class="card-body__price">
								 Бесплатно
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header" id="headingFour">
					<h3 class="mb-0 d-flex align-items-center"><img src="/bitrix/templates/motion/images/a6.svg"> <button class="btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">Комплексная подготовка </button> </h3>
				</div>
				<div class="collapse" id="collapseFour" aria-labelledby="headingFour" data-parent="#accordionExample">
					<div class="card-body">
						<div class="card-body__header d-flex">
							<div class="card-body__left">
								 Услуга
							</div>
							<div class="card-body__right">
								 Стоимость
							</div>
						</div>
						<div class="card-body__line d-flex align-items-center">
							<div class="card-body__left">
								<div class="card-body__name">
									 Комплекс «Начальный»
								</div>
								<div class="card-body__desc">
									 Подробно на странице комплексных работ
								</div>
							</div>
							<div class="card-body__price">
								2 500 ₽
							</div>
						</div>
						<div class="card-body__line d-flex align-items-center">
							<div class="card-body__left">
								<div class="card-body__name">
									 Комплекс «Полный»
								</div>
								<div class="card-body__desc">
									 Подробно на странице комплексных работ
								</div>
							</div>
							<div class="card-body__price">
								8 000₽
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 <br>