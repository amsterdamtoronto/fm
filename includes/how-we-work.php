<div class="row">
    <h2 class="col-lg-12 title mb-3 mt-5">
        Как мы работаем </h2>
    <div class="col-lg-6">
        <p>
            Мы ценим простоту и открытость в людях, всегда стараемся решить проблему или помочь реализовать пожелания клиента
        </p>
    </div>
</div>
<div class="row mt-5">
    <div class="col-lg-9 line">
        <div class="line-red">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="work-block d-flex flex-column">
            <span>1</span>
            <div class="work-block__title">
                Согласовываем кол-ва самокатов и опции
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="work-block d-flex flex-column">
           <span>2</span>
            <div class="work-block__title">
                Заключаем договор
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="work-block d-flex flex-column">
            <span>3</span>
            <div class="work-block__title">
                Предоставляем доступ к платформе
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="work-block d-flex flex-column">
            <span>4</span>
            <div class="work-block__title">
                Доставляем самокаты
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="work-block d-flex flex-column">
            <span>5</span>
            <div class="work-block__title">
                Мы полностью берем обслуживание самокатов на себя
            </div>
        </div>
    </div>
</div>