<section class="advantages">
<div class="container">
	<div class="row d-flex justify-content-between">
		<div class="col-lg-5">
			<div class="advantages__left">
 <img src="null" class="lazy" data-original="/bitrix/templates/motion/images/alogo.svg">
				<h1 class="advantages__title">
				Ремонт, продажа и аренда для бизнеса&nbsp; &nbsp; &nbsp; </h1>
				<div class="advantages__desc">
					 Xiaomi, Kugoo, Ninebot Модернизация, тюнинг,&nbsp;запчасти в Москве и Краснодаре.
				</div>
 <a class="btn" href="#" data-toggle="modal" data-target="#exampleModal2">КОНСУЛЬТАЦИЯ</a>
			</div>
		</div>
		<div class="col-lg-7 d-flex flex-wrap justify-content-between">
			<div class="advantages__block d-flex flex-column">
 <img src="null" class="lazy" data-original="/bitrix/templates/motion/images/01.svg">
				<div class="advantages__t">
					 Cервисные центры
				</div>
				<div class="advantages__d">
					 Калужская, ВДНХ и Путилково<br>
					 в Москве
				</div>
			</div>
			<div class="advantages__block d-flex flex-column">
 <img src="null" class="lazy" data-original="/bitrix/templates/motion/images/02.svg">
				<div class="advantages__t">
					 Запчасти
				</div>
				<div class="advantages__d">
					 Свой склад запчстей, гарантирует оперативный ремонт
				</div>
			</div>
			<div class="advantages__block d-flex flex-column">
 <img src="null" class="lazy" data-original="/bitrix/templates/motion/images/03.svg">
				<div class="advantages__t">
					 Гарантия 1 год
				</div>
				<div class="advantages__d">
					 На новые самокаты с нашей подготовкой и ремонт
				</div>
			</div>
			<div class="advantages__block d-flex flex-column">
 <img src="null" class="lazy" data-original="/bitrix/templates/motion/images/04.svg">
				<div class="advantages__t">
					 Выездной ремонт&nbsp;
				</div>
				<div class="advantages__d">
					 Мастера cделают все работу у вас дома или в офисе&nbsp;
				</div>
			</div>
		</div>
	</div>
</div>
 </section>