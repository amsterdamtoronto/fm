<div class="row">
	<h2 class="col-lg-12 title">
		Мастерская и сервис
	</h2>
	<div class="col-lg-6 col-md-6">
		<div class="service-block">
			<div class="service-block__img">
				<img class="lazy"  data-original="/bitrix/templates/motion/images/ser1.jpg" class="service-block__image">
			</div>
			<div class="service-block__item d-flex justify-content-between align-items-end">
				<div class="service-block__left">
					<div class="service-block__category">
						Тюнинг
					</div>
					<div class="service-block__name">
						Модернизация и тюнинг
					</div>
					<div class="service-block__desc">
						Прокачаем Ваш электросамокат
					</div>
				</div>
				<a class="elipse-btn" href="/modernizatsiya/"><img class="lazy"  data-original="/bitrix/templates/motion/images/r1.svg"></a>
			</div>
		</div>
	</div>
	<div class="col-lg-6 col-md-6">
		<div class="service-block">
			<div class="service-block__img">
				<img class="lazy" data-original="/bitrix/templates/motion/images/ser2.jpg" class="service-block__image">
			</div>
			<div class="service-block__item d-flex justify-content-between align-items-end">
				<div class="service-block__left">
					<div class="service-block__category">
						Ремонт
					</div>
					<div class="service-block__name">
						Диагностика и ремонт
					</div>
					<div class="service-block__desc">
						Ремонт электросамокатов любой сложности
					</div>
				</div>
				<a class="elipse-btn" href="/remont/"><img  class="lazy"  data-original="/bitrix/templates/motion/images/r1.svg"></a>
			</div>
		</div>
	</div>
</div>
<br>