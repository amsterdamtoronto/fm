<div class="row">
	<h2 class="col-lg-12 title">
		Аренда и прокат
	</h2>
</div>
<div class="row main-rent">
	<div class="col-lg-6 col-md-6">
		<div class="rent-block">
			<div class="rent-block__img">
				<img class="lazy" data-original="/bitrix/templates/motion/images/r1.png">
			</div>
			<div class="rent-block__main">
				<div class="rent-block__pre">
					Для бизнеса
				</div>
				<div class="rent-block__name">
					Аренда электросамокатов для сервисов доставки
				</div>
				<div class="rent-block__desc">
					С помощью FM. Delivery
				</div>
				<a class="btn" href="/b2b-arenda/">взять в аренду</a>
			</div>
		</div>
	</div>
	<div class="col-lg-6 col-md-6">
		<div class="rent-block rent-red">
			<div class="rent-block__img">
				<img class="lazy" data-original="/bitrix/templates/motion/images/r2.png">
			</div>
			<div class="rent-block__main">
				<div class="rent-block__pre">
					Для мероприятий
				</div>
				<div class="rent-block__name">
					Прокат электросамокатов для эвентов, экскурсий
				</div>
				<div class="rent-block__desc">
					С помощью FASTMOTION
				</div>
				<a class="btn" href="/dlya-dostavki/">взять в прокат</a>
			</div>
		</div>
	</div>
</div>
<br>