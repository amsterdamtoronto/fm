<h2 class="title mb-4">Комплексная модернизация и тюнинг электросамокатов</h2>

            <div class="tuning js-tuning js-tuning-fixed-top-container">
                <div class="tuning-top">
                    <div class="row">
                        <div class="col-12 col-lg-4">
                            <div class="tuning-header">
                                <div class="row">
                                    <div class="col-xxs-12 col-6 col-md-5 col-lg-12">
                                        <div class="tuning-header__model js-tuning-container is-active" data-tuning="6">
                                            <div class="row">
                                                <div class="d-none d-sm-block col-4">
                                                    <div class="tuning-header__img">
                                                        <img src="/images/tarifs/274a0f0d1e4e5da13eae7c5ede7871e6.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-8">
                                                    <div class="tuning-header__body-container">
                                                        <div class="tuning-header__body">
                                                            <div class="tuning-header__subtitle">Выбранная модель</div>
                                                            <div class="tuning-header__title">Xiaomi M365</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tuning-header__model js-tuning-container" data-tuning="7">
                                            <div class="row">
                                                <div class="d-none d-sm-block col-4">
                                                    <div class="tuning-header__img">
                                                        <img src="/images/tarifs/55a0d6aee4e719301815a12c9043aa1a.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-8">
                                                    <div class="tuning-header__body-container">
                                                        <div class="tuning-header__body">
                                                            <div class="tuning-header__subtitle">Выбранная модель</div>
                                                            <div class="tuning-header__title">1S</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tuning-header__model js-tuning-container" data-tuning="9">
                                            <div class="row">
                                                <div class="d-none d-sm-block col-4">
                                                    <div class="tuning-header__img">
                                                        <img src="/images/tarifs/e1ace6f9669cd3a4c6c5425fb8974e51.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-8">
                                                    <div class="tuning-header__body-container">
                                                        <div class="tuning-header__body">
                                                            <div class="tuning-header__subtitle">Выбранная модель</div>
                                                            <div class="tuning-header__title">Xiaomi PRO</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tuning-header__model js-tuning-container" data-tuning="10">
                                            <div class="row">
                                                <div class="d-none d-sm-block col-4">
                                                    <div class="tuning-header__img">
                                                        <img src="/images/tarifs/e1ace6f9669cd3a4c6c5425fb8974e51.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-8">
                                                    <div class="tuning-header__body-container">
                                                        <div class="tuning-header__body">
                                                            <div class="tuning-header__subtitle">Выбранная модель</div>
                                                            <div class="tuning-header__title">Xiaomi PRO 2</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tuning-header__model js-tuning-container" data-tuning="12">
                                            <div class="row">
                                                <div class="d-none d-sm-block col-4">
                                                    <div class="tuning-header__img">
                                                        <img src="/images/tarifs/e1ace6f9669cd3a4c6c5425fb8974e51.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-8">
                                                    <div class="tuning-header__body-container">
                                                        <div class="tuning-header__body">
                                                            <div class="tuning-header__subtitle">Выбранная модель</div>
                                                            <div class="tuning-header__title">Ninebot MAX G30/LP</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tuning-header__model js-tuning-container" data-tuning="8">
                                            <div class="row">
                                                <div class="d-none d-sm-block col-4">
                                                    <div class="tuning-header__img">
                                                        <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/none.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-8">
                                                    <div class="tuning-header__body-container">
                                                        <div class="tuning-header__body">
                                                            <div class="tuning-header__subtitle">Выбранная модель</div>
                                                            <div class="tuning-header__title">Ninebot ES1</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tuning-header__model js-tuning-container" data-tuning="11">
                                            <div class="row">
                                                <div class="d-none d-sm-block col-4">
                                                    <div class="tuning-header__img">
                                                        <img src="/images/tarifs/e1ace6f9669cd3a4c6c5425fb8974e51.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-8">
                                                    <div class="tuning-header__body-container">
                                                        <div class="tuning-header__body">
                                                            <div class="tuning-header__subtitle">Выбранная модель</div>
                                                            <div class="tuning-header__title">Ninebot ES2/4</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xxs-12 col-6 col-md-7 col-lg-12">
                                        <div class="tuning-header__selector-container">
                                            <div class="tuning-header__selector tuning-selector js-tuning-selector">
                                                <a class="btn-selector js-tuning-selector-btn" href="#">
                                                    <span class="d-none d-md-inline">Выбрать другую модель</span>
                                                    <span class="d-md-none">Изменить</span>
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/chevron-down.svg" alt="">
                                                </a>
                                                <div class="tuning-header-selector__overlay tuning-selector-overlay js-tuning-selector-close"></div>
                                                <div class="tuning-header-selector__popup js-custom-scroll tuning-selector-popup">
                                                    <div class="tuning-header-selector__models">
                                                        <div class="tuning-header-selector__models-title">Выберите модель</div>
                                                        <div class="tuning-header-selector__model">
                                                            <div class="tuning-header-selector__model-name">Модели Xiaomi</div>
                                                            <div class="tuning-header-selector__items">
                                                                <a href="#" class="tuning-header-selector__item js-tuning-select-model" data-tuning="6">
																	<span class="tuning-header-selector__item-img">
																																					<img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/none.png" alt="">
																																			</span>
                                                                    <span class="tuning-header-selector__item-name">Xiaomi M365</span>
                                                                    <span class="tuning-header-selector__item-selected js-tuning-select-model-selected" data-tuning="6">Выбрано</span>
                                                                </a>
                                                                <a href="#" class="tuning-header-selector__item js-tuning-select-model" data-tuning="7">
																	<span class="tuning-header-selector__item-img">
																																					<img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/none.png" alt="">
																																			</span>
                                                                    <span class="tuning-header-selector__item-name">1S</span>
                                                                    <span class="tuning-header-selector__item-selected js-tuning-select-model-selected" data-tuning="7"></span>
                                                                </a>
                                                                <a href="#" class="tuning-header-selector__item js-tuning-select-model" data-tuning="9">
																	<span class="tuning-header-selector__item-img">
																																					<img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/none.png" alt="">
																																			</span>
                                                                    <span class="tuning-header-selector__item-name">Xiaomi PRO</span>
                                                                    <span class="tuning-header-selector__item-selected js-tuning-select-model-selected" data-tuning="9"></span>
                                                                </a>
                                                                <a href="#" class="tuning-header-selector__item js-tuning-select-model" data-tuning="10">
																	<span class="tuning-header-selector__item-img">
																																					<img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/none.png" alt="">
																																			</span>
                                                                    <span class="tuning-header-selector__item-name">Xiaomi PRO 2</span>
                                                                    <span class="tuning-header-selector__item-selected js-tuning-select-model-selected" data-tuning="10"></span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="tuning-header-selector__model">
                                                            <div class="tuning-header-selector__model-name">Модели Ninebot</div>
                                                            <div class="tuning-header-selector__items">
                                                                <a href="#" class="tuning-header-selector__item js-tuning-select-model" data-tuning="12">
																	<span class="tuning-header-selector__item-img">
																																					<img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/none.png" alt="">
																																			</span>
                                                                    <span class="tuning-header-selector__item-name">Ninebot MAX G30/LP</span>
                                                                    <span class="tuning-header-selector__item-selected js-tuning-select-model-selected" data-tuning="12"></span>
                                                                </a>
                                                                <a href="#" class="tuning-header-selector__item js-tuning-select-model" data-tuning="8">
																	<span class="tuning-header-selector__item-img">
																																					<img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/none.png" alt="">
																																			</span>
                                                                    <span class="tuning-header-selector__item-name">Ninebot ES1</span>
                                                                    <span class="tuning-header-selector__item-selected js-tuning-select-model-selected" data-tuning="8"></span>
                                                                </a>
                                                                <a href="#" class="tuning-header-selector__item js-tuning-select-model" data-tuning="11">
																	<span class="tuning-header-selector__item-img">
																																					<img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/none.png" alt="">
																																			</span>
                                                                    <span class="tuning-header-selector__item-name">Ninebot ES2/4</span>
                                                                    <span class="tuning-header-selector__item-selected js-tuning-select-model-selected" data-tuning="11"></span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-none d-lg-block col-8">
                            <div class="tuning-variants-headers js-tuning-container is-active" data-tuning="6">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="tuning-variants-header">
                                            <div class="tuning-variants-header__info-container">
                                                <div class="tuning-variants-header__info">
                                                    <div class="tuning-variants-header__name">Комплекс</div>
                                                    <div class="tuning-variants-header__price">9 000₽</div>
                                                </div>
                                            </div>
                                            <div class="tuning-variants-header__buy">
                                                <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="tuning-variants-header">
                                            <div class="tuning-variants-header__info-container">
                                                <div class="tuning-variants-header__info">
                                                    <div class="tuning-variants-header__name">Гидроизоляция+</div>
                                                    <div class="tuning-variants-header__price">6 500₽</div>
                                                </div>
                                            </div>
                                            <div class="tuning-variants-header__buy">
                                                <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="tuning-variants-header">
                                            <div class="tuning-variants-header__info-container">
                                                <div class="tuning-variants-header__info">
                                                    <div class="tuning-variants-header__name">Базовая подготовка</div>
                                                    <div class="tuning-variants-header__price">3 000₽</div>
                                                </div>
                                            </div>
                                            <div class="tuning-variants-header__buy">
                                                <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tuning-variants-headers js-tuning-container" data-tuning="7">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="tuning-variants-header">
                                            <div class="tuning-variants-header__info-container">
                                                <div class="tuning-variants-header__info">
                                                    <div class="tuning-variants-header__name">Комплекс</div>
                                                    <div class="tuning-variants-header__price">9 000₽</div>
                                                </div>
                                            </div>
                                            <div class="tuning-variants-header__buy">
                                                <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="tuning-variants-header">
                                            <div class="tuning-variants-header__info-container">
                                                <div class="tuning-variants-header__info">
                                                    <div class="tuning-variants-header__name"> Гидроизоляция+</div>
                                                    <div class="tuning-variants-header__price">6 500₽</div>
                                                </div>
                                            </div>
                                            <div class="tuning-variants-header__buy">
                                                <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="tuning-variants-header">
                                            <div class="tuning-variants-header__info-container">
                                                <div class="tuning-variants-header__info">
                                                    <div class="tuning-variants-header__name">Базовая подготовка</div>
                                                    <div class="tuning-variants-header__price">3 000₽</div>
                                                </div>
                                            </div>
                                            <div class="tuning-variants-header__buy">
                                                <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tuning-variants-headers js-tuning-container" data-tuning="9">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="tuning-variants-header">
                                            <div class="tuning-variants-header__info-container">
                                                <div class="tuning-variants-header__info">
                                                    <div class="tuning-variants-header__name">Комплекс</div>
                                                    <div class="tuning-variants-header__price">10 000₽</div>
                                                </div>
                                            </div>
                                            <div class="tuning-variants-header__buy">
                                                <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="tuning-variants-header">
                                            <div class="tuning-variants-header__info-container">
                                                <div class="tuning-variants-header__info">
                                                    <div class="tuning-variants-header__name">Гидроизоляция+</div>
                                                    <div class="tuning-variants-header__price">6 500₽</div>
                                                </div>
                                            </div>
                                            <div class="tuning-variants-header__buy">
                                                <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="tuning-variants-header">
                                            <div class="tuning-variants-header__info-container">
                                                <div class="tuning-variants-header__info">
                                                    <div class="tuning-variants-header__name">Базовая подготовка</div>
                                                    <div class="tuning-variants-header__price">3 000₽</div>
                                                </div>
                                            </div>
                                            <div class="tuning-variants-header__buy">
                                                <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tuning-variants-headers js-tuning-container" data-tuning="10">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="tuning-variants-header">
                                            <div class="tuning-variants-header__info-container">
                                                <div class="tuning-variants-header__info">
                                                    <div class="tuning-variants-header__name">Комплекс</div>
                                                    <div class="tuning-variants-header__price">8 500₽</div>
                                                </div>
                                            </div>
                                            <div class="tuning-variants-header__buy">
                                                <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="tuning-variants-header">
                                            <div class="tuning-variants-header__info-container">
                                                <div class="tuning-variants-header__info">
                                                    <div class="tuning-variants-header__name">Гидроизоляция+</div>
                                                    <div class="tuning-variants-header__price">6 500₽</div>
                                                </div>
                                            </div>
                                            <div class="tuning-variants-header__buy">
                                                <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="tuning-variants-header">
                                            <div class="tuning-variants-header__info-container">
                                                <div class="tuning-variants-header__info">
                                                    <div class="tuning-variants-header__name">Базовая подготовка</div>
                                                    <div class="tuning-variants-header__price">2 000₽</div>
                                                </div>
                                            </div>
                                            <div class="tuning-variants-header__buy">
                                                <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tuning-variants-headers js-tuning-container" data-tuning="12">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="tuning-variants-header">
                                            <div class="tuning-variants-header__info-container">
                                                <div class="tuning-variants-header__info">
                                                    <div class="tuning-variants-header__name">Комплекс</div>
                                                    <div class="tuning-variants-header__price">9 000₽</div>
                                                </div>
                                            </div>
                                            <div class="tuning-variants-header__buy">
                                                <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="tuning-variants-header">
                                            <div class="tuning-variants-header__info-container">
                                                <div class="tuning-variants-header__info">
                                                    <div class="tuning-variants-header__name">Гидроизоляция+</div>
                                                    <div class="tuning-variants-header__price">6 000₽</div>
                                                </div>
                                            </div>
                                            <div class="tuning-variants-header__buy">
                                                <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="tuning-variants-header">
                                            <div class="tuning-variants-header__info-container">
                                                <div class="tuning-variants-header__info">
                                                    <div class="tuning-variants-header__name">Начальная подготвока</div>
                                                    <div class="tuning-variants-header__price">1 500₽</div>
                                                </div>
                                            </div>
                                            <div class="tuning-variants-header__buy">
                                                <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tuning-variants-headers js-tuning-container" data-tuning="8">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="tuning-variants-header">
                                            <div class="tuning-variants-header__info-container">
                                                <div class="tuning-variants-header__info">
                                                    <div class="tuning-variants-header__name">ES1</div>
                                                    <div class="tuning-variants-header__price">6 500₽</div>
                                                </div>
                                            </div>
                                            <div class="tuning-variants-header__buy">
                                                <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tuning-variants-headers js-tuning-container" data-tuning="11">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="tuning-variants-header">
                                            <div class="tuning-variants-header__info-container">
                                                <div class="tuning-variants-header__info">
                                                    <div class="tuning-variants-header__name"> ES2/4</div>
                                                    <div class="tuning-variants-header__price">8 500₽</div>
                                                </div>
                                            </div>
                                            <div class="tuning-variants-header__buy">
                                                <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tuning-bottom d-none d-lg-block">
                    <div class="tuning-bottom__properties js-tuning-container is-active" data-tuning="6">
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Герметизация дэки, мотора и кнопки включения</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Замена подшипников мотора (NTN, NSK с индексом 2rs)</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Замена подшипников заднего колеса (NTN, NSK с индексом 2rs)</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Установка поддержки заднего крыла</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Замена винтов крышки дэки на потайные</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    1 000₽																													</div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Усиление контроллера</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Настройка и регулировка всех компонентов и узлов самоката. (Тормаза и узел складывания)</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Установка кастомной прошивки, увеличивающей скорость и мощность самоката</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Перенос подножки самоката на переднюю часть (опция). Актуальная только для версии PRO/PRO2</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Резинки на ручку тормоза и подножку</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Наклейки fast-motion ;)</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tuning-bottom__properties js-tuning-container" data-tuning="7">
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Герметизация дэки, мотора и кнопки включения</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Замена подшипников мотора (NTN, NSK с индексом 2rs)</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Замена подшипников заднего колеса (NTN, NSK с индексом 2rs)</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Установка поддержки заднего крыла</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Замена винтов крышки дэки на потайные</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    1 000₽																													</div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Усиление контроллера</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Настройка и регулировка всех компонентов и узлов самоката. (Тормаза и узел складывания)</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Установка кастомной прошивки, увеличивающей скорость и мощность самоката</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    500₽																													</div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    500₽																													</div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    1 000₽																													</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Перенос подножки самоката на переднюю часть (опция). Актуальная только для версии PRO/PRO2</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Резинки на ручку тормоза и подножку</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Наклейки fast-motion ;)</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tuning-bottom__properties js-tuning-container" data-tuning="9">
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Герметизация дэки, мотора и кнопки включения</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Замена подшипников мотора (NTN, NSK с индексом 2rs)</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Замена подшипников заднего колеса (NTN, NSK с индексом 2rs)</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Установка поддержки заднего крыла</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Замена винтов крышки дэки на потайные</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    1 000₽																													</div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Усиление контроллера</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    1 000₽																													</div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Настройка и регулировка всех компонентов и узлов самоката. (Тормаза и узел складывания)</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Установка кастомной прошивки, увеличивающей скорость и мощность самоката</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Перенос подножки самоката на переднюю часть (опция). Актуальная только для версии PRO/PRO2</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Резинки на ручку тормоза и подножку</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Наклейки fast-motion ;)</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tuning-bottom__properties js-tuning-container" data-tuning="10">
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Герметизация дэки, мотора и кнопки включения</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Замена подшипников мотора (NTN, NSK с индексом 2rs)</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Замена подшипников заднего колеса (NTN, NSK с индексом 2rs)</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Установка поддержки заднего крыла</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Замена винтов крышки дэки на потайные</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    1 000₽																													</div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Усиление контроллера</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Настройка и регулировка всех компонентов и узлов самоката. (Тормаза и узел складывания)</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Установка кастомной прошивки, увеличивающей скорость и мощность самоката</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    500₽																													</div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    500₽																													</div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    1 000₽																													</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Перенос подножки самоката на переднюю часть (опция). Актуальная только для версии PRO/PRO2</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Резинки на ручку тормоза и подножку</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Наклейки fast-motion ;)</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tuning-bottom__properties js-tuning-container" data-tuning="12">
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Замена подшипников мотора (NTN, NSK с индексом 2rs)</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Замена подшипников переднего колеса  (NTN, NSK с индексом 2rs)</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Установка поддержки заднего крыла</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Резинки на ручку тормоза и подножку</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Наклейки fast-motion ;)</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Гидроизоляция деки и мотора</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Настройка и регулировка всех компонентов и узлов самоката. (Тормаза и узел складывания )</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tuning-bottom__properties js-tuning-container" data-tuning="8">
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Установка 8-ми гранной рулевой втулки</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Доработка узла складывания (замена втулок)</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title"> Доработка заднего амортизатора</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Замена втулок заднего амортизатора</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Замена втулок заднего колеса</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Гидроизоляция разъемов подсветки</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Установка демпферов узла складывания</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tuning-bottom__properties js-tuning-container" data-tuning="11">
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Установка 8-ми гранной рулевой втулки</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Доработка узла складывания (замена втулок)</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title"> Доработка заднего амортизатора</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Замена втулок заднего амортизатора</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Замена втулок заднего колеса</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Гидроизоляция разъемов подсветки</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-bottom__property">
                            <div class="row">
                                <div class="col-4">
                                    <div class="tuning-bottom__property-title">Установка демпферов узла складывания</div>
                                </div>
                                <div class="col-8">
                                    <div class="tuning-bottom__property-variants">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="tuning-bottom__property-value">
                                                    <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tuning-bottom tuning-bottom-mobile d-lg-none">
                    <div class="tuning-bottom-mobile__variants js-tuning-slider js-tuning-container is-active" data-tuning="6">
                        <div class="swiper-wrapper">
                            <div class="tuning-bottom-mobile__variant swiper-slide">
                                <div class="tuning-bottom-mobile__variant-header js-tuning-slider-header">
                                    <div class="tuning-variants-header__name tuning-bottom-mobile__variant-name">Комплекс</div>
                                    <div class="tuning-variants-header__price tuning-bottom-mobile__variant-price">9 000₽</div>
                                </div>
                                <div class="tuning-bottom-mobile__properties">
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Герметизация дэки, мотора и кнопки включения</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников мотора (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников заднего колеса (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Установка поддержки заднего крыла</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена винтов крышки дэки на потайные</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Усиление контроллера</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Настройка и регулировка всех компонентов и узлов самоката. (Тормаза и узел складывания)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Установка кастомной прошивки, увеличивающей скорость и мощность самоката</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Перенос подножки самоката на переднюю часть (опция). Актуальная только для версии PRO/PRO2</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Резинки на ручку тормоза и подножку</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Наклейки fast-motion ;)</div>
                                    </div>
                                </div>
                                <div class="tuning-variants-header__buy">
                                    <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                </div>
                            </div>
                            <div class="tuning-bottom-mobile__variant swiper-slide">
                                <div class="tuning-bottom-mobile__variant-header js-tuning-slider-header">
                                    <div class="tuning-variants-header__name tuning-bottom-mobile__variant-name">Гидроизоляция+</div>
                                    <div class="tuning-variants-header__price tuning-bottom-mobile__variant-price">6 500₽</div>
                                </div>
                                <div class="tuning-bottom-mobile__properties">
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Герметизация дэки, мотора и кнопки включения</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников мотора (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников заднего колеса (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Установка поддержки заднего крыла</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            1 000₽																									</div>
                                        <div class="tuning-bottom__property-title">Замена винтов крышки дэки на потайные</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Усиление контроллера</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Настройка и регулировка всех компонентов и узлов самоката. (Тормаза и узел складывания)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Установка кастомной прошивки, увеличивающей скорость и мощность самоката</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Перенос подножки самоката на переднюю часть (опция). Актуальная только для версии PRO/PRO2</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Резинки на ручку тормоза и подножку</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Наклейки fast-motion ;)</div>
                                    </div>
                                </div>
                                <div class="tuning-variants-header__buy">
                                    <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                </div>
                            </div>
                            <div class="tuning-bottom-mobile__variant swiper-slide">
                                <div class="tuning-bottom-mobile__variant-header js-tuning-slider-header">
                                    <div class="tuning-variants-header__name tuning-bottom-mobile__variant-name">Базовая подготовка</div>
                                    <div class="tuning-variants-header__price tuning-bottom-mobile__variant-price">3 000₽</div>
                                </div>
                                <div class="tuning-bottom-mobile__properties">
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Герметизация дэки, мотора и кнопки включения</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников мотора (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников заднего колеса (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Установка поддержки заднего крыла</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена винтов крышки дэки на потайные</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Усиление контроллера</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Настройка и регулировка всех компонентов и узлов самоката. (Тормаза и узел складывания)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Установка кастомной прошивки, увеличивающей скорость и мощность самоката</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Перенос подножки самоката на переднюю часть (опция). Актуальная только для версии PRO/PRO2</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Резинки на ручку тормоза и подножку</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Наклейки fast-motion ;)</div>
                                    </div>
                                </div>
                                <div class="tuning-variants-header__buy">
                                    <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-slider-button-prev swiper-button-prev js-tuning-slider-button-prev">
                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/chevron-left.svg" alt="">
                        </div>
                        <div class="tuning-slider-button-next swiper-button-next js-tuning-slider-button-next">
                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/chevron-right.svg" alt="">
                        </div>
                    </div>
                    <div class="tuning-bottom-mobile__variants js-tuning-slider js-tuning-container" data-tuning="7">
                        <div class="swiper-wrapper">
                            <div class="tuning-bottom-mobile__variant swiper-slide">
                                <div class="tuning-bottom-mobile__variant-header js-tuning-slider-header">
                                    <div class="tuning-variants-header__name tuning-bottom-mobile__variant-name">Комплекс</div>
                                    <div class="tuning-variants-header__price tuning-bottom-mobile__variant-price">9 000₽</div>
                                </div>
                                <div class="tuning-bottom-mobile__properties">
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Герметизация дэки, мотора и кнопки включения</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников мотора (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников заднего колеса (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Установка поддержки заднего крыла</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена винтов крышки дэки на потайные</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Усиление контроллера</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Настройка и регулировка всех компонентов и узлов самоката. (Тормаза и узел складывания)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            500₽																									</div>
                                        <div class="tuning-bottom__property-title">Установка кастомной прошивки, увеличивающей скорость и мощность самоката</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Перенос подножки самоката на переднюю часть (опция). Актуальная только для версии PRO/PRO2</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Резинки на ручку тормоза и подножку</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Наклейки fast-motion ;)</div>
                                    </div>
                                </div>
                                <div class="tuning-variants-header__buy">
                                    <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                </div>
                            </div>
                            <div class="tuning-bottom-mobile__variant swiper-slide">
                                <div class="tuning-bottom-mobile__variant-header js-tuning-slider-header">
                                    <div class="tuning-variants-header__name tuning-bottom-mobile__variant-name"> Гидроизоляция+</div>
                                    <div class="tuning-variants-header__price tuning-bottom-mobile__variant-price">6 500₽</div>
                                </div>
                                <div class="tuning-bottom-mobile__properties">
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Герметизация дэки, мотора и кнопки включения</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников мотора (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников заднего колеса (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Установка поддержки заднего крыла</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            1 000₽																									</div>
                                        <div class="tuning-bottom__property-title">Замена винтов крышки дэки на потайные</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Усиление контроллера</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Настройка и регулировка всех компонентов и узлов самоката. (Тормаза и узел складывания)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            500₽																									</div>
                                        <div class="tuning-bottom__property-title">Установка кастомной прошивки, увеличивающей скорость и мощность самоката</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Перенос подножки самоката на переднюю часть (опция). Актуальная только для версии PRO/PRO2</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Резинки на ручку тормоза и подножку</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Наклейки fast-motion ;)</div>
                                    </div>
                                </div>
                                <div class="tuning-variants-header__buy">
                                    <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                </div>
                            </div>
                            <div class="tuning-bottom-mobile__variant swiper-slide">
                                <div class="tuning-bottom-mobile__variant-header js-tuning-slider-header">
                                    <div class="tuning-variants-header__name tuning-bottom-mobile__variant-name">Базовая подготовка</div>
                                    <div class="tuning-variants-header__price tuning-bottom-mobile__variant-price">3 000₽</div>
                                </div>
                                <div class="tuning-bottom-mobile__properties">
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Герметизация дэки, мотора и кнопки включения</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников мотора (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников заднего колеса (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Установка поддержки заднего крыла</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена винтов крышки дэки на потайные</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Усиление контроллера</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Настройка и регулировка всех компонентов и узлов самоката. (Тормаза и узел складывания)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            1 000₽																									</div>
                                        <div class="tuning-bottom__property-title">Установка кастомной прошивки, увеличивающей скорость и мощность самоката</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Перенос подножки самоката на переднюю часть (опция). Актуальная только для версии PRO/PRO2</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Резинки на ручку тормоза и подножку</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Наклейки fast-motion ;)</div>
                                    </div>
                                </div>
                                <div class="tuning-variants-header__buy">
                                    <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-slider-button-prev swiper-button-prev js-tuning-slider-button-prev">
                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/chevron-left.svg" alt="">
                        </div>
                        <div class="tuning-slider-button-next swiper-button-next js-tuning-slider-button-next">
                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/chevron-right.svg" alt="">
                        </div>
                    </div>
                    <div class="tuning-bottom-mobile__variants js-tuning-slider js-tuning-container" data-tuning="9">
                        <div class="swiper-wrapper">
                            <div class="tuning-bottom-mobile__variant swiper-slide">
                                <div class="tuning-bottom-mobile__variant-header js-tuning-slider-header">
                                    <div class="tuning-variants-header__name tuning-bottom-mobile__variant-name">Комплекс</div>
                                    <div class="tuning-variants-header__price tuning-bottom-mobile__variant-price">10 000₽</div>
                                </div>
                                <div class="tuning-bottom-mobile__properties">
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Герметизация дэки, мотора и кнопки включения</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников мотора (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников заднего колеса (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Установка поддержки заднего крыла</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена винтов крышки дэки на потайные</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Усиление контроллера</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Настройка и регулировка всех компонентов и узлов самоката. (Тормаза и узел складывания)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Установка кастомной прошивки, увеличивающей скорость и мощность самоката</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Перенос подножки самоката на переднюю часть (опция). Актуальная только для версии PRO/PRO2</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Резинки на ручку тормоза и подножку</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Наклейки fast-motion ;)</div>
                                    </div>
                                </div>
                                <div class="tuning-variants-header__buy">
                                    <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                </div>
                            </div>
                            <div class="tuning-bottom-mobile__variant swiper-slide">
                                <div class="tuning-bottom-mobile__variant-header js-tuning-slider-header">
                                    <div class="tuning-variants-header__name tuning-bottom-mobile__variant-name">Гидроизоляция+</div>
                                    <div class="tuning-variants-header__price tuning-bottom-mobile__variant-price">6 500₽</div>
                                </div>
                                <div class="tuning-bottom-mobile__properties">
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Герметизация дэки, мотора и кнопки включения</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников мотора (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников заднего колеса (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Установка поддержки заднего крыла</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            1 000₽																									</div>
                                        <div class="tuning-bottom__property-title">Замена винтов крышки дэки на потайные</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            1 000₽																									</div>
                                        <div class="tuning-bottom__property-title">Усиление контроллера</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Настройка и регулировка всех компонентов и узлов самоката. (Тормаза и узел складывания)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Установка кастомной прошивки, увеличивающей скорость и мощность самоката</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Перенос подножки самоката на переднюю часть (опция). Актуальная только для версии PRO/PRO2</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Резинки на ручку тормоза и подножку</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Наклейки fast-motion ;)</div>
                                    </div>
                                </div>
                                <div class="tuning-variants-header__buy">
                                    <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                </div>
                            </div>
                            <div class="tuning-bottom-mobile__variant swiper-slide">
                                <div class="tuning-bottom-mobile__variant-header js-tuning-slider-header">
                                    <div class="tuning-variants-header__name tuning-bottom-mobile__variant-name">Базовая подготовка</div>
                                    <div class="tuning-variants-header__price tuning-bottom-mobile__variant-price">3 000₽</div>
                                </div>
                                <div class="tuning-bottom-mobile__properties">
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Герметизация дэки, мотора и кнопки включения</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников мотора (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников заднего колеса (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Установка поддержки заднего крыла</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена винтов крышки дэки на потайные</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Усиление контроллера</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Настройка и регулировка всех компонентов и узлов самоката. (Тормаза и узел складывания)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Установка кастомной прошивки, увеличивающей скорость и мощность самоката</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Перенос подножки самоката на переднюю часть (опция). Актуальная только для версии PRO/PRO2</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Резинки на ручку тормоза и подножку</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Наклейки fast-motion ;)</div>
                                    </div>
                                </div>
                                <div class="tuning-variants-header__buy">
                                    <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-slider-button-prev swiper-button-prev js-tuning-slider-button-prev">
                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/chevron-left.svg" alt="">
                        </div>
                        <div class="tuning-slider-button-next swiper-button-next js-tuning-slider-button-next">
                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/chevron-right.svg" alt="">
                        </div>
                    </div>
                    <div class="tuning-bottom-mobile__variants js-tuning-slider js-tuning-container" data-tuning="10">
                        <div class="swiper-wrapper">
                            <div class="tuning-bottom-mobile__variant swiper-slide">
                                <div class="tuning-bottom-mobile__variant-header js-tuning-slider-header">
                                    <div class="tuning-variants-header__name tuning-bottom-mobile__variant-name">Комплекс</div>
                                    <div class="tuning-variants-header__price tuning-bottom-mobile__variant-price">8 500₽</div>
                                </div>
                                <div class="tuning-bottom-mobile__properties">
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Герметизация дэки, мотора и кнопки включения</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников мотора (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников заднего колеса (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Установка поддержки заднего крыла</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена винтов крышки дэки на потайные</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Усиление контроллера</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Настройка и регулировка всех компонентов и узлов самоката. (Тормаза и узел складывания)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            500₽																									</div>
                                        <div class="tuning-bottom__property-title">Установка кастомной прошивки, увеличивающей скорость и мощность самоката</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Перенос подножки самоката на переднюю часть (опция). Актуальная только для версии PRO/PRO2</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Резинки на ручку тормоза и подножку</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Наклейки fast-motion ;)</div>
                                    </div>
                                </div>
                                <div class="tuning-variants-header__buy">
                                    <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                </div>
                            </div>
                            <div class="tuning-bottom-mobile__variant swiper-slide">
                                <div class="tuning-bottom-mobile__variant-header js-tuning-slider-header">
                                    <div class="tuning-variants-header__name tuning-bottom-mobile__variant-name">Гидроизоляция+</div>
                                    <div class="tuning-variants-header__price tuning-bottom-mobile__variant-price">6 500₽</div>
                                </div>
                                <div class="tuning-bottom-mobile__properties">
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Герметизация дэки, мотора и кнопки включения</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников мотора (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников заднего колеса (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Установка поддержки заднего крыла</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            1 000₽																									</div>
                                        <div class="tuning-bottom__property-title">Замена винтов крышки дэки на потайные</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Усиление контроллера</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Настройка и регулировка всех компонентов и узлов самоката. (Тормаза и узел складывания)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            500₽																									</div>
                                        <div class="tuning-bottom__property-title">Установка кастомной прошивки, увеличивающей скорость и мощность самоката</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Перенос подножки самоката на переднюю часть (опция). Актуальная только для версии PRO/PRO2</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Резинки на ручку тормоза и подножку</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Наклейки fast-motion ;)</div>
                                    </div>
                                </div>
                                <div class="tuning-variants-header__buy">
                                    <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                </div>
                            </div>
                            <div class="tuning-bottom-mobile__variant swiper-slide">
                                <div class="tuning-bottom-mobile__variant-header js-tuning-slider-header">
                                    <div class="tuning-variants-header__name tuning-bottom-mobile__variant-name">Базовая подготовка</div>
                                    <div class="tuning-variants-header__price tuning-bottom-mobile__variant-price">2 000₽</div>
                                </div>
                                <div class="tuning-bottom-mobile__properties">
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Герметизация дэки, мотора и кнопки включения</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников мотора (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников заднего колеса (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Установка поддержки заднего крыла</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена винтов крышки дэки на потайные</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Усиление контроллера</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Настройка и регулировка всех компонентов и узлов самоката. (Тормаза и узел складывания)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            1 000₽																									</div>
                                        <div class="tuning-bottom__property-title">Установка кастомной прошивки, увеличивающей скорость и мощность самоката</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Перенос подножки самоката на переднюю часть (опция). Актуальная только для версии PRO/PRO2</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Резинки на ручку тормоза и подножку</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Наклейки fast-motion ;)</div>
                                    </div>
                                </div>
                                <div class="tuning-variants-header__buy">
                                    <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-slider-button-prev swiper-button-prev js-tuning-slider-button-prev">
                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/chevron-left.svg" alt="">
                        </div>
                        <div class="tuning-slider-button-next swiper-button-next js-tuning-slider-button-next">
                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/chevron-right.svg" alt="">
                        </div>
                    </div>
                    <div class="tuning-bottom-mobile__variants js-tuning-slider js-tuning-container" data-tuning="12">
                        <div class="swiper-wrapper">
                            <div class="tuning-bottom-mobile__variant swiper-slide">
                                <div class="tuning-bottom-mobile__variant-header js-tuning-slider-header">
                                    <div class="tuning-variants-header__name tuning-bottom-mobile__variant-name">Комплекс</div>
                                    <div class="tuning-variants-header__price tuning-bottom-mobile__variant-price">9 000₽</div>
                                </div>
                                <div class="tuning-bottom-mobile__properties">
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников мотора (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников переднего колеса  (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Установка поддержки заднего крыла</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Резинки на ручку тормоза и подножку</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Наклейки fast-motion ;)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Гидроизоляция деки и мотора</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Настройка и регулировка всех компонентов и узлов самоката. (Тормаза и узел складывания )</div>
                                    </div>
                                </div>
                                <div class="tuning-variants-header__buy">
                                    <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                </div>
                            </div>
                            <div class="tuning-bottom-mobile__variant swiper-slide">
                                <div class="tuning-bottom-mobile__variant-header js-tuning-slider-header">
                                    <div class="tuning-variants-header__name tuning-bottom-mobile__variant-name">Гидроизоляция+</div>
                                    <div class="tuning-variants-header__price tuning-bottom-mobile__variant-price">6 000₽</div>
                                </div>
                                <div class="tuning-bottom-mobile__properties">
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников мотора (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников переднего колеса  (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Установка поддержки заднего крыла</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Резинки на ручку тормоза и подножку</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Наклейки fast-motion ;)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Гидроизоляция деки и мотора</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Настройка и регулировка всех компонентов и узлов самоката. (Тормаза и узел складывания )</div>
                                    </div>
                                </div>
                                <div class="tuning-variants-header__buy">
                                    <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                </div>
                            </div>
                            <div class="tuning-bottom-mobile__variant swiper-slide">
                                <div class="tuning-bottom-mobile__variant-header js-tuning-slider-header">
                                    <div class="tuning-variants-header__name tuning-bottom-mobile__variant-name">Начальная подготвока</div>
                                    <div class="tuning-variants-header__price tuning-bottom-mobile__variant-price">1 500₽</div>
                                </div>
                                <div class="tuning-bottom-mobile__properties">
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников мотора (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена подшипников переднего колеса  (NTN, NSK с индексом 2rs)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Установка поддержки заднего крыла</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Резинки на ручку тормоза и подножку</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Наклейки fast-motion ;)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Гидроизоляция деки и мотора</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Настройка и регулировка всех компонентов и узлов самоката. (Тормаза и узел складывания )</div>
                                    </div>
                                </div>
                                <div class="tuning-variants-header__buy">
                                    <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-slider-button-prev swiper-button-prev js-tuning-slider-button-prev">
                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/chevron-left.svg" alt="">
                        </div>
                        <div class="tuning-slider-button-next swiper-button-next js-tuning-slider-button-next">
                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/chevron-right.svg" alt="">
                        </div>
                    </div>
                    <div class="tuning-bottom-mobile__variants js-tuning-slider js-tuning-container" data-tuning="8">
                        <div class="swiper-wrapper">
                            <div class="tuning-bottom-mobile__variant swiper-slide">
                                <div class="tuning-bottom-mobile__variant-header js-tuning-slider-header">
                                    <div class="tuning-variants-header__name tuning-bottom-mobile__variant-name">ES1</div>
                                    <div class="tuning-variants-header__price tuning-bottom-mobile__variant-price">6 500₽</div>
                                </div>
                                <div class="tuning-bottom-mobile__properties">
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Установка 8-ми гранной рулевой втулки</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Доработка узла складывания (замена втулок)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title"> Доработка заднего амортизатора</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/close.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена втулок заднего амортизатора</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена втулок заднего колеса</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Гидроизоляция разъемов подсветки</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Установка демпферов узла складывания</div>
                                    </div>
                                </div>
                                <div class="tuning-variants-header__buy">
                                    <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-slider-button-prev swiper-button-prev js-tuning-slider-button-prev">
                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/chevron-left.svg" alt="">
                        </div>
                        <div class="tuning-slider-button-next swiper-button-next js-tuning-slider-button-next">
                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/chevron-right.svg" alt="">
                        </div>
                    </div>
                    <div class="tuning-bottom-mobile__variants js-tuning-slider js-tuning-container" data-tuning="11">
                        <div class="swiper-wrapper">
                            <div class="tuning-bottom-mobile__variant swiper-slide">
                                <div class="tuning-bottom-mobile__variant-header js-tuning-slider-header">
                                    <div class="tuning-variants-header__name tuning-bottom-mobile__variant-name"> ES2/4</div>
                                    <div class="tuning-variants-header__price tuning-bottom-mobile__variant-price">8 500₽</div>
                                </div>
                                <div class="tuning-bottom-mobile__properties">
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Установка 8-ми гранной рулевой втулки</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Доработка узла складывания (замена втулок)</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title"> Доработка заднего амортизатора</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена втулок заднего амортизатора</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Замена втулок заднего колеса</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Гидроизоляция разъемов подсветки</div>
                                    </div>
                                    <div class="tuning-bottom__property tuning-bottom-mobile__property">
                                        <div class="tuning-bottom__property-value tuning-bottom-mobile__property-value">
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/check.svg" alt="">
                                        </div>
                                        <div class="tuning-bottom__property-title">Установка демпферов узла складывания</div>
                                    </div>
                                </div>
                                <div class="tuning-variants-header__buy">
                                    <a class="btn-variant" href="#" data-toggle="modal" data-target="#exampleModal3">Выбрать</a>
                                </div>
                            </div>
                        </div>
                        <div class="tuning-slider-button-prev swiper-button-prev js-tuning-slider-button-prev">
                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/chevron-left.svg" alt="">
                        </div>
                        <div class="tuning-slider-button-next swiper-button-next js-tuning-slider-button-next">
                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/chevron-right.svg" alt="">
                        </div>
                    </div>
                </div>
                <div class="tuning-top js-tuning-fixed-top tuning-fixed-top d-md-none">
                    <div class="tuning-bottom-mobile__variant-header js-tuning-slider-header-fixed">
                        <div class="tuning-variants-header__name tuning-bottom-mobile__variant-name">Комплекс</div>
                        <div class="tuning-variants-header__price tuning-bottom-mobile__variant-price">9 000₽</div>
                    </div>
                    <div class="tuning-slider-button-prev swiper-button-prev js-tuning-slider-button-prev-double">
                        <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/chevron-left.svg" alt="">
                    </div>
                    <div class="tuning-slider-button-next swiper-button-next js-tuning-slider-button-next-double">
                        <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/chevron-right.svg" alt="">
                    </div>
                    <div class="tuning-header">
                        <div class="row">
                            <div class="col-xxs-12 col-6 col-md-5 col-lg-12">
                                <div class="tuning-header__model js-tuning-container is-active" data-tuning="6">
                                    <div class="row">
                                        <div class="d-none d-sm-block col-4">
                                            <div class="tuning-header__img">
                                                <img src="/images/tarifs/274a0f0d1e4e5da13eae7c5ede7871e6.png" alt="">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-8">
                                            <div class="tuning-header__body-container">
                                                <div class="tuning-header__body">
                                                    <div class="tuning-header__subtitle">Выбранная модель</div>
                                                    <div class="tuning-header__title">Xiaomi M365</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tuning-header__model js-tuning-container" data-tuning="7">
                                    <div class="row">
                                        <div class="d-none d-sm-block col-4">
                                            <div class="tuning-header__img">
                                                <img src="/images/tarifs/55a0d6aee4e719301815a12c9043aa1a.png" alt="">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-8">
                                            <div class="tuning-header__body-container">
                                                <div class="tuning-header__body">
                                                    <div class="tuning-header__subtitle">Выбранная модель</div>
                                                    <div class="tuning-header__title">1S</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tuning-header__model js-tuning-container" data-tuning="9">
                                    <div class="row">
                                        <div class="d-none d-sm-block col-4">
                                            <div class="tuning-header__img">
                                                <img src="/images/tarifs/e1ace6f9669cd3a4c6c5425fb8974e51.png" alt="">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-8">
                                            <div class="tuning-header__body-container">
                                                <div class="tuning-header__body">
                                                    <div class="tuning-header__subtitle">Выбранная модель</div>
                                                    <div class="tuning-header__title">Xiaomi PRO</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tuning-header__model js-tuning-container" data-tuning="10">
                                    <div class="row">
                                        <div class="d-none d-sm-block col-4">
                                            <div class="tuning-header__img">
                                                <img src="/images/tarifs/e1ace6f9669cd3a4c6c5425fb8974e51.png" alt="">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-8">
                                            <div class="tuning-header__body-container">
                                                <div class="tuning-header__body">
                                                    <div class="tuning-header__subtitle">Выбранная модель</div>
                                                    <div class="tuning-header__title">Xiaomi PRO 2</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tuning-header__model js-tuning-container" data-tuning="12">
                                    <div class="row">
                                        <div class="d-none d-sm-block col-4">
                                            <div class="tuning-header__img">
                                                <img src="/images/tarifs/e1ace6f9669cd3a4c6c5425fb8974e51.png" alt="">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-8">
                                            <div class="tuning-header__body-container">
                                                <div class="tuning-header__body">
                                                    <div class="tuning-header__subtitle">Выбранная модель</div>
                                                    <div class="tuning-header__title">Ninebot MAX G30/LP</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tuning-header__model js-tuning-container" data-tuning="8">
                                    <div class="row">
                                        <div class="d-none d-sm-block col-4">
                                            <div class="tuning-header__img">
                                                <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/none.png" alt="">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-8">
                                            <div class="tuning-header__body-container">
                                                <div class="tuning-header__body">
                                                    <div class="tuning-header__subtitle">Выбранная модель</div>
                                                    <div class="tuning-header__title">Ninebot ES1</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tuning-header__model js-tuning-container" data-tuning="11">
                                    <div class="row">
                                        <div class="d-none d-sm-block col-4">
                                            <div class="tuning-header__img">
                                                <img src="/images/tarifs/e1ace6f9669cd3a4c6c5425fb8974e51.png" alt="">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-8">
                                            <div class="tuning-header__body-container">
                                                <div class="tuning-header__body">
                                                    <div class="tuning-header__subtitle">Выбранная модель</div>
                                                    <div class="tuning-header__title">Ninebot ES2/4</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxs-12 col-6 col-md-7 col-lg-12">
                                <div class="tuning-header__selector-container">
                                    <div class="tuning-header__selector tuning-selector js-tuning-selector">
                                        <a class="btn-selector js-tuning-selector-btn" href="#">
                                            <span class="d-none d-md-inline">Выбрать другую модель</span>
                                            <span class="d-md-none">Изменить</span>
                                            <img src="https://fast-motion.ru/local/templates/motion_new/components/bitrix/news.list/tuning/images/chevron-down.svg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<?/*
<h2 class="col-lg-9 title mb-4">Комплексная модернизация и тюнинг электросамокатов</h2>
<div class="accordion" id="accordionExample">
	<div class="card">
		<div class="card-header" id="headingOne">
			<div class="spec-left">
 <img src="/bitrix/templates/motion/images/101.jpg" alt="">
				<div class="spec-block d-flex flex-column" style="width: 100%;">
 <button class="btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">Xiaomi M365, 1S, M365 PRO, PRO2</button>
				</div>
			</div>
		</div>
		<div class="collapse grey" id="collapseOne" aria-labelledby="headingOne" data-parent="#accordionExample">
			<div class="row d-flex">
				<div class="col-lg-4 mt-5">
					<div class="tariff-block">
						<div class="tariff-block__head">
							<div class="tariff-block__pre">
								 Для хорошей погоды
							</div>
							<div class="tariff-block__name">
								 «Начальный»
							</div>
						</div>
						<ul class="tariff-block__main">
							<li>Установка поддержки заднего крыла с защитой провода заднего габарита</li>
							<li>Настройка и регулировка всех компонентов и узлов самоката</li>
							<li>Установка кастомного ПО&nbsp;</li>
						</ul>
						<div class="tariff-block__footer">
							<div class="tariff-block__price">
								 2 500 ₽
							</div>
 <a class="btn " href="#" data-toggle="modal" data-target="#exampleModal3">выбрать</a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 mt-5">
					<div class="tariff-block">
						<div class="tariff-block__head">
							<div class="tariff-block__pre">
								 До 5 000 км&nbsp;
							</div>
							<div class="tariff-block__name">
								 «Полный »
							</div>
						</div>
						<ul class="tariff-block__main">
							<li>Герметизация мотора, дэки и кнопки включения</li>
							<li>Замена подшипников мотора на более качественные</li>
							<li>Установка поддержки заднего крыла с защитой провода заднего габарита</li>
							<li>Замена винтов крышки дэки на потайные</li>
							<li>Настройка и регулировка всех компонентов и узлов самоката</li>
							<li>Установка кастомного ПО </li>
							<li>Модернизация крепления батареи (для простой версии М365)</li>
							<li>Усиление контроллера (для PRO версии)</li>
							<li>Замена задних подшипников </li>
						</ul>
						<div class="tariff-block__footer">
							<div class="tariff-block__price">
								 8 000 ₽ (9 000 ₽ PRO)
							</div>
 <a class="btn " href="#" data-toggle="modal" data-target="#exampleModal3">выбрать</a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 mt-5">
					<div class="tariff-block">
						<div class="tariff-block__head">
							<div class="tariff-block__pre">
								 Комплексный тюнинг
							</div>
							<div class="tariff-block__name">
								 «Дополнительно»
							</div>
						</div>
						<ul class="tariff-block__main">
							<li>Замена покрышек на 10 дюймов (+4500р а составе комплекса 6000 отдельно, входят покрышки, комплект проставок, брызговики)</li>
						
							<li>Перенос подножки самоката на переднюю часть (бесплатно при комплексных работах)</li>
						</ul>
						<div class="tariff-block__footer">
							<div class="tariff-block__price">
								 4 500 ₽
							</div>
 <a class="btn " href="#" data-toggle="modal" data-target="#exampleModal3">выбрать</a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 mt-5">
					<div class="tariff-block">
						<div class="tariff-block__head">
							<div class="tariff-block__name">
								 Покраска
							</div>
						</div>
						<ul class="tariff-block__main tariff-block__main2">
							<li>Сделаем ваш самокат уникальным, покрасив в любой цвет </li>
						</ul>
						<div class="tariff-block__footer">
							<div class="tariff-block__price">
								 от 10 000₽
							</div>
 <a class="btn " href="#" data-toggle="modal" data-target="#exampleModal3">выбрать</a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 mt-5">
					<div class="tariff-block">
						<div class="tariff-block__head">
							<div class="tariff-block__name">
								 Обновление ПО
							</div>
						</div>
						<ul class="tariff-block__main tariff-block__main2">
							<li>Уникальная прошивка по пожеланиям заказчика, увеличивающая скорость и мощность электросамоката</li>
						</ul>
						<div class="tariff-block__footer">
							<div class="tariff-block__price">
								 Бесплатно
							</div>
 <a class="btn " href="#" data-toggle="modal" data-target="#exampleModal3">выбрать</a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 mt-5">
					<div class="tariff-block">
						<div class="tariff-block__head">
							<div class="tariff-block__name">
								 «С ребенком»
							</div>
						</div>
						<ul class="tariff-block__main tariff-block__main2">
							<li>Установка подножки и руля для ребенка на взрослый самокат для безопасности поездок вдвоем на одном самокате</li>
						</ul>
						<div class="tariff-block__footer">
							<div class="tariff-block__price">
								 2 000₽
							</div>
 <a class="btn " href="#" data-toggle="modal" data-target="#exampleModal3">выбрать</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingTwo">
			<div class="spec-left">
 <img src="/bitrix/templates/motion/images/104.jpg" alt="">
				<div class="spec-block d-flex flex-column" style="width: 100%;">
 <button class="btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Ninebot ES + Ninebot MAX</button>
				</div>
			</div>
		</div>
		<div class="collapse grey" id="collapseTwo" aria-labelledby="headingTwo" data-parent="#accordionExample">
			<div class="row d-flex">
				<div class="col-lg-4 mt-5">
					<div class="tariff-block">
						<div class="tariff-block__head">
							<div class="tariff-block__pre">
								 Для любой погоды
							</div>
							<div class="tariff-block__name">
								 «ninebot MAX»
							</div>
						</div>
						<ul class="tariff-block__main">
							<li>Замена подшипников мотора</li>
							<li>Замена подшипников на переднем колесе</li>
							<li>Установка поддержки заднего крыла</li>
							<li>Дополнительная гидроизоляция дэки</li>
							<li>Настройка переднего тормоза</li>
							<li>Настройка узла складывания</li>
							<li>Установка кастомной прошивки (по желанию)</li>
						</ul>
						<div class="tariff-block__footer">
							<div class="tariff-block__price">
								 8 500 ₽
							</div>
 <a class="btn " href="#" data-toggle="modal" data-target="#exampleModal3">выбрать</a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 mt-5">
					<div class="tariff-block">
						<div class="tariff-block__head">
							<div class="tariff-block__pre">
								 Модернизация &nbsp;
							</div>
							<div class="tariff-block__name">
								 «Механика ES»
							</div>
						</div>
						<ul class="tariff-block__main">
							<li>Установка 8-ми гранной рулевой втулки</li>
							<li>Доработка узла складывания</li>
							<li>Доработка заднего амортизатора</li>
							<li>Установка втулки заднего амортизатора</li>
							<li>Обслуживание всех узлов, доработка разъемов и ленточки </li>
							<li>Установка кастомного ПО </li>
						</ul>
						<div class="tariff-block__footer">
							<div class="tariff-block__price">
								 8 500 ₽
							</div>
 <a class="btn " href="#" data-toggle="modal" data-target="#exampleModal3">выбрать</a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 mt-5">
					<div class="tariff-block">
						<div class="tariff-block__head">
							<div class="tariff-block__pre">
								 Комплексный тюнинг
							</div>
							<div class="tariff-block__name">
								 «Дополнительно ES»
							</div>
						</div>
						<ul class="tariff-block__main">
							<li>Замена подшипников мотор-колеса </li>
							<li>Замена силовых разъемов мотор-колеса (МТ60) ES</li>
							<li></li>
							<li></li>
						</ul>
						<div class="tariff-block__footer">
							<div class="tariff-block__price">
								 3 500 ₽
							</div>
 <a class="btn " href="#" data-toggle="modal" data-target="#exampleModal3">выбрать</a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 mt-5">
					<div class="tariff-block">
						<div class="tariff-block__head">
							<div class="tariff-block__name">
								 Покраска
							</div>
						</div>
						<ul class="tariff-block__main tariff-block__main2">
							<li>Сделаем ваш самокат уникальным, покрасив в любой цвет </li>
						</ul>
						<div class="tariff-block__footer">
							<div class="tariff-block__price">
								 от 10 000₽
							</div>
 <a class="btn " href="#" data-toggle="modal" data-target="#exampleModal3">выбрать</a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 mt-5">
					<div class="tariff-block">
						<div class="tariff-block__head">
							<div class="tariff-block__name">
								 Обновление ПО
							</div>
						</div>
						<ul class="tariff-block__main tariff-block__main2">
							<li>Уникальная прошивка по пожеланиям заказчика, увеличивающая скорость и мощность электросамоката</li>
						</ul>
						<div class="tariff-block__footer">
							<div class="tariff-block__price">
								 Бесплатно
							</div>
 <a class="btn " href="#" data-toggle="modal" data-target="#exampleModal3">выбрать</a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 mt-5">
					<div class="tariff-block">
						<div class="tariff-block__head">
							<div class="tariff-block__name">
								 «С ребенком»
							</div>
						</div>
						<ul class="tariff-block__main tariff-block__main2">
							<li>Установка подножки и руля для ребенка на взрослый самокат для безопасности поездок вдвоем на одном самокате</li>
						</ul>
						<div class="tariff-block__footer">
							<div class="tariff-block__price">
								 2 000 ₽
							</div>
 <a class="btn " href="#" data-toggle="modal" data-target="#exampleModal3">выбрать</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 <br>
*/?>