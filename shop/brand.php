<?php
$APPLICATION->AddChainItem($APPLICATION->GetTitle());

?>
    <section class="head-section">
        <div class="container">
            <div class="row">

                <div class="col-lg-12">
                    <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "bread", Array(
                        "PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
                        "SITE_ID" => "s1",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
                        "START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
                    ),
                        false
                    );?>
                </div>
                <div class="col-lg-12">
                    <h1 class="head-section__title"><?$APPLICATION->ShowTitle(false)?></h1><a class="btn" href="#">БЕСПЛАТНАЯ КОНСУЛЬТАЦИЯ</a>
                </div>
            </div>
        </div>
    </section>
    <section class="gray pt-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="filter d-flex justify-content-between">


						<div class="row brands" style="width: 100%;">
<?


	CModule::IncludeModule('iblock');
	$brands = CIBlockElement::GetList([], ['IBLOCK_ID' => 6]);

while($brand = $brands->GetNext()){
	?>


<div class="col-md-2">
<a href="<?=$brand["DETAIL_PAGE_URL"]?>" class="brands__item <? if(isset($_REQUEST["SECTION_CODE"]) && $brand["CODE"] == $_REQUEST["SECTION_CODE"]) echo 'active' ?>">
	<img src="<?=CFile::GetPath($brand["PREVIEW_PICTURE"])?>">
	<span><?=$brand["NAME"]?></span>
	</a>
	</div>

<?
}
global $arFilter;
if(isset($_REQUEST["SECTION_CODE"])){
	$brand = CIBlockElement::GetList([], ['IBLOCK_ID' => 6, 'CODE' => $_REQUEST["SECTION_CODE"]]);
	if($currentBrand = $brand->GetNext()){
		$arFilter['PROPERTY_BRAND'] = $currentBrand["ID"];
	}
}

?>
							</div>
                            <!--<div class="model-block">
                                <select class="filter__model">
                                    <option class="mijia">Xiaomi Mijia</option>
                                    <option class="mijia2">Ninebot</option>
                                    <option class="mijia3">Kugoo</option>
                                    <option class="mijia4">Halten</option>
                                </select>
                            </div>
                            <div class="filter-block">
                                <select class="filter__select">
                                    <option>Фильтр</option>
                                    <option>По цене</option>
                                    <option>По названию</option>
                                </select>
                            </div>-->
                        </div>
<!--                        <div class="filter__right d-flex">-->
<!--                            <button type=""><img src="./images/fire.svg"><span>Хиты продаж</span></button>-->
<!--                            <select class="filter__select">-->
<!--                                <option>По популярности</option>-->
<!--                                <option>По популярности</option>-->
<!--                                <option>По популярности</option>-->
<!--                                <option>По популярности</option>-->
<!--                            </select>-->
<!--                        </div>-->

                </div>
            </div>
            <?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"goods", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "N",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "arFilter",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "content",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "PRICE",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "goods",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>


<? if(isset($currentBrand)){ ?>
<?
    /*$APPLICATION->IncludeComponent(
	"bitrix:news.detail", 
	"brand", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_ELEMENT_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_CODE" => $_REQUEST["BRAND_CODE"],
		"ELEMENT_ID" => "",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"IBLOCK_ID" => "6",
		"IBLOCK_TYPE" => "content",
		"IBLOCK_URL" => "",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Страница",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "Y",
		"SET_CANONICAL_URL" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"STRICT_SECTION_CHECK" => "N",
		"USE_PERMISSIONS" => "N",
		"USE_SHARE" => "N",
		"COMPONENT_TEMPLATE" => "brand"
	),
	false
);*/
    ?>
			<? } ?>

        </div>
    </section>
    <section class="white-section">
        <div class="container">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => "/includes/blog.php"
                )
            );?>
        </div>
    </section>
