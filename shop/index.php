<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Магазин электросамокатов с каталогом по брендам и моделям");
$APPLICATION->SetPageProperty("description", "Каталог взрослых электросамокатов на любой вкус и цвет, представлены все ведущие бренды: Xiaomi, Kogoo, Ninebot и многие другие. Хотите выбрать электросамокат в проверенном месте с гарантией? Обращайтесь в FastMotion - мастерскую и магазин №1 в Москве!");
$APPLICATION->SetTitle("Магазин электросамокатов");
CModule::IncludeModule('iblock');
?>

<?

if (isset($_REQUEST["SECTION_CODE"])) {


    $result = CIBlockElement::GetList([], ['IBLOCK_ID' => 6, 'CODE' => $_REQUEST['SECTION_CODE']]);



    if ($item = $result->GetNext()) {
        echo 'brand-1';

        $APPLICATION->SetPageProperty("title", "Каталог электросамокатов бренда " . $item["NAME"]);
        $APPLICATION->SetPageProperty("description", "Линейка электросамокатов бренда " . $item["NAME"] . " в интернет-магазине ведущей мастерской Москвы FastMotion в интернет-магазине. Хотите подобрать электросамокат " . $item["NAME"] . " по душе? Заходите сюда!");
        $APPLICATION->SetTitle('Электросамокаты ' . $item["NAME"]);

        include_once('brand.php');


    } else {

        $result = CIBlockElement::GetList([], ['IBLOCK_ID' => 4, 'CODE' => $_REQUEST['SECTION_CODE']]);

        if ($item = $result->GetNext()) {
            echo 'section';
            include_once('good.php');

        } else {
            $APPLICATION->SetTitle('Страница не найдена');
            Bitrix\Iblock\Component\Tools::process404(null, true, true, true);


        }


    }


} else {
    echo 'custom';
    include_once('brand.php');
}
?>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>