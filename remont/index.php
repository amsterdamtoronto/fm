<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Ремонт электросамокатов в Москве в мастерской FastMotion");
$APPLICATION->SetPageProperty("description", "Профессиональный ремонт электросамокатов в Москве по выгодным ценам, передовой сервисный центр FastMotion. Всегда в наличии запчасти и правдивые цены за работу комплектующие. Быстро поможем с ремонтом и сделаем тюнинг Вашего электросамоката. Обращайтесь!");
$APPLICATION->SetTitle("Ремонт электросамокатов");
?><section class="head-section">
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"bread",
	Array(
		"PATH" => "",
		"SITE_ID" => "s1",
		"START_FROM" => "0"
	)
);?>
		</div>
		<div class="col-lg-12">
			<h1 class="head-section__title">
			<? $APPLICATION->ShowTitle(false) ?> </h1>
 <a class="btn" href="#">БЕСПЛАТНАЯ КОНСУЛЬТАЦИЯ</a>
		</div>
	</div>
</div>
 </section> <section class="advantages pt-0">
<div class="container">
	<div class="row d-flex align-items-center mt-5">
		<div class="col-lg-12 title mt-5">
			<div class="title">
				 Стоимость ремонта
			</div>
			 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"remont",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "remont",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"DETAIL_PICTURE",1=>"",),
		"FILTER_NAME" => "arFilter",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "content",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"SUBTITLE",1=>"",),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>
		</div>
		<div class="row mt-5">
			<div class="col-lg-3 mb-4">
				<div class="problem-block d-flex flex-column justify-content-between">
					<div class="problem-block__item">
						<div class="problem-block__pre">
							 Диагностика
						</div>
						<div class="problem-block__name">
							 Электросамокат не заряжается
						</div>
					</div>
 <a class="elipse-btn" href="#" data-toggle="modal" data-target="#exampleModal"><img src="/bitrix/templates/motion/images/reda.svg"></a>
				</div>
			</div>
			<div class="col-lg-3 mb-4">
				<div class="problem-block d-flex flex-column justify-content-between">
					<div class="problem-block__item">
						<div class="problem-block__pre">
							 Диагностика
						</div>
						<div class="problem-block__name">
							 Сломался механизм складывания
						</div>
					</div>
 <a class="elipse-btn" href="#" data-toggle="modal" data-target="#exampleModal"><img src="/bitrix/templates/motion/images/reda.svg"></a>
				</div>
			</div>
			<div class="col-lg-3 mb-4">
				<div class="problem-block d-flex flex-column justify-content-between">
					<div class="problem-block__item">
						<div class="problem-block__pre">
							 Диагностика
						</div>
						<div class="problem-block__name">
							 Спустило колесо
						</div>
					</div>
 <a class="elipse-btn" href="#" data-toggle="modal" data-target="#exampleModal"><img src="/bitrix/templates/motion/images/reda.svg"></a>
				</div>
			</div>
			<div class="col-lg-3 mb-4">
				<div class="problem-block d-flex flex-column justify-content-between">
					<div class="problem-block__item">
						<div class="problem-block__pre">
							 Диагностика
						</div>
						<div class="problem-block__name">
							 Самокат залит
						</div>
					</div>
 <a class="elipse-btn" href="#" data-toggle="modal" data-target="#exampleModal"><img src="/bitrix/templates/motion/images/reda.svg"></a>
				</div>
			</div>
		</div>
	</div>
</div>
 </section> <section class="white-section">
<div class="container">
	<div class="row">
		<h2 class="col-lg-12 title">
		Наш ремонт электросамокатов Xiaomi Mijia M365 и PRO </h2>
	</div>
	<div class="row about-images">
		<div class="col-lg-3 mb-4">
 <img src="/bitrix/templates/motion/images/09.jpg">
		</div>
		<div class="col-lg-3 mb-4">
 <img src="/bitrix/templates/motion/images/10.jpg">
		</div>
		<div class="col-lg-3 mb-4">
 <img src="/bitrix/templates/motion/images/11.jpg">
		</div>
		<div class="col-lg-3 mb-4">
 <img src="/bitrix/templates/motion/images/12.jpg">
		</div>
	</div>
	 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/includes/how-we-work.php"
	)
);?>
</div>
 </section>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/includes/testi.php"
	)
);?> <section class="white-section">
<div class="container">
	 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/includes/blog.php"
	)
);?>
</div>
<div class="container">
	<div class="row about-text">
		<div class="col-lg-6 pr-5">
			<h2 class="title pb-3">Профессиональный ремонт электросамокатов в Fast motion</h2>
			<p>
				 Специализированный сервис Fast motion оказывает полный перечень услуг по ремонту, обслуживанию и модернизации электросамокатов Xiaomi и Kugoo в Москве. Мастерская проводит профессиональную диагностику, учитывая инструкции и рекомендации от производителей. На все виды работ предоставляется гарантия сроком на 1 год. Мы уверены в качестве предоставляемых услуг и обеспечиваем клиентам высокий уровень сервиса.
			</p>
			<p>
				 Опытные мастера осуществляют гарантийный и послегарантийный ремонт. В сервисном центре Fast motion вы можете заказать комплектующие, зарядки и другие аксессуары для электросамоката. Мы поставляем только оригинальные сертифицированные детали. Они соответствуют строгим стандартам, отличаются долговечностью и исключительно высоким качеством.
			</p>
			<h2 class="title pb-3">Особенности работ по ремонту и обслуживанию</h2>
			<p>
				 В мастерской Fast motion проводится ремонт, обслуживание и модернизация электросамокатов любой сложности. Для этого у специалистов есть все необходимое – соответствующая квалификация, опыт, диагностическое оборудование и полный перечень оригинальных комплектующих.
			</p>
			<p>
				 При необходимости вы сможете вызвать мастера с выездом на дом, чтобы восстановить работу электросамоката. Специалист прибудет по указанному в заявке адресу в необходимое клиенту время с полным арсеналом инструментов. На сайте Fast motion также возможно найти ближайший сервисный центр на карте, чтобы посетить профессиональную мастерскую самостоятельно. Мастерская предоставляет услуги для таких округов Москвы:
			</p>
			<ul>
				<li>вао;</li>
				<li>юваю;</li>
				<li>юао;</li>
				<li>юзао;</li>
				<li>зао;</li>
				<li>сзао;</li>
				<li>сао;</li>
				<li>свао.</li>
			</ul>
			<p>
				 Настоятельно не рекомендуется проводить диагностику и ремонт своими руками. В противном случае вы рискуете столкнуться с еще большими проблемами в работе электросамоката и необратимыми изменениями в его механизме. Лучше всего посетить сервисный центр или вызвать мастера на дом при выявлении любых проблем с транспортным средством. Своевременное обслуживание и мелкий ремонт позволят избежать глобальных дефектов и поломок.
			</p>
		</div>
		<div class="col-lg-6">
			<h2 class="title pb-3">Какие работы проводятся в мастерской Fast motion?</h2>
			<p>
				 Мастерская Fast motion предоставляет весь комплекс услуг по ремонту и модернизации электросамокатов. Все работы можно разделить на такие группы:
			</p>
			<ul>
				<li>&gt;электрика;</li>
				<li>&gt;колеса и покрышки;</li>
				<li>&gt;общие работы;</li>
				<li>&gt;комплексная подготовка.</li>
			</ul>
			<p>
				 Убедиться в эффективности работы мастеров вы можете, ознакомившись с отзывами клиентов.
			</p>
			<p>
				 Особой популярностью пользуются такие услуги:
			</p>
			<ul>
				<li>прошивка ПО;</li>
				<li>замена насоса, подшипников и аккумулятора;</li>
				<li>переустановка колес;</li>
				<li>установка нового сиденья;</li>
				<li>ремонт контроллера и батареи;</li>
				<li>ремонт заднего колеса;</li>
				<li> установка литой покрышки;</li>
				<li>шиномонтажные работы;</li>
				<li>замена важнейших узлов системы</li>
				<li>регулировка узла складывания и тормоза;</li>
				<li>гидроизоляционные работы;</li>
				<li>замена покрышек;</li>
				<li>обновление ПО.</li>
			</ul>
			<p>
				 В мастерской Fast motion вы также можете приобрести зарядное устройство и другие аксессуары оригинального качества. Ознакомьтесь с актуальными ценами на сайте и заказывайте профессиональный ремонт, чтобы не рисковать своим электросамокатом.
			</p>
		</div>
	</div>
</div>
 </section>
<div class="container">
	<div class="row d-flex justify-content-between remont-services">
		<div class="col-lg-5">
			<div class="advantages__left">
 <img src="/bitrix/templates/motion/images/alogo.svg" class="logo">
				<h2 class="advantages__title">
				Сервисное обслуживание и ремонт любой сложности</h2>
				<div class="advantages__desc">
					 Специализированный сервис Xiaomi,&nbsp;Kugoo : модернизация, тюнинг, ремонт, запчасти, покупка и продажа по низким ценам в Москве
				</div>
			</div>
		</div>
		<div class="col-lg-7 d-flex flex-wrap justify-content-between">
			<div class="advantages__block d-flex flex-column">
 <img src="/bitrix/templates/motion/images/01.svg">
				<div class="advantages__t">
					 Cервисные центры
				</div>
				<div class="advantages__d">
					 метро Университет в Москве
				</div>
			</div>
			<div class="advantages__block d-flex flex-column">
 <img src="/bitrix/templates/motion/images/02.svg">
				<div class="advantages__t">
					 Запчасти
				</div>
				<div class="advantages__d">
					 Свой склад запчастей
				</div>
			</div>
			<div class="advantages__block d-flex flex-column">
 <img src="/bitrix/templates/motion/images/03.svg">
				<div class="advantages__t">
					 Гарантия 1 год
				</div>
				<div class="advantages__d">
					 На новые самокаты с нашей подготовкой и ремонт
				</div>
			</div>
			<div class="advantages__block d-flex flex-column">
 <img src="/bitrix/templates/motion/images/04.svg">
				<div class="advantages__t">
					 Выездной ремонт
				</div>
				<div class="advantages__d">
					 Мастера cделают все работу у вас дома или в офисе
				</div>
			</div>
		</div>
	</div>
</div><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>