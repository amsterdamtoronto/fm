<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "курс по ремонту Xiaomi");
$APPLICATION->SetTitle("Ремонт персонального электротранспорта на базе Xiaomi от Артема Чурякова");
?><section class="head-section">
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"bread",
	Array(
		"PATH" => "",
		"SITE_ID" => "s1",
		"START_FROM" => "0"
	)
);?>
		</div>
		<div class="col-lg-12">
			<div class="head-section__title">
				 <?$APPLICATION->ShowTitle(false)?>
			</div>
		</div>
	</div>
</div>
 </section> <section class="advantages training-section">
<div class="container">
	<div class="row d-flex justify-content-between">
		<div class="col-md-4">
			<div class="advantages__left">
 <img src="/bitrix/templates/motion/images/alogo.svg" class="logo">
				<div class="advantages__title">
					 Обучение от Fastmotion
				</div>
				<div class="advantages__desc">
					 Вы узнаете что нужно учесть при создании мастерской по ремонту электротранспорта от технологии по ремонту до структуры доходов/расходов. Команда Fast-motion расскажет свой опыт за 2 года.
				</div>
 <a class="btn" href="#" data-toggle="modal" data-target="#exampleModal4">записаться на курс</a>
			</div>
		</div>
		<div class="col-md-8 d-flex flex-wrap justify-content-between">
			<div class="advantages__title">
				 Программа курса
			</div>
			<div class="accordion" id="accordionExample">
				<div class="card">
					<div class="card-header" id="headingOne">
						<h3 class="mb-0"><button class="btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">24 июля Старт 16-00 МСК (4 часа)</button> </h3>
					</div>
					<div class="collapse show" id="collapseOne" aria-labelledby="headingOne" data-parent="#accordionExample">
						<div class="card-body">
							<ul>
 <strong>Введение</strong>
								<li>общие сведения о персональном электротранспорте;</li>
								<li>современный электротранспорт, состояние индустрии малого электротранспорта на 2020 год.</li>
							</ul>
							<ul>
 <strong>Наши товары и услуги, как и что продаем.</strong>
								<li>Экономика ремонта малого электротранспорта;</li>
								<li>Прокат малого электротранспорта;</li>
								<li>Продажа запчастей и аксессуаров;</li>
								<li>Продажа электротранспорта.</li>
							</ul>
							<ul>
 <strong>Реклама и клиенты</strong>
							</ul>
							<ul>
 <strong>Введение</strong>
								<li>Места откуда о нас узнают;</li>
								<li>Работа с клиентом.</li>
							</ul>
							<ul>
 <strong>Персонал и локация</strong>
								<li>Мастера;</li>
								<li>Офисный состав;</li>
								<li>Локация.</li>
							</ul>
							<ul>
 <strong>Комплексные работы</strong>
								<li>Основные сведения о комплексных работах;</li>
								<li>Технологии быстрого ремонта.</li>
							</ul>
							<ul>
 <strong>Общение и ответы на вопросы</strong>
							</ul>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header" id="headingTwo">
						<h3 class="mb-0"> <button class="btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">25 июля Старт 16-00 МСК (4 часа)</button> </h3>
					</div>
					<div class="collapse" id="collapseTwo" aria-labelledby="headingTwo" data-parent="#accordionExample">
						<div class="card-body">
							<ul>
 <strong>Практика (для мастеров):</strong>
								<li>Демонстрация способов и технологий ремонта и технического обслуживания обслуживания на примере xiaomi;</li>
								<li>Ответы на вопросы и общение.</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<p>
 <strong>Дополнительно к онлайн курсу вы получите техническую поддержку от Артема и команды мастеров на 2&nbsp;месяца.</strong>
			</p>
			<p style="font-size: 22px; width: 100%;">
				 9 900₽
			</p>
			<form action="https://money.yandex.ru/eshop.xml" method="post">
 <input name="shopId" value="724469" type="hidden" required=""> <input name="scid" value="1843840" type="hidden" required=""> <input name="sum" value="9900" type="hidden"> Имя<br>
 <input required="" name="customerNumber" value="" size="64"><br>
 <br>
				 Телефон<br>
 <input name="custName" value="" size="64"><br>
 <br>
				 Почта<br>
 <input name="custEmail" value="" size="64"><br>
 <br>
 <input type="submit" value="Заплатить">
			</form>
 <span style="width: 100%; display: block;">Что вы получаете</span>
			<p>
			</p>
			<ul>
				<li>Участие в вебинаре и видеозапись </li>
				<li>Техническую поддержку от команды fast-motion </li>
			</ul>
			<p>
			</p>
			<p>
				 Онлайн курс проведут<br>
				  <br>
				 Александр Алыренков
			</p>
		</div>
	</div>
</div>
 </section>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/includes/testi.php"
	)
);?> <section class="white-section">
<div class="container">
	 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/includes/blog.php"
	)
);?>
</div>
 </section> <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>