<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "курс по ремонту Xiaomi");
$APPLICATION->SetTitle("Ремонт персонального электротранспорта на базе Xiaomi от Артема Чурякова");
$APPLICATION->SetAdditionalCSS("/local/templates/motion_new/css/remont.css",true);
?>

<div class="header-course">
    <img  class="header-course__bg-image1" src="/local/templates/motion_new/images/header-bg1.png"/>
    <img  class="header-course__bg-image1-mob" src="/local/templates/motion_new/images/header-bg1.png"/>
    <img class="header-course__bg-image2" src="/local/templates/motion_new/images/header-bg2.svg"/>
    <div class="container">
        <div class="header-course__top d-flex justify-content-between">
            <div class="header-course__top-left d-flex">
                <div class="header-course__logo">
                    <a href='/'>
                        <img src="/local/templates/motion_new/images/Logo.svg">
                    </a>
                </div>

            </div>
            <div class="header-course__top-right">
                <div class="follow-us d-flex">
                    <div class="follow-us__text">
                        Присоединяйтесь к нам
                    </div>
                    <ul class="social">
                        <li>
                            <a href="https://www.instagram.com/fastmotionelectric/">
                                <img class="lazy" data-original="/local/templates/motion_new/images/in.svg" src="/local/templates/motion_new/images/in.svg" style="display: inline;">
                            </a>
                        </li>
                        <li>
                            <a href="https://www.vk.com/fastmotionelectric/">
                                <img class="lazy" data-original="/local/templates/motion_new/images/vk.svg" src="/local/templates/motion_new/images/vk.svg" style="display: inline;">
                            </a>
                        </li>
                        <li>
                            <a href="https://t.me/FastMotion">
                                <img class="lazy" data-original="/local/templates/motion_new/images/tg.svg" src="/local/templates/motion_new/images/tg.svg" style="display: inline;">
                            </a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/c/fastmotionelectric">
                                <img class="lazy" data-original="/local/templates/motion_new/images/yt.svg" src="/local/templates/motion_new/images/yt.svg" style="display: inline;">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
        <div class="header-course__middle">
            <div class="header-course__promo">3-х дневный очный курс</div>
            <h2>По открытию мастерской и ремонту электросамокатов</h2>
            <div class="header-course__icons-wrapper d-flex">
                <div class="header-course__icon-wrapper">
                    <img src="/local/templates/motion_new/images/mijia2.png"/>
                </div>
                <div class="header-course__icon-wrapper">
                    <img src="/local/templates/motion_new/images/ninebot2.png"/>
                </div>
            </div>
        </div>
        <div class="header-course__bottom">
            <div class="d-flex justify-content-between">
                <ul class="header-course__bottom-list d-flex">
                    <li><span>Весь опыт команды Fast-Motion за 2,5 года </span></li>
                    <li><span>Без паушальных взносов и роялти </span></li>
                </ul>
                <div class="d-flex">
                    <div>
                        <a class="btn anchor-to-form" href="#form-apply "  >Записаться на курс</a>
                    </div>
                    <div class="program__place-item">
                        <div class="program__place-item-title">Ближайшая дата</div>
                        <div class="program__place-item-text">18-20 февраля</div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<main class="remont">
    <section class="about-us">
        <div class="container">
            <div class="image-large">
                <img src="/images/newbg_obuchenie.jpg">
            </div>
        </div>
    </section>
<section class="advantages">
    <div class="container">
        <div class="d-flex justify-content-between ">
            <div class="d-flex flex-wrap justify-content-between advantages__remont-list">
                <div class="advantages__block d-flex flex-column">
                    <img src="/local/templates/motion_new/images/team.svg">
                        <div class="advantages__t">
Собрали крутую команду специалистов
</div>
                </div>
                <div class="advantages__block d-flex flex-column">
           	        <img src="/bitrix/templates/motion/images/04.svg">
                        <div class="advantages__t">
Открыли 5 мастерских в Москве
</div>
                </div>
                <div class="advantages__block d-flex flex-column">
           	        <img src="/bitrix/templates/motion/images/01.svg">
                        <div class="advantages__t">
Отремонтировали более 5000 самокатов с 2018 года
</div>
           				</div>
           				<div class="advantages__block d-flex flex-column">
           	<img src="/local/templates/motion_new/images/education-insurance 1.svg">
           					<div class="advantages__t">
Делаем образовательный контент на youtube
</div>
           				</div>
           			</div>
           		</div>
           	</div>
           </section>
           <section class="section-youtubeButton">
                <div class="container">
           		    <div class="youtubeButton">
                        <div class="youtubeButton__left">
                            <div class="youtubeButton__icon">
                                <img src="/local/templates/motion_new/images/youtube_icon.png" alt="">
                            </div>
                            <div class="youtubeButton__text">
Мы на Youtube
</div>
                        </div>
                        <div class="youtubeButton__right">
                            <div class="youtubeButton__text--gray">
Технические обзоры, курсы, о самокатах
</div>
                            <a class="btn youtubeButton__link" target="_blank" href="https://web.archive.org/web/20220626050352/https://www.youtube.com/c/fastmotionelectric">Перейти на канал</a>
                        </div>
           			</div>
                </div>
           </section>
            <section class="forWho">
           	    <div class="container">
           		    <div class="forWho__inner d-flex justify-content-between">
           			    <div>
           				    <div class="advantages__left">
           					    <h2 class="advantages__title">
Кому подойдет программа
</h2>
           					    <div class="advantages__desc">
           					    </div>
           	                    <a class="btn  anchor-to-form" href="#form-apply">Записаться на курс</a>
           				    </div>
           			    </div>
           			    <div class="d-flex flex-wrap justify-content-between">
                            <ul class="forWho__list flex flex-column">
                                <li class="forWho__item">
                                    <div class="forWho__large-number">1</div>
                                    <div class="forWho__text">Для владельцев сервисных центров, которые хотят запустить новое направления</div>
                                </li>

                                <li class="forWho__item">
                                    <div class="forWho__large-number">2</div>
                                    <div class="forWho__text">Для мастеров которые хотят запустить новую мастерскую</div>
                                </li>
                                <li class="forWho__item">
                                    <div class="forWho__large-number">3</div>
                                    <div class="forWho__text">Для тех кто рассматривает покупку франшизы, но не хочет переплачивать </div>
                                </li>
                            </ul>
           		        </div>
           	        </div>
           	    </div>
           </section>
           <section class="program gray">
                <div class="container">
                    <div class=" d-flex justify-content-between program__top">
                        <h2 class="title">Программа курса</h2>
                        <div>
                            <div class="program__place d-flex">
                                <div class="program__place-item">
                                    <div class="program__place-item-title">Место проведения</div>
                                    <div class="program__place-item-text">Москва, Строителей 4с2</div>
                                </div>
                                <div class="vertical-line"></div>
                                <div class="program__place-item">
                                    <div class="program__place-item-title">Группы</div>
                                    <div class="program__place-item-text">до 7-8 человек</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="program__inner">
                        <div class="program__part d-flex">
                            <div class="program__part-left">
                                <h3 class="program__part-title">Ремонт</h3>
                                <div class="program__part-days">2 дня</div>
                            </div>
                            <div class="program__module d-flex  flex-column">
                                <div class="program__module-item d-flex">
                                    <div class="program__module-item-left">
                                        <div class="program__module-item-count">Модуль 1</div>
                                        <div class="program__module-item-title">Введение</div>
                                    </div>
                                    <div class="program__module-item-right">
                                        <ul class="program__module-item-list">
                                            <li><span>Общие сведения о персональном электротранспорте;</span></li>
                                            <li><span>Современный электротранспорт, состояние индустрии малого электротранспорта</span></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="program__module-item d-flex">
                                    <div class="program__module-item-left">
                                        <div class="program__module-item-count">Модуль 2</div>
                                        <div class="program__module-item-title">Теория электрика на примере Xiaomi и Ninebot </div>
                                    </div>
                                    <div class="program__module-item-right">
                                        <ul class="program__module-item-list">
                                            <li><span>Устройство самоката</span></li>
                                            <li><span>АКБ</span></li>
                                            <li><span>BMS</span></li>
                                            <li><span>Контроллер</span></li>
                                            <li><span>Мотор колесо</span></li>
                                            <li><span>Отдельные компоненты и их особенности</span></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="program__module-item d-flex">
                                    <div class="program__module-item-left">
                                        <div class="program__module-item-count">Модуль 3</div>
                                        <div class="program__module-item-title">Теория электрика на примере Xiaomi и Ninebot </div>
                                    </div>
                                    <div class="program__module-item-right">
                                        <ul class="program__module-item-list">
                                            <li><span>Подшипники</span></li>
                                            <li><span>Колёса и шиномонтаж ( типы, давления )</span></li>
                                            <li><span>Регулировки</span></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="program__module-item d-flex">
                                    <div class="program__module-item-left">
                                        <div class="program__module-item-count">Модуль 4</div>
                                        <div class="program__module-item-title">Практика механический ремонт</div>
                                    </div>
                                    <div class="program__module-item-right">
                                        <ul class="program__module-item-list">
                                            <li><span>Регулировка тормозных систем</span></li>
                                            <li><span>Шиномонтаж</span></li>
                                            <li><span>Регулировки узлов</span></li>
                                            <li><span>Подшипники</span></li>
                                            <li><span>Гидроизоляция и попадания воды в самокат</span></li>
                                            <li><span>Ручки тормоза и газа</span></li>
                                            <li><span>Сверчки</span></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="program__module-item d-flex">
                                    <div class="program__module-item-left">
                                        <div class="program__module-item-count">Модуль 5</div>
                                        <div class="program__module-item-title">Практика ремонт электрика </div>
                                    </div>
                                    <div class="program__module-item-right">
                                        <ul class="program__module-item-list">
                                            <li><span>АКБ</span></li>
                                            <li><span>BMS</span></li>
                                            <li><span>Контроллер</span></li>
                                            <li><span>Мотор колесо</span></li>
                                            <li><span>Проводка</span></li>
                                            <li><span>Дисплеи</span></li>
                                            <li><span>Свет</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="program__part d-flex">
                            <div class="program__part-left">
                                <h3 class="program__part-title">Открытие мастерской </h3>
                                <div class="program__part-days">1 день</div>
                            </div>
                            <div class="program__module d-flex  flex-column">
                                <div class="program__module-item d-flex">
                                    <div class="program__module-item-left">
                                        <div class="program__module-item-count">Модуль 1</div>
                                        <div class="program__module-item-title">Мастерская наш опыт </div>
                                    </div>
                                    <div class="program__module-item-right">
                                        <ul class="program__module-item-list">
                                            <li><span>Выбор локации и требования к мастерской</span></li>
                                            <li><span>Номенклатура инструмента</span></li>
                                            <li><span>Мебель, оборудование и оргтехника</span></li>
                                            <li><span>Номенклатура запчастей</span></li>
                                            <li><span>CRM</span></li>
                                            <li><span>Безопасность</span></li>
                                            <li><span>Юр часть</span></li>
                                            <li><span>Наша экономика мастерской</span></li>
                                            <li><span>Продажи самокатов и аксессуаров</span></li>
                                            <li><span>Работа с клиентами</span></li>
                                            <li><span>Система мотивации персонала</span></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="program__module-item d-flex justify-content-between">
                                    <div class="program__module-item-left">
                                        <div class="program__module-item-count">Модуль 2</div>
                                        <div class="program__module-item-title">Реклама </div>
                                    </div>
                                    <div class="program__module-item-right">
                                        <ul class="program__module-item-list">
                                            <li><span>Какие инструменты работают и не работают у нас</span></li>
                                            <li><span>Ютуб</span></li>
                                            <li><span>Продвижения в полях</span></li>
                                        </ul>
                                    </div>
                                </div>
                            <div>
                        </div>
                    </div>
                </div>
           </div></div></section>
           <section class="after-course">
                <div class="container">
                    <div class="d-flex justify-content-between after-course__top">
                        <h2 class="title">После прохождения курса вы получите </h2>
                        <div class="d-flex">
                                <div class="program__place-item">
                                    <div class="program__place-item-title">Ближайшая дата</div>
                                    <div class="program__place-item-text">18-20 февраля</div>
                                </div>
                                <div>
                                    <a class="btn anchor-to-form" href="#form-apply">Записаться на курс</a>
                                </div>
                        </div>
                    </div>
                    <div class="d-flex flex-wrap after-course__list">
                        <div class="after-course__item d-flex flex-column">
           	                <img src="/local/templates/motion_new/images/registration.svg">
           	                <div class="after-course__item-text">Сертификат по итога прохождения курса</div>
                        </div>
                        <div class="after-course__item d-flex flex-column">
           	                <img src="/local/templates/motion_new/images/chat.svg">
           	                <div class="after-course__item-text">Персональную поддержку в чате </div>
                        </div>
                        <div class="after-course__item d-flex flex-column">
           	                <img src="/local/templates/motion_new/images/fm.svg">
           	                <div class="after-course__item-text">Предложение о сотрудничестве под ФМ </div>
                        </div>
                        <div class="after-course__item d-flex flex-column">
           	                <img src="/local/templates/motion_new/images/documents.svg">
           	                <div class="after-course__item-text">Все презентации и материалы </div>
                        </div>
                    </div>
                </div>
           </section>
            <section class="gray">
    <div class="container">
		<!--<div class="ajax-load-block" data-lhref="/includes/ajax-testi.php"></div>-->

<div class="row">
    <div class="col-lg-12 section-head d-flex align-items-center justify-content-between mb-5">
        <h2 class="title mb-0">Отзывы клиентов</h2><a class="btn" href="/web/20220626050352/https://fast-motion.ru/otzyvy/">все отзывы</a>
    </div>
</div>
<div class="row lazy-load-box">
    <div class="col-lg-12">
        <div class="swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-autoheight" id="rewievs-slider">
            <div class="swiper-wrapper" style="height: 503px; transform: translate3d(0px, 0px, 0px);">


    <div class="swiper-slide d-flex flex-column justify-content-between swiper-slide-active" id="bx_3218110189_17" style="width: 529.667px; margin-right: 30px;">
        <div class="swiper-head">
            <div class="swiper-head__top d-flex"><img src="/upload/iblock/d45/d455b17e706537ecf5b6a466a1b65524.png">
                <div class="swiper-head__right">
                    <div class="swiper-head__name">Владимир из mail.ru</div>
                    <a href="#" class="swiper-head__proff">www.facebook.com/voftik</a>
                </div>
            </div>
            <div class="swiper-head__text"></div>
                            <div id="bx_3218110189_17">

                    <div class="youtube" id="V_cEmpqVKHo" style="width: 370px; height: 245px; background-image: url(&quot;https://web.archive.org/web/20220626050352/https://i.ytimg.com/vi/V_cEmpqVKHo/sddefault.jpg&quot;);"><div class="play"></div></div>
                </div>
                    </div>
        <div class="swiper-date">29.04.2020</div>
    </div>

    <div class="swiper-slide d-flex flex-column justify-content-between swiper-slide-next" id="bx_3218110189_16" style="width: 529.667px; margin-right: 30px;">
        <div class="swiper-head">
            <div class="swiper-head__top d-flex"><img src="/upload/iblock/9f0/9f0baeebb0e51a6acbc8e57a572044f7.png">
                <div class="swiper-head__right">
                    <div class="swiper-head__name">Саша Усольцев</div>
                    <a href="#" class="swiper-head__proff">moscowwalks.ru</a>
                </div>
            </div>
            <div class="swiper-head__text"></div>
                            <div id="bx_3218110189_16">

                    <div class="youtube" id="RVzvk8hQ_8A" style="width: 370px; height: 245px; background-image: url(&quot;https://web.archive.org/web/20220626050352/https://i.ytimg.com/vi/RVzvk8hQ_8A/sddefault.jpg&quot;);"><div class="play"></div></div>
                </div>
                    </div>
        <div class="swiper-date">29.04.2020</div>
    </div>

    <div class="swiper-slide d-flex flex-column justify-content-between" id="bx_3218110189_2" style="width: 529.667px; margin-right: 30px;">
        <div class="swiper-head">
            <div class="swiper-head__top d-flex"><img src="/upload/iblock/6da/6da511860632a77f86d3e562e0134474.png">
                <div class="swiper-head__right">
                    <div class="swiper-head__name">Николай Роштальдт</div>
                    <a href="#" class="swiper-head__proff">яндекс</a>
                </div>
            </div>
            <div class="swiper-head__text">Выражаю благодарность сотрудникам сервисного центра FastMotion за высокий уровень сервиса, теплый прием и результат. Я приехал на сервис чтобы устранить люфт руля, а по итогу обменял свой по Trade-in на PRO и купил самокат M365 для жены. Всем рекомендую!</div>
                    </div>
        <div class="swiper-date">01.04.2020</div>
    </div>
            </div>
            <div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets"><span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button" aria-label="Go to slide 1"></span></div>
        <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
    </div>
</div>

<script type="text/javascript">
    "use strict";
new Swiper("#rewievs-slider",{slidesPerView:3,spaceBetween:30,observer:!0,observeParents:!0,autoHeight:!0,pagination:{el:"#rewievs-slider .swiper-pagination",clickable:!0},autoplay:{delay:5e3,disableOnInteraction:!1},breakpoints:{1110:{slidesPerView:2},650:{slidesPerView:1}}})

</script>
    </div>
</section>            <section class="dark">
                <div class="container">
                    <div class="d-flex justify-content-between dark__top">
                        <h2 class="title">Стоимость обучения</h2>
                        <div class="program__place-item">
                            <div class="program__place-item-title">Ближайшая дата</div>
                            <div class="program__place-item-text">18-20 февраля</div>
                        </div>
                    </div>
                    <div class="d-flex  justify-content-between about-cost">
                        <div class="about-cost__item">
                            <div class="about-cost__all-info">Стоимость за 2 блока курса </div>
                            <div class="about-cost__all-price">40 000 ₽</div>
                            <div class="about-cost__part">
                                <div class="about-cost__part-title">Блочные форматы обучения</div>
                                <div class="d-flex">
                                    <div class="about-cost__part-item">
                                        <div class="about-cost__part-price">30 000 ₽</div>
                                        <div class="about-cost__part-info">1 Блок (Ремонт 2 дня)</div>
                                    </div>
                                    <div class="about-cost__part-item">
                                        <div class="about-cost__part-price">15 000 ₽</div>
                                        <div class="about-cost__part-info">Можно в рассрочку</div>
                                    </div>
                                </div>
                            </div>
                            <div class="about-cost__info">
                                <div class="about-cost__info-title">Что входит в стоимость:</div>
                                <div class="about-cost__info-list">
                                    <div class="about-cost__info-item">- 3 для очного  обучккния </div>
                                    <div class="about-cost__info-item">- поддержка после обучения </div>
                                    <div class="about-cost__info-item">- материалы и сертиификат</div>
                                </div>
                            </div>
                        </div>
                        <div class="about-cost__item">
                            <div id="form-apply" class="title">Получить бесплатную консультацию</div>
                            <form class="ajax-form">
                                <input type="hidden" name="subject" value="Запись на курс">
                                <input type="hidden" name="goal" value="course">
                                <input type="hidden" name="url" value="https://fast-motion.ru/obuchenie/remont-samokatov/">
                                <div class="form__wrap">
                                    <div class="form-group">
                                        <input type="name" id="name" name="name" class="form-control" placeholder="Имя">
                                    </div>
                                    <div class="form-group">
                                        <input type="phone" id="phone" required="" name="phone" class="form-control phone_mask" placeholder="Телефон">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" id="email" name="email" placeholder="Email">
                                    </div>
                                </div>
                                <button class="btn" data-toggle="modal">Отправить</button>
                            </form>
                            <div class="privacy">Нажимая на кнопку, я соглашаюсь
                                                 на обработку персональных данных</div>
                        </div>
                    </div>
                </div>
            </section>
</main>

    <script>$(function(){

            $('.anchor-to-form').on('click', function(e){
                $('html,body').stop().animate({ scrollTop: $('#form-apply').offset().top }, 1000);
                e.preventDefault();
            });

        });
    </script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>