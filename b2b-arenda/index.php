<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Аренда электросамокатов для мероприятий в Москве");
$APPLICATION->SetPageProperty("description", "Аренда электросамокатов для мероприятий, велосипеды и мощные самокаты для вас.");
$APPLICATION->SetTitle("B2B Аренда");
?>
    <section class="head-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:breadcrumb",
                        "bread",
                        Array(
                            "PATH" => "",
                            "SITE_ID" => "s1",
                            "START_FROM" => "0"
                        )
                    );?>
                </div>
                <div class="col-lg-12">
                    <h1 class="head-section__title">
                        <?$APPLICATION->ShowTitle(false)?>
                    </h1>
                    <a class="btn" href="#" data-target="#exampleModal2" data-toggle="modal">КОНСУЛЬТАЦИЯ</a>
                </div>
            </div>
        </div>
    </section> <section class="advantages">
    <div class="container">
        <div class="row d-flex justify-content-between pb-5">
            <div class="col-lg-5">
                <div class="advantages__left">
                    <img src="/bitrix/templates/motion/images/alogo.svg" class="logo">
                    <h2 class="advantages__title">
                        Мощные электросамокаты для вашего мероприятия
                    </h2>
                    <div class="advantages__desc">
                        Вы можете взять в прокат мощные электросамокаты Xiaomi Mijia M365 PRO
                    </div>
                    <a class="btn" href="#" data-toggle="modal" data-target="#exampleModal2">КОНСУЛЬТАЦИЯ</a>
                </div>
            </div>
            <div class="col-lg-7 d-flex flex-wrap justify-content-between">
                <div class="advantages__block d-flex flex-column">
                    <img src="/bitrix/templates/motion/images/01.svg">
                    <div class="advantages__t">
                        Доставка
                    </div>
                    <div class="advantages__d">
                        Доставим и заберем электросамоты в Москве и области
                    </div>
                </div>
                <div class="advantages__block d-flex flex-column">
                    <img src="/bitrix/templates/motion/images/02.svg">
                    <div class="advantages__t">
                        Безналичная оплата
                    </div>
                    <div class="advantages__d">
                        Оплата по безналичному расчету (без НДС)
                    </div>
                </div>
                <div class="advantages__block d-flex flex-column">
                    <img src="/bitrix/templates/motion/images/03.svg">
                    <div class="advantages__t">
                        Выдача и контроль
                    </div>
                    <div class="advantages__d">
                        Обеспечим выдачу и контроль на вашем меропиятии
                    </div>
                </div>
                <div class="advantages__block d-flex flex-column">
                    <img src="/bitrix/templates/motion/images/04.svg">
                    <div class="advantages__t">
                        Обучение
                    </div>
                    <div class="advantages__d">
                        Для гостей мероприятия и расскажем про электротранспорт
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container mt-5 mb-5">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="rent-block rent-big">
                    <img src="/bitrix/templates/motion/images/r3.jpg">
                    <div class="rent-block__main">
                        <div class="rent-block__name">
                            Аренда электросамокатов<br>
                            для эвентов и экскурский
                        </div>
                        <div class="rent-block__desc">
                            Дополнительно при аренде электросамокатов, устроим мастер класс по обучению катанию на моноколесах
                        </div>
                        <div class="rent-block__price">
                            2500₽ / под ключ<a class="btn" href="#" data-target="#exampleModal5" data-toggle="modal">взять в аренду</a>
                        </div>
                        <div class="rent-block__mini">
                            Минимум 15 самокатов *
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container mb-5">
        <div class="row">
            <div class="col-lg-12 section-head d-flex align-items-center justify-content-between mb-5">
                <h2 class="title mb-0">
                    Наши экскурсии
                </h2>
                <a class="btn" href="#">больше экскурский</a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <div class="ex-block">
                    <img src="/bitrix/templates/motion/images/e1.jpg" class="ex-block__image">
                    <div class="ex-block__pre">
                        Экскурсия
                    </div>
                    <div class="ex-block__name">
                        Настоящая Тверская и ее переулки
                    </div>
                    <a class="elipse-btn" href="#"><img src="/bitrix/templates/motion/images/reda.svg"></a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="ex-block">
                    <img src="/bitrix/templates/motion/images/e2.jpg" class="ex-block__image">
                    <div class="ex-block__pre">
                        Эвент
                    </div>
                    <div class="ex-block__name">
                        От Воздвиженки до Никитской
                    </div>
                    <a class="elipse-btn" href="#"><img src="/bitrix/templates/motion/images/reda.svg"></a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="ex-block">
                    <img src="/bitrix/templates/motion/images/e3.jpg" class="ex-block__image">
                    <div class="ex-block__pre">
                        Экскурсия
                    </div>
                    <div class="ex-block__name">
                        Настоящая Тверская и ее переулки
                    </div>
                    <a class="elipse-btn" href="#"><img src="/bitrix/templates/motion/images/reda.svg"></a>
                </div>
            </div>
        </div>
    </div>
    <div class="container pt-5">
        <div class="row about-images">
            <div class="col-lg-3 mb-4">
                <img src="/bitrix/templates/motion/images/13.jpg">
            </div>
            <div class="col-lg-3 mb-4">
                <img src="/bitrix/templates/motion/images/14.jpg">
            </div>
            <div class="col-lg-3 mb-4">
                <img src="/bitrix/templates/motion/images/15.jpg">
            </div>
            <div class="col-lg-3 mb-4">
                <img src="/bitrix/templates/motion/images/16.jpg">
            </div>
        </div>
    </div>
</section>
<?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    Array(
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => "",
        "PATH" => "/includes/testi.php"
    )
);?> <section class="white-section">
    <div class="container">
        <?$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
                "AREA_FILE_SHOW" => "file",
                "AREA_FILE_SUFFIX" => "inc",
                "EDIT_TEMPLATE" => "",
                "PATH" => "/includes/blog.php"
            )
        );?>
    </div>
</section> <br>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>