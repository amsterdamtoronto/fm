<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Тюнинг электросамокатов в Москве Xiaomi и Kugoo");
$APPLICATION->SetPageProperty("description", "Тюнинг электросамокатов в Москве по низким ценам. Модернизация за приемлемые деньги, профессиональная команда мастеров, большой выбор тюнинга вашего электросамоката от ведущей мастерской города. Хотите прокачать свой транспорт до предела? Обращайтесь!");
$APPLICATION->SetTitle("Тюнинг электросамокатов");
?><section class="head-section">
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"bread",
	Array(
		"PATH" => "",
		"SITE_ID" => "s1",
		"START_FROM" => "0"
	)
);?>
		</div>
		<div class="col-lg-12">
			<h1 class="head-section__title">
			<?$APPLICATION->ShowTitle(false)?> </h1>
 <a class="btn" href="#">БЕСПЛАТНАЯ КОНСУЛЬТАЦИЯ</a>
		</div>
	</div>
</div>
</section>
<section class="gray">
	<div class="container">
		<div class="row d-flex align-items-center mt-5 pt-5 pb-5 mb-5">
			<div class="col-lg-6">
				<?/*<div class="youtube" id="6hPuT1e9vbM" style="width: 100%; height: 245px;"></div>*/?>
				<div class="youtube" id="XIwND2mX8Z4" style="width: 100%; height: 245px;"></div>
			</div>
			<div class="col-lg-6">
				<div class="view">
					<div class="view-pre">
						Комплексные работы
					</div>
					<div class="view-name">
						Смотрите видео о комплексных работах
					</div>
	<a href="" class="btn">Больше видео</a>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="white-section">
	<div class="container">
		<?/*<div class="row">*/?>
			<?$APPLICATION->IncludeComponent(
				"bitrix:main.include",
				"",
				Array(
					"AREA_FILE_SHOW" => "file",
					"AREA_FILE_SUFFIX" => "inc",
					"EDIT_TEMPLATE" => "",
					"PATH" => "/includes/tarifs.php"
				)
			);?>
		<?/*</div>*/?>
	</div>
</section>
<section class="white-section">
<div class="container">
	<div class="row">
		<h2 class="col-lg-12 title">Наши модернизации и тюнинг электросамокатов </h2>
	</div>
	<div class="row about-images popup-gallery2">
		<div class="col-lg-3 mb-4">
 <a href="/bitrix/templates/motion/images/01.jpg"> <img src="/bitrix/templates/motion/images/01.jpg"> </a>
		</div>
		<div class="col-lg-3 mb-4">
 <a href="/bitrix/templates/motion/images/02.jpg"> <img src="/bitrix/templates/motion/images/02.jpg"> </a>
		</div>
		<div class="col-lg-3 mb-4">
 <a href="/bitrix/templates/motion/images/03.jpg"> <img src="/bitrix/templates/motion/images/03.jpg"> </a>
		</div>
		<div class="col-lg-3 mb-4">
 <a href="/bitrix/templates/motion/images/04.jpg"> <img src="/bitrix/templates/motion/images/04.jpg"> </a>
		</div>
		<div class="col-lg-3 mb-4">
 <a href="/bitrix/templates/motion/images/05.jpg"> <img src="/bitrix/templates/motion/images/05.jpg"> </a>
		</div>
		<div class="col-lg-3 mb-4">
 <a href="/bitrix/templates/motion/images/06.jpg"> <img src="/bitrix/templates/motion/images/06.jpg"> </a>
		</div>
		<div class="col-lg-3 mb-4">
 <a href="/bitrix/templates/motion/images/07.jpg"> <img src="/bitrix/templates/motion/images/07.jpg"> </a>
		</div>
		<div class="col-lg-3 mb-4">
 <a href="/bitrix/templates/motion/images/08.jpg"> <img src="/bitrix/templates/motion/images/08.jpg"> </a>
		</div>
	</div>
</div>
 </section>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/includes/testi.php"
	)
);?> <section class="white-section">
<div class="container">
	 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/includes/blog.php"
	)
);?>
</div>
<div class="container">
<div class="row about-text">
<div class="col-lg-6 pr-5">
	<h2 class="title mb-3">Тюнинг в Fast motion – модернизация электросамоката до максимума</h2>
	<p>Мастерская Fast motion осуществляет профессиональный тюнинг электросамокатов брендов куга и ксиаоми. Воспользовавшись нашим предложением, вы сможете обновить бу или модернизировать купленное транспортное средство.</p>
	<p>В работе используются исключительно оригинальные комплектующие – клиенты получают лучший результат, на который предоставляется гарантия сроком 1 год. На сайте возможно ознакомиться с возможными вариантами тюнинга для Xiaomi Mijia M365, Xiaomi Mijia PRO и Kugoo. Это позволит определиться с приоритетами в модернизации и определить масштабы работ для мастера.</p>
</div>
<div class="col-lg-6">
<h2 class="title mb-3">Что можно улучшить в электросамокате?</h2>
	<p>Особой популярностью пользуются такие процедуры при тюнинге электросамокатов:</p>
	<ul>
	<li>обновление ПО (уникальная прошивка, увеличивающая мощность и скорость);</li>
<li>герметизация важных узлов;</li>
<li>установка ручки на руль и подножки для ребенка;</li>
<li>прошивка чипа;</li>
<li>обслуживание всех составляющих;</li>
<li>замена подшипников и силовых разъемов;</li>
<li>доработка узла складывания и заднего амортизатора;</li>
<li>установка втулки заднего амортизатора, 8-гранной рулевой втулки, поддержки заднего крыла;</li>
<li>дополнительный обжим фазных проводов двигателя;</li>
<li>регулировка и настройка всех узлов;</li>
<li>улучшение контроллера;</li>
<li>модернизация крепления батареи;</li>
<li>покраска.</li>
</ul>
	<p>Это далеко не весь перечень доступных модернизаций. Вы можете обратиться к мастеру с конкретным вопросом – он обязательно предложит вам вариант реализации идеи или альтернативный способ улучшения электросамоката.</p>
	<p>После профессионального тюнинга ваше транспортное средство станет не только эффективнее, быстрее и мощнее, оно приобретет впечатляющий внешний вид, который будет заслуживать восхищенных взглядов окружающих. Заказывайте модернизацию в мастерской Fast motion, чтобы максимально прокачать свой электросамокат.</p>
</div>
</div>
</div>
 </section>

 <section class="advantages gray">
	<div class="container">
		<div class="row d-flex justify-content-between">
			<div class="col-lg-5">
				<div class="advantages__left">
	<img src="/bitrix/templates/motion/images/alogo.svg" class="logo">
					<h2 class="advantages__title">
					Прокачаем ваш электросамокат </h2>
					<div class="advantages__desc">
						Тюнинг самокатов Xiaomi, Kugoo
					</div>
	<a class="btn" href="#" data-toggle="modal" data-target="#exampleModal2">КОНСУЛЬТАЦИЯ</a>
				</div>
			</div>
			<div class="col-lg-7 d-flex flex-wrap justify-content-between">
				<div class="advantages__block d-flex flex-column">
	<img src="/bitrix/templates/motion/images/01.svg">
					<div class="advantages__t">
						Cервисные центры
					</div>
					<div class="advantages__d">
						Калужская и Путилково в Москве
					</div>
				</div>
				<div class="advantages__block d-flex flex-column">
	<img src="/bitrix/templates/motion/images/02.svg">
					<div class="advantages__t">
						Запчасти
					</div>
					<div class="advantages__d">
						Свой склад запчстей
					</div>
				</div>
				<div class="advantages__block d-flex flex-column">
	<img src="/bitrix/templates/motion/images/03.svg">
					<div class="advantages__t">
						Гарантия 1 год
					</div>
					<div class="advantages__d">
						На новые самокаты с нашей подготовкой и ремонт
					</div>
				</div>
				<div class="advantages__block d-flex flex-column">
	<img src="/bitrix/templates/motion/images/04.svg">
					<div class="advantages__t">
						Выездной ремонт
					</div>
					<div class="advantages__d">
						Мастера cделают все работу у вас дома или в офисе
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>