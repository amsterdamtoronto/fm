<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Контактная информация FastoMotion: телефон, адрес, обратная связь, карта проезда к сервисным центрам в Москве рядом с метро.");
$APPLICATION->SetPageProperty("title", "Контакты Fast-Motion в Москве адрес и телефон");
$APPLICATION->SetTitle("Контактная информация FastMotion");
?>
    <section class="head-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:breadcrumb",
                        "bread",
                        Array(
                            "PATH" => "",
                            "SITE_ID" => "s1",
                            "START_FROM" => "0"
                        )
                    ); ?>
                </div>
                <div class="col-lg-12">
                    <h1 class="head-section__title">
                        <? $APPLICATION->ShowTitle(false) ?> </h1>
                    <a class="btn" href="#" data-toggle="modal" data-target="#exampleModal2">БЕСПЛАТНАЯ КОНСУЛЬТАЦИЯ</a>
                </div>
            </div>
        </div>
    </section>
    <section class="white-section custom-contacts">
        <div class="tabs">
            <div class="contacts-city__block-wrapper">
                <div class="container">
                    <ul class="tabs__caption contacts-city__block">
                        <li class="active">Москва</li>
                        <li>Краснодар</li>
                        <li>Санкт-Петербург</li>
                    </ul>
                </div>
            </div>
            <div class="tabs__content active tabs__newContact">
                <div class="container">
                    <div class="row d-flex contacts-content">
                        <div class="col-lg-4 col-sm-12 justify-content-around">
                            <div class="contacts-line contacts-line__top-lg">
                                <div class="contacts-line__small">
                                    Сервисный центр
                                </div>
                                <a class="contacts-line__large" href="tel:+7 495 133 59 40">+7 495 133 59 40</a>
                            </div>
                            <div class="contacts-line">
                                <div class="contacts-line__small">
                                    Интернет-магазин:

                                </div>
                                <div class="contacts-line__large">
                                    Пн-Вс 10:00-20:00
                                    <!-- <a class="contacts-line__large" href="https://www.youtube.com/watch?v=fs4tHB94xWA" target="_blank">Как пройти<br></a> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-12 justify-content-around">
                            <div class="contacts-line contacts-line__top-lg">
                                <div class="contacts-line__small">
                                    По всем вопросам
                                </div>
                                <a class="contacts-line__large" href="mailto:office@fast-motion.ru">office@fast-motion.ru</a>
                            </div>

                        </div>
                        <div class="col-lg-4 col-sm-12 justify-content-around">
                            <div class="contacts-line contacts-line__call contacts-line__top-lg">
                                <a class="btn" href="tel:+7 495 133 59 40">Позвонить</a>
                            </div>
                            <!-- <div class="contacts-line">
                                 <div class="contacts-line__small">
                                     Cоциальные сети
                                 </div>
                                 <ul class="contacts-line__social">
                                     <li><a href="https://www.instagram.com/fastmotionelectric/"> <img
                                                     src="/bitrix/templates/motion/images/inb.svg"></a></li>
                                     <li><a href="https://t.me/FastMotion"><img
                                                     src="/bitrix/templates/motion/images/tgb.svg"></a></li>
                                     <li><a href="https://www.youtube.com/c/fastmotionelectric"><img
                                                     src="/bitrix/templates/motion/images/ytb.svg"></a></li>
                                 </ul>
                             </div>-->
                        </div>
                    </div>
					<div class="contacts-line__large">
						<div class="contacts-line">
                                <div class="contacts-line__small">
                                    Реквизиты
                                </div>
                                <ul class="recvizit">
	<li>Наименование: Индивидуальный предприниматель Алыренков Александр Николаевич</li>
	<li>ИНН: 770470048011</li>
	<li>Реквизиты банка:</li>
	<li>Название: ТОЧКА ПАО БАНКА "ФК ОТКРЫТИЕ"</li>
	<li>ИНН: 7706092528</li>
	<li>КПП: 770543002</li>
	<li>БИК: 044525999</li>
	<li>Город: Москва</li>
	<li>Корр. счёт: 30101810845250000999</li>
	<li>в ГУ БАНКА РОССИИ ПО ЦФО</li>
	<li>Счёт №: 4080281080150003839</li>
							</ul><br/>
                            </div>
					</div>
                    <div class="contacts-line__large">
                        Адреса мастерских в москве
                    </div>
                    <br>
                    <div class="row d-flex mb-5 tabs__newContact--item">
                        <div class="col-lg-8 col-sm-9">
                            <div class="row align-items-center">
                                <div class="contacts-line__large-number">
                                    1
                                </div>
                                <div class="col-lg-10 col-sm-10 justify-content-around how_to_get__adress">

                                    <a class="contacts-line__large" href="https://yandex.ru/maps/-/CCQx6VS7hC"
                                       target="_blank">
                                        м. Университет, улица Строителей 4 с 2</a>
                                    <div class="contacts-line__small mb-0">
                                        Время работы Вт-Сб 11-19
                                        (Вс выходной)
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-3 justify-content-around how_to_get">
                            <a class="btn" href="#" data-youtube="fs4tHB94xWA" data-toggle="modal"
                               data-target="#showGoTo">Как
                                пройти</a>
                        </div>
                    </div>

         <!--           <div class="row d-flex mb-5 tabs__newContact--item">
                        <div class="col-lg-8 col-sm-9">
                            <div class="row align-items-center">
                                <div class="contacts-line__large-number">
                                    2
                                </div>
                                <div class="col-lg-10 col-sm-10 justify-content-around how_to_get__adress">

                                    <a class="contacts-line__large"
                                       href="https://yandex.ru/maps/213/moscow/house/novotushinskiy_proyezd_8k1/Z04YdQ9gTEcCQFtvfXRyeX9iYA==/?from=tabbar&ll=37.381744%2C55.838912&mode=search&sctx=ZAAAAAgCEAAaKAoSCRr9aDhlxEJAEUfKFkm72UtAEhIJS2z9%2F3%2BN3z8RIydv3sERxT8oCjgAQIufAUgBagJydXAAnQHNzEw9oAEAqAEAvQGtSIM2wgER5%2FSu96wGvuOj%2FgPGjunKzATqAQDyAQD4AQCCAqIB0LwuINCS0L7Qu9C%2B0LrQvtC70LDQvNGB0LrQsNGPINCd0L7QstC%2B0YLRg9GI0LjQvdGB0LrQuNC5INC%2F0YDQvtC10LfQtCA40LoxICDQktGA0LXQvNGPINGA0LDQsdC%2B0YLRiyDQn9C9LdCh0LEgMTAtMTkgKNCS0YEg0LLRi9GF0L7QtNC90L7QuSApICDQmtCw0Log0J%2FRgNC%2B0LnRgtC4igIA&sll=37.381744%2C55.838912&source=serp_navig&sspn=0.090226%2C0.030017&text=%D0%BC.%20%D0%92%D0%BE%D0%BB%D0%BE%D0%BA%D0%BE%D0%BB%D0%B0%D0%BC%D1%81%D0%BA%D0%B0%D1%8F%20%D0%9D%D0%BE%D0%B2%D0%BE%D1%82%D1%83%D1%88%D0%B8%D0%BD%D1%81%D0%BA%D0%B8%D0%B9%20%D0%BF%D1%80%D0%BE%D0%B5%D0%B7%D0%B4%208%D0%BA1%20%20%D0%92%D1%80%D0%B5%D0%BC%D1%8F%20%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D1%8B%20%D0%9F%D0%BD-%D0%A1%D0%B1%2010-19%20%28%D0%92%D1%81%20%D0%B2%D1%8B%D1%85%D0%BE%D0%B4%D0%BD%D0%BE%D0%B9%20%29%20%20%D0%9A%D0%B0%D0%BA%20%D0%9F%D1%80%D0%BE%D0%B9%D1%82%D0%B8&z=14"
                                       target="_blank">
                                        м. Волоколамская,
                                        Новотушинский проезд 8к1</a>
                                    <div class="contacts-line__small mb-0">
                                         Время работы (По предварительной записи) Вт-Сб 11-18
                                        (Пн, Вс выходной)
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-3 justify-content-around how_to_get">
                            <a class="btn"
                               href="#" data-toggle="modal"
                               data-target="#showGoTo">Как
                                пройти <span class="data" style="display: none"><div class="swiper-slide">
                                <img src="<?/*= SITE_TEMPLATE_PATH */?>/images/contact_2_1.jpg" alt="">
                                        <div class="text">Новотушинский проезд 8к1, сзади метро волокаламская</div>
                            </div>
                            <div class="swiper-slide">
                                <img src="<?/*= SITE_TEMPLATE_PATH */?>/images/contact_2_2.jpg" alt="">
                                <div class="text">в торце  Торговая галлерия , заходите</div>
                            </div>
                            <div class="swiper-slide">
                                <img src="<?/*= SITE_TEMPLATE_PATH */?>/images/contact_2_3.jpg" alt="">
                               <div class="text">в торце  Торговая галлерия , заходите</div>
                            </div>
                            <div class="swiper-slide">
                                <img src="<?/*= SITE_TEMPLATE_PATH */?>/images/contact_2_4.jpg" alt="">
                                <div class="text">Прходите прямо и находите нас</div>
                            </div></span>
                            </a>

                        </div>
                    </div>-->
                <!--    <div class="row d-flex mb-5 tabs__newContact--item">
                        <div class="col-lg-8 col-sm-9">
                            <div class="row align-items-center">
                                <div class="contacts-line__large-number">
                                    3
                                </div>
                                <div class="col-lg-10 col-sm-10 justify-content-around how_to_get__adress">

                                    <a class="contacts-line__large"
                                       href="https://yandex.ru/maps/213/moscow/house/malaya_semyonovskaya_ulitsa_9s10/Z04YcQdpSEQGQFtvfXt5d3xjbQ==/?from=tabbar&ll=37.709110%2C55.786010&source=serp_navig&z=17"
                                       target="_blank">
                                        м Электрозаводская,
                                        Малая семеновская улица д9 стр 10</a>
                                    <div class="contacts-line__small mb-0">
                                        Время работы (По предварительной записи) Вт-Сб 10-19
                                        (Вс,Пн выходной)
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-3 justify-content-around how_to_get">

                            <a class="btn"
                               href="#" data-toggle="modal"
                               data-target="#showGoTo">Как
                                пройти <span class="data" style="display: none"><div class="swiper-slide">
                                <img src="<?/*= SITE_TEMPLATE_PATH */?>/images/contact_3_1.png" alt="">
                                        <div class="text">Главное КПП и въезд с улицы Девятая рота</div>
                            </div>
                            <div class="swiper-slide">
                                <img src="<?/*= SITE_TEMPLATE_PATH */?>/images/contact_3_2.png" alt="">
                                <div class="text">Вход в кпп</div>
                            </div>
                            <div class="swiper-slide">
                                <img src="<?/*= SITE_TEMPLATE_PATH */?>/images/contact_3_3.png" alt="">
                               <div class="text">Выходим из КПП, двигаемся прямо метров 30 огибая с левой стороны здание 9стр12</div>
                            </div>
                            <div class="swiper-slide">
                                <img src="<?/*= SITE_TEMPLATE_PATH */?>/images/contact_3_4.png" alt="">
                                <div class="text">После здания 9стр12 поворачиваем направо и двигаемся прямо ещё метров 30</div>
                            </div>
                                     <div class="swiper-slide">
                                <img src="<?/*= SITE_TEMPLATE_PATH */?>/images/contact_3_5.png" alt="">
                                <div class="text">Наша здание отдельно стоящее ул. Малая Семёновская 9стр10, огибаем его справа</div>
                            </div>
                                     <div class="swiper-slide">
                                <img src="<?/*= SITE_TEMPLATE_PATH */?>/images/contact_3_6.png" alt="">
                                <div class="text">Вход</div>
                            </div>
                                </span>
                            </a>
                        </div>
                    </div>-->
                  <!--  <div class="row d-flex mb-5 tabs__newContact--item">
                        <div class="col-lg-8 col-sm-9">
                            <div class="row align-items-center">
                                <div class="contacts-line__large-number">
                                 3
                                </div>
                                <div class="col-lg-10 col-sm-10 justify-content-around how_to_get__adress">

                                    <a class="contacts-line__large"
                                       href="https://yandex.ru/maps/213/moscow/house/ulitsa_bogdanova_52k1/Z04YdQ5mS0QHQFtvfXp1eHhibA==/?ll=37.396814%2C55.649471&source=wizgeo&utm_medium=maps-desktop&utm_source=serp&z=17.36"
                                       target="_blank">
                                        м. Солнцево,
                                        ул. Богданова 52/1
                                    </a>
                                    <div class="contacts-line__small mb-0">
                                        Время работы Пн-Сб 10-19
                                        (Вс выходной)
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-3 justify-content-around how_to_get">

                            <a class="btn"
                               href="#" data-toggle="modal"
                               data-target="#showGoTo">Как
                                пройти <span class="data" style="display: none">
                                    <div class="swiper-slide">
                                        <img src="<?/*= SITE_TEMPLATE_PATH */?>/images/contact_4_1.png" alt="">
                                        <div class="text">От ст. метро Солнцево (4-й выход) двигаемся в сторону нового ресторанного комплекса (ярмарки выходного дня)</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <img src="<?/*= SITE_TEMPLATE_PATH */?>/images/contact_4_2.png" alt="">
                                        <div class="text">Проходим между ресторанным комплексом и ярмаркой</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <img src="<?/*= SITE_TEMPLATE_PATH */?>/images/contact_4_3.png" alt="">
                                        <div class="text">Проходим мимо семейного медицинского центра</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <img src="<?/*= SITE_TEMPLATE_PATH */?>/images/contact_4_4.png" alt="">
                                        <div class="text">Следующее после мед центра наше здание. Обходим его слева.</div>
                                    </div>
                                     <div class="swiper-slide">
                                         <img src="<?/*= SITE_TEMPLATE_PATH */?>/images/contact_4_5.png" alt="">
                                         <div class="text"></div>
                                     </div>
                                </span>
                            </a>
                        </div>
                    </div>-->
                    <!--<div class="row d-flex mb-5 tabs__newContact--item">
                        <div class="col-lg-8 col-sm-9">
                            <div class="row align-items-center">
                                <div class="contacts-line__large-number">
                                    3
                                </div>
                                <div class="col-lg-10 col-sm-10 justify-content-around how_to_get__adress">

                                    <a class="contacts-line__large"
                                       href="https://yandex.ru/maps/213/moscow/house/vereskovaya_ulitsa_16/Z04YcANjTE0PQFtvfXR3cH1mYg==/?ll=37.643588%2C55.861156&source=wizgeo&utm_medium=maps-desktop&utm_source=serp&z=17.15"
                                       target="_blank">
                                        м. Свиблово,
                                        ул. Вересковая 16

                                    </a>
                                    <div class="contacts-line__small mb-0">
                                      Время работы (По предварительной записи) Вт-Сб 11-18
                                        (Пн, Вс выходной)
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-3 justify-content-around how_to_get">

                            <a class="btn"
                               href="#" data-toggle="modal"
                               data-target="#showGoTo">Как
                                пройти <span class="data" style="display: none">
                                    <div class="swiper-slide">
                                        <img src="<?/*= SITE_TEMPLATE_PATH */?>/images/contact_5_1.png" alt="">
                                        <div class="text">Вид с ул. Вересковой (ориентир - магазин «Пятерочка»)</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <img src="<?/*= SITE_TEMPLATE_PATH */?>/images/contact_5_2.png" alt="">
                                        <div class="text">Обходим дом во двор</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <img src="<?/*= SITE_TEMPLATE_PATH */?>/images/contact_5_3.png" alt="">
                                        <div class="text">Поворачиваем</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <img src="<?/*= SITE_TEMPLATE_PATH */?>/images/contact_5_4.png" alt="">
                                        <div class="text">Большая дверь возле второго подъезда.</div>
                                    </div>
                                </span>
                            </a>
                        </div>
                    </div>-->
                </div>
                <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A0ba0acc9c01164d5da7b88170dcb389a28feae40a7b0f85bf7030595b25f63d0&amp;source=constructor" width="100%" height="500" frameborder="0"></iframe>
            </div>
            <div class="tabs__content">
                <div class="container">
                    <div class="row d-flex contacts-content">
                        <div class="col-lg-4 col-sm-12 justify-content-around">
                            <div class="contacts-line contacts-line__top-lg">
                                <div class="contacts-line__small">
                                    Сервисный центр
                                </div>
                                <a class="contacts-line__large" href="tel:+7 918 346 72 72">+7 918 346 72 72</a>
                            </div>
                            <div class="contacts-line">
                                <div class="contacts-line__small">
                                    Режим работы
                                </div>
                                <div class="contacts-line__large">
                                    ПН – ВС с 10:00 до 19:00
                                    <!-- <a class="contacts-line__large" href="https://www.youtube.com/watch?v=fs4tHB94xWA" target="_blank">Как пройти<br></a> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-12 justify-content-around">
                            <div class="contacts-line contacts-line__top-lg">
                                <div class="contacts-line__small">
                                    По всем вопросам
                                </div>
                                <a class="contacts-line__large" href="mailto:office@fast-motion.ru">office@fast-motion.ru</a>
                            </div>
                            <div class="contacts-line">
                                <div class="contacts-line__small">
                                    Реквизиты
                                </div>
                                <a class="contacts-line__large"
                                   href="https://docs.google.com/document/d/19EeMEu6DrsTzmg74t8NUcEIq9F451Y2i2ttdHdniapo/edit">ИП
                                    Алыренков А.Н.</a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-12 justify-content-around">
                            <div class="contacts-line contacts-line__call contacts-line__top-lg">
                                <a class="btn" href="tel:+7 918 346 72 72">Позвонить</a>
                            </div>
                            <div class="contacts-line">
                                <div class="contacts-line__small">
                                    Cоциальные сети
                                </div>
                                <ul class="contacts-line__social">
                                   <!-- <li><a href="https://www.instagram.com/fastmotionelectric/"> <img
                                                    src="/bitrix/templates/motion/images/inb.svg"></a></li>-->
                                    <li><a href="https://t.me/FastMotion"><img
                                                    src="/bitrix/templates/motion/images/tgb.svg"></a></li>
                                    <li><a href="https://www.youtube.com/c/fastmotionelectric"><img
                                                    src="/bitrix/templates/motion/images/ytb.svg"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row d-flex mb-5">
                        <div class="col-lg-8 col-sm-12 justify-content-around">
                            <a class="contacts-line__large" href="https://yandex.ru/maps/-/CCQt4BUhkA" target="_blank">
                                <div class="contacts-line__large-number">
                                    2
                                </div>
                                Красноармейская улица, 95</a>
                        </div>
                        <!-- <div class="col-lg-4 col-sm-12 justify-content-around">
                                   <a href="https://www.youtube.com/watch?v=fs4tHB94xWA" class="btn" target="_blank">Как пройти</a>
                               </div> -->
                    </div>
                </div>
                <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A9ca3deef1cbfe38ede6bbf66642b6ad4f804a5a70861c117e8953894425733d9&amp;source=constructor"
                        width="100%" height="506" frameborder="0"></iframe>
            </div>
            <div class="tabs__content">
                <div class="container">
                    <div class="row d-flex contacts-content">
                        <div class="col-lg-4 col-sm-12 justify-content-around">
                            <div class="contacts-line contacts-line__top-lg">
                                <div class="contacts-line__small">
                                    Сервисный центр
                                </div>
                                <a class="contacts-line__large" href="tel:+7 900 659 69 77">+7 900 659 69 77</a>
                            </div>
                            <div class="contacts-line">
                                <div class="contacts-line__small">
                                    Режим работы
                                </div>
                                <div class="contacts-line__large">
                                    ПН – ВС с 12:00 до 19:00 (Предварительная запись)
                                    <!-- <a class="contacts-line__large" href="https://www.youtube.com/watch?v=fs4tHB94xWA" target="_blank">Как пройти<br></a> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-12 justify-content-around">
                            <div class="contacts-line contacts-line__top-lg">
                                <div class="contacts-line__small">
                                    По всем вопросам
                                </div>
                                <a class="contacts-line__large" href="mailto:office@fast-motion.ru">office@fast-motion.ru</a>
                            </div>
                            <div class="contacts-line">
                                <div class="contacts-line__small">
                                    Реквизиты
                                </div>
                                <a class="contacts-line__large"
                                   href="https://docs.google.com/document/d/19EeMEu6DrsTzmg74t8NUcEIq9F451Y2i2ttdHdniapo/edit">ИП
                                    Алыренков А.Н.</a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-12 justify-content-around">
                            <div class="contacts-line contacts-line__call contacts-line__top-lg">
                                <a class="btn" href="tel:+7 900 659 69 77">Позвонить</a>
                            </div>
                            <div class="contacts-line">
                                <div class="contacts-line__small">
                                    Cоциальные сети
                                </div>
                                <ul class="contacts-line__social">
                                    <!--<li><a href="https://www.instagram.com/fastmotionelectric/"> <img
                                                    src="/bitrix/templates/motion/images/inb.svg"></a></li>-->
                                    <li><a href="https://t.me/FastMotion"><img
                                                    src="/bitrix/templates/motion/images/tgb.svg"></a></li>
                                    <li><a href="https://www.youtube.com/c/fastmotionelectric"><img
                                                    src="/bitrix/templates/motion/images/ytb.svg"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row d-flex mb-5">
                        <div class="col-lg-8 col-sm-12 justify-content-around">
                            <a class="contacts-line__large" href="https://yandex.ru/maps/-/CCQx6VS7hC" target="_blank">
                                <div class="contacts-line__large-number">
                                    3
                                </div>
                                Метро Чернышевская улица Рылеева, 10</a>
                        </div>
                        <!-- <div class="col-lg-4 col-sm-12 justify-content-around">
                                   <a href="https://www.youtube.com/watch?v=fs4tHB94xWA" class="btn" target="_blank">Как пройти</a>
                               </div> -->
                    </div>
                </div>
                <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3Ad95a50426d680957a1cb8ed7b887685ef778245a387d6644ea39fcac20e98427&amp;source=constructor"
                        width="100%" height="506" frameborder="0"></iframe>
            </div>
        </div>
        <div class="container">
            <div class="row mt-5 pt-2 d-flex justify-content-between">
                <div class="col-lg-4">
                    <h2 class="con-title">
                        Свяжитесь<br>
                        с нами </h2>
                    <div class="con-desc">
                        Вы можете написать нам по любому интересующему вас вопросу: от обучения до сотрудничества.
                    </div>
                </div>
                <div class="col-lg-7">
                    <form class="contacts-form">
                        <input type="text" name="name" id="name3" placeholder="Ваше имя" required=""> <input
                                class="small-input" type="text" name="phone" id="phone3" placeholder="Номер телефона"
                                required=""> <input class="small-input" type="text" name="mail" id="mail"
                                                    placeholder="Электронная почта" required=""> <textarea rows="1"
                                                                                                           placeholder="Сообщение"
                                                                                                           required=""></textarea>
                        <label class="for-custom d-flex flex-row-reverse align-items-center justify-content-end"
                               for="agree">Я согласен с условиями обработки персональных данных <input
                                    class="custom-checkbox" type="checkbox" name="agree" id="agree"> </label>
                        <div class="bottom">
                        </div>
                        <label class="for-file d-flex flex-row-reverse align-items-start" for="file">Прикрепить файл<img
                                    src="/bitrix/templates/motion/images/v.svg"> <input class="file" type="file"
                                                                                        id="file"> </label> <span
                                class="mini d-none">только doc, docx, pdf или jpg не более 5 Мб</span>
                        <button class="btn" type="submit">Отправить</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="showGoTo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title" id="exampleModalLabel"><h3>Как пройти</h3></div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="swiper-container" id="popUpSlider">
                        <div class="swiper-wrapper">

                        </div>

                        <div slot="button-prev" id="SliderProductsPrev" class="swiper-button-prev" tabindex="0"
                             role="button"></div>
                        <div slot="button-next" id="SliderProductsNext" class="swiper-button-next" tabindex="0"
                             role="button"></div>

                        <div class="swiper-pagination"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        (function ($) {
            $(function () {

                $('ul.tabs__caption').on('click', 'li:not(.active)', function () {
                    $(this)
                        .addClass('active').siblings().removeClass('active')
                        .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
                });

            });
        })(jQuery);
    </script><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>