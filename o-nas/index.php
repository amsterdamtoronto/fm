<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О компании FastMotion");
?>
    <section class="head-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "bread", Array(
                        "PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
                        "SITE_ID" => "s1",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
                        "START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
                    ),
                        false
                    );?>
                </div>
                <div class="col-lg-12">
                    <div class="head-section__title"><?$APPLICATION->ShowTitle(false)?></div><a class="btn" href="#">БЕСПЛАТНАЯ КОНСУЛЬТАЦИЯ</a>
                </div>
            </div>
        </div>
    </section>
    <section class="advantages white-section">
        <div class="container">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => "/includes/advantages.php"
                )
            );?>
            <div class="row mt-5 mb-5">
                <div class="col-lg-6 title">Специализированный сервис Xiaomi Mijia M365 и PRO</div>
                <div class="col-lg-12">
                    <p>Наш профиль - это ремонт и тюнинг электросамокатов Xiaomi Mijia по низким ценам. Обращаясь к нам можете быть уверены в высоком уровне сервиса обслуживания и выгодных условиях на ремонтные работы и сервисное обслуживание. Доверьте свой любимый транспорт профессионалам.</p>
                </div>
            </div>
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => "/includes/service.php"
                )
            );?>
            <div class="row about-text mt-5">
                <div class="col-lg-10 title">Как осуществляется Trade-In?</div>
                <div class="col-lg-12">
                    <p>В наш сервисный центр Вы можете сдать любую модель электросамокат. Принимаются как новые, так и б/у устройства в любом состоянии.</p>
                </div>
                <div class="col-lg-4 mb-4">
                    <div class="conditions-block">
                        <div class="conditions-block__top d-flex align-items-center"><span>1</span>
                            <div class="conditions-block__name">Привести электросамокат</div>
                        </div>
                        <div class="conditions-block__text">К нам в сервисный центр, по адресу: город Москва, Научный проезд 8с1</div>
                    </div>
                </div>
                <div class="col-lg-4 mb-4">
                    <div class="conditions-block">
                        <div class="conditions-block__top d-flex align-items-center"><span>2</span>
                            <div class="conditions-block__name">Выбрать<br> вариант</div>
                        </div>
                        <div class="conditions-block__text">Вам поможет наш мастер выбрать подходящий для себя вариант</div>
                    </div>
                </div>
                <div class="col-lg-4 mb-4">
                    <div class="conditions-block">
                        <div class="conditions-block__top d-flex align-items-center"><span>3</span>
                            <div class="conditions-block__name">Доплатить разницу в стоимости</div>
                        </div>
                        <div class="conditions-block__text">Можете пользоваться и наслаждаться!</div>
                    </div>
                </div>
                <div class="col-lg-12 pt-4 pb-0 mb-0">
                    <p>Свяжитесь с нами чтобы сдать свой электросамокат через <a href="#">форму обратной связи</a> или по телефону <a href="#">+7 (915) 050-32-56</a></p>
                </div>
            </div>
            <div class="row docs mb-5">
                <div class="col-lg-12 d-flex align-items-center justify-content-between mt-5 pt-5">
                    <div class="title">В наличии вся разрешительная документация и лицензии</div>
                    <div class="swiper-buttons">
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="swiper-container" id="docs-slider">
                        <div class="swiper-wrapper popup-gallery">
                            <div class="swiper-slide"><a href="<?=SITE_TEMPLATE_PATH?>/images/docs.jpg"><img src="<?=SITE_TEMPLATE_PATH?>/images/docs.jpg"></a></div>
                            <div class="swiper-slide"><a href="<?=SITE_TEMPLATE_PATH?>/images/docs.jpg"><img src="<?=SITE_TEMPLATE_PATH?>/images/docs.jpg"></a></div>
                            <div class="swiper-slide"><a href="<?=SITE_TEMPLATE_PATH?>/images/docs.jpg"><img src="<?=SITE_TEMPLATE_PATH?>/images/docs.jpg"></a></div>
                            <div class="swiper-slide"><a href="<?=SITE_TEMPLATE_PATH?>/images/docs.jpg"><img src="<?=SITE_TEMPLATE_PATH?>/images/docs.jpg"></a></div>
                            <div class="swiper-slide"><a href="<?=SITE_TEMPLATE_PATH?>/images/docs.jpg"><img src="<?=SITE_TEMPLATE_PATH?>/images/docs.jpg"></a></div>
                            <div class="swiper-slide"><a href="<?=SITE_TEMPLATE_PATH?>/images/docs.jpg"><img src="<?=SITE_TEMPLATE_PATH?>/images/docs.jpg"></a></div>
                        </div>
                    </div>
                </div>
            </div>
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => "/includes/arenda.php"
                )
            );?>
        </div>
    </section>
<?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    Array(
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => "",
        "PATH" => "/includes/testi.php"
    )
);?>
    <section class="white-section">
        <div class="container">
            <div class="row about-text pt-5">
                <div class="col-lg-6 pr-5">
                    <div class="title mb-3">Тюнинг и ремонт электросамокатов</div>
                    <p>Наш профиль - это ремонт и тюнинг электросамокатов Xiaomi Mijia по низким ценам. Обращаясь к нам можете быть уверены в высоком уровне сервиса обслуживания и выгодных условиях на ремонтные работы и сервисное обслуживание. Доверьте свой любимый транспорт профессионалам.</p>
                    <ul>
                        <li>Приехать к нам в сервис</li>
                        <li>Оставить гарантию в размере 20000 рублей и забрать самокат Через неделю мы вычтем из гарантии 3500 рублей (500 рублей в день) за 7 дней аренды и вернем Вам 16500 когда вы вернете самокат.</li>
                        <li>Свяжитесь с нами чтобы взять электросамокат на прокат через форму обратной связи или по телефону +7 (915) 050-32-56</li>
                    </ul>
                    <p>В связи с большим потоком клиентов мы занимаемся скупкой, ремонтом и продажей электросамокатов Xiaomi Mijia. У нас Вы можете приобрести проверенные, прошедшие полное сервисное обслуживание самокаты, которые по своим характеристикам и ходовым качествам будут превосходить новые самокаты Mijia. Приезжайте в наш сервис на тест-драйв тюнингованных самокатов Xiaomi Mijia M365 и Mijia PRO!</p>
                </div>
                <div class="col-lg-6">
                    <div class="title mb-3">Услуги сервисного центра FASTMOTION</div>
                    <p>Мы работаем с 2017 года с самого начала появления электросамокатов в России. Про нас хорошо отзываются клиенты, пишут благодарности на форумах и в тематических чатах, доверяют и обращаются за тюнингом самокатов Xiaomi Mijia M365 или Xiaomi Mijia PRO до возможного предела.</p>
                    <ul>
                        <li>Модернизация и сервис Xiaomi Mijia</li>
                        <li>Ремонт и сервисное обслуживание</li>
                        <li>Покупка и продажа, Trade-in</li>
                        <li>Прокат и аренда самокатов</li>
                    </ul>
                    <p>Наши клиенты рекомендуют нас своим друзьям, после чего мы получаем много сообщений и звонков насчет покупки или просьбы дать покататься на неделю или месяц на самокате Xiaomi Mijia M365 или его старшего брата, Mijia PRO. У нас есть возможность давать скупленные и новые электросамокаты в прокат по низким ценам на короткий и длительный срок. Если вы хотите покататься месяц или сезон - обращайтесь!</p>
                </div>
            </div>
        </div>
    </section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>